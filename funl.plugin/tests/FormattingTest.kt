package org.ddol.idea.plugin.funl.tests

import com.intellij.openapi.command.WriteCommandAction
import com.intellij.psi.codeStyle.CodeStyleManager
import com.intellij.testFramework.builders.EmptyModuleFixtureBuilder
import com.intellij.testFramework.fixtures.CodeInsightFixtureTestCase
import com.intellij.testFramework.fixtures.ModuleFixture

/**
 * A test for Funl formatter.
 */
class FormattingTest : CodeInsightFixtureTestCase<EmptyModuleFixtureBuilder<ModuleFixture>>() {

    fun testFormatting() {

        myFixture.configureByFile(testFilePath("FormattingTest-unformatted.funl"))

        object : WriteCommandAction.Simple<Any>(project) {
            override fun run() {
                CodeStyleManager.getInstance(project).reformatText(myFixture.file, listOf(myFixture.file.textRange))
            }
        }.execute()

        myFixture.checkResult(doLoadFile("FormattingTest-formatted.funl"))
    }
}
