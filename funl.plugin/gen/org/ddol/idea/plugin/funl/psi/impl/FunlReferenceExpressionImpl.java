// This is a generated file. Not intended for manual editing.
package org.ddol.idea.plugin.funl.psi.impl;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.util.PsiTreeUtil;
import static org.ddol.idea.plugin.funl.psi.FunlTypes.*;
import org.ddol.idea.plugin.funl.navigation.FunlBasicReferenceElement;
import org.ddol.idea.plugin.funl.psi.*;
import com.intellij.psi.PsiElementResolveResult;
import org.ddol.idea.plugin.funl.navigation.FunlNamedElement;

public class FunlReferenceExpressionImpl extends FunlBasicReferenceElement implements FunlReferenceExpression {

  public FunlReferenceExpressionImpl(ASTNode node) {
    super(node);
  }

  public void accept(@NotNull FunlVisitor visitor) {
    visitor.visitReferenceExpression(this);
  }

  public void accept(@NotNull PsiElementVisitor visitor) {
    if (visitor instanceof FunlVisitor) accept((FunlVisitor)visitor);
    else super.accept(visitor);
  }

  @Override
  @NotNull
  public PsiElement getIdentifier() {
    return findNotNullChildByType(IDENTIFIER);
  }

  public boolean isFunctionCall() {
    return InjectedMethods.isFunctionCall(this);
  }

  @NotNull
  public FunlReferenceExpression getReference() {
    return InjectedMethods.getReference(this);
  }

  @NotNull
  public PsiElementResolveResult[] multiResolve(boolean unused) {
    return InjectedMethods.multiResolve(this, unused);
  }

  @NotNull
  public FunlNamedElement[] getVariants() {
    return InjectedMethods.getVariants(this);
  }

  @Nullable
  public String getName() {
    return InjectedMethods.getName(this);
  }

}
