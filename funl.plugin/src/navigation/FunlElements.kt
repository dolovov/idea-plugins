package org.ddol.idea.plugin.funl.navigation

import com.intellij.codeInsight.daemon.EmptyResolveMessageProvider
import com.intellij.extapi.psi.ASTWrapperPsiElement
import com.intellij.lang.ASTNode
import com.intellij.openapi.util.TextRange
import com.intellij.psi.*
import org.ddol.idea.plugin.funl.Texts.TEXT_ERROR_UNRESOLVED_REFERENCE_TEMPLATE
import org.ddol.idea.plugin.funl.psi.FunlTypes.IDENTIFIER
import org.ddol.idea.plugin.funl.psi.renameElement
import org.ddol.idea.plugin.funl.unreachableCode

/**
 * Some [PsiElement] that contains an [IDENTIFIER] token inside.
 */
interface FunlElementWithIdentifier : PsiElement {

    /**
     * Quick access to the identifier.
     */
    fun getIdentifier(): PsiElement?

    /**
     * Return name (out of identifier).
     */
    fun getName(): String?
}

/**
 * An interface for every [PsiNameIdentifierOwner] ([PsiNamedElement]) in Funl language.
 */
interface FunlNamedElement : FunlElementWithIdentifier, PsiNameIdentifierOwner

/**
 * Basic implementation of [PsiPolyVariantReference].
 */
abstract class FunlBasicReferenceElement(node: ASTNode) :
        ASTWrapperPsiElement(node),
        PsiElement,
        PsiPolyVariantReference,
        EmptyResolveMessageProvider {

    final override fun resolve() = multiResolve(false).takeIf { it.size == 1 }?.first()?.element

    final override fun isReferenceTo(anotherElement: PsiElement) =
            multiResolve(false).firstOrNull { element.manager.areElementsEquivalent(it.element, anotherElement) } != null

    final override fun getUnresolvedMessagePattern() = TEXT_ERROR_UNRESOLVED_REFERENCE_TEMPLATE

    final override fun getElement() = this

    final override fun getRangeInElement() = getManipulator().getRangeInElement(element)

    final override fun bindToElement(element: PsiElement): PsiElement =
            unreachableCode("Rebind cannot be performed for ${this::class}")

    final override fun getCanonicalText() = rangeInElement.substring(element.text)

    final override fun handleElementRename(newElementName: String?): PsiElement? =
            getManipulator().handleContentChange(element, rangeInElement, newElementName)

    final override fun isSoft() = false

    private fun getManipulator() = ElementManipulators.getManipulator(element)
}

/**
 * An [ElementManipulator] implementation for implementers of [FunlBasicReferenceElement].
 */
class FunlElementWithIdentifierManipulator : AbstractElementManipulator<FunlElementWithIdentifier>() {

    override fun handleContentChange(
            element: FunlElementWithIdentifier,
            range: TextRange,
            newContent: String?): FunlElementWithIdentifier? {

        with(TextRange(0, element.textLength)) {
            assert(this == range) { "The given range $range does not match with the expected range $this" }
        }

        newContent?.let {
            return renameElement(element, it)
        }

        return null
    }
}
