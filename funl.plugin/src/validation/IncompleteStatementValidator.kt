package org.ddol.idea.plugin.funl.validation

import com.intellij.psi.PsiElement
import com.intellij.psi.tree.TokenSet.create
import org.ddol.idea.plugin.funl.FunlParserDefinition.Companion.BLOCK_BODY_KEYWORD_TOKENS
import org.ddol.idea.plugin.funl.FunlParserDefinition.Companion.FUNL_FILE
import org.ddol.idea.plugin.funl.FunlParserDefinition.Companion.PROGRAM_KEYWORD_TOKENS
import org.ddol.idea.plugin.funl.Texts.TEXT_ERROR_INCOMPLETE_STATEMENT
import org.ddol.idea.plugin.funl.psi.FunlIncomplete
import org.ddol.idea.plugin.funl.psi.FunlTokenType
import org.ddol.idea.plugin.funl.psi.FunlTypes.BLOCK_BODY
import org.ddol.idea.plugin.funl.psi.FunlTypes.INCOMPLETE
import org.ddol.idea.plugin.funl.psi.findFirstLeafElement
import org.ddol.idea.plugin.funl.unreachableCode
import org.ddol.idea.plugin.funl.validation.FunlIssueLevel.ERROR

/**
 * Validator: Report every incomplete statement.
 */
object IncompleteStatementValidator : FunlValidator<FunlIncomplete> {

    override val matchingElementTypes = create(INCOMPLETE)

    private val PROGRAM_EXPECTED_TOKENS = PROGRAM_KEYWORD_TOKENS.types.map { (it as FunlTokenType).shortName }.toList()
    private val BLOCK_BODY_EXPECTED_TOKENS = BLOCK_BODY_KEYWORD_TOKENS.types.map { (it as FunlTokenType).shortName }.toList()

    override fun validate(element: FunlIncomplete, sink: FunlIssueSink) {

        val firstLeafElement = findFirstLeafElement(element)!!
        val message = TEXT_ERROR_INCOMPLETE_STATEMENT(firstLeafElement.node.elementType.toString(), getExpectedTokens(element))

        sink.addIssue(FunlIssue(ERROR, firstLeafElement, message))
    }

    private fun getExpectedTokens(element: PsiElement) = when (element.parent.node.elementType) {
        FUNL_FILE -> PROGRAM_EXPECTED_TOKENS
        BLOCK_BODY -> BLOCK_BODY_EXPECTED_TOKENS
        else -> unreachableCode()
    }
}
