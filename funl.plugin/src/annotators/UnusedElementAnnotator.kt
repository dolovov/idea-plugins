package org.ddol.idea.plugin.funl.annotators

import com.intellij.codeInsight.intention.IntentionAction
import com.intellij.psi.PsiElement
import org.ddol.idea.plugin.funl.validation.UnusedElementValidator
import org.ddol.idea.plugin.funl.validation.UnusedElementValidator.SUGGESTION_DELETE_ELEMENT
import org.ddol.idea.plugin.funl.navigation.FunlNamedElement

/**
 * Annotator: Detects and highlights as warning [FunlNamedElement]s that are in fact
 * not used by Funl program.
 */
class UnusedElementAnnotator : FunlValidatorAdapter<FunlNamedElement>() {

    override val elementClass = FunlNamedElement::class
    override val validator = UnusedElementValidator

    override fun produceFixes(alias: String, element: PsiElement, consume: (IntentionAction) -> Unit) {
        if (alias == SUGGESTION_DELETE_ELEMENT) consume(DeleteElementFix(element))
    }
}
