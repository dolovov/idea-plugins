package org.ddol.idea.plugin.funl.annotators

import org.ddol.idea.plugin.funl.validation.DuplicatedAndShadowedDeclarationsValidator
import org.ddol.idea.plugin.funl.psi.FunlFile
import org.ddol.idea.plugin.funl.psi.FunlParameter
import org.ddol.idea.plugin.funl.psi.FunlValue

/**
 * Annotator:
 * - Detects and highlights as error duplicated declarations in Funl program.
 * - Detects and highlights as warning name shadowing ([FunlValue]s/[FunlParameter]s).
 */
class DuplicatedAndShadowedDeclarationsAnnotator : FunlValidatorAdapter<FunlFile>() {
    override val elementClass = FunlFile::class
    override val validator = DuplicatedAndShadowedDeclarationsValidator
}
