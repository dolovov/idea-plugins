package org.ddol.idea.plugin.funl.annotators

import com.intellij.codeInsight.intention.IntentionAction
import com.intellij.lang.annotation.AnnotationHolder
import com.intellij.psi.PsiElement
import org.ddol.idea.plugin.funl.validation.ReturnStatementIssueValidator
import org.ddol.idea.plugin.funl.validation.ReturnStatementIssueValidator.SUGGESTION_INSERT_RETURN_STATEMENT
import org.ddol.idea.plugin.funl.psi.*

/**
 * Annotator: Detect duplicated, missed or misplaced [FunlReturn] statements inside [FunlBlockBody].
 */
class ReturnStatementIssueAnnotator : FunlValidatorAdapter<FunlFunction>() {
    override val elementClass = FunlFunction::class
    override val validator = ReturnStatementIssueValidator

    override fun produceFixes(alias: String, element: PsiElement, consume: (IntentionAction) -> Unit) {
        if (alias == SUGGESTION_INSERT_RETURN_STATEMENT) consume(FunctionBlockBodyWithoutReturnStatementFix(element))
    }

    override fun annotate(element: PsiElement, holder: AnnotationHolder) {
        if (element !is FunlFile)
            return

        // get all the functions inside of the file, because function elements are not supplied to annotators (why?)
        getFunctions(element).forEach { super.annotate(it, holder) }
    }
}
