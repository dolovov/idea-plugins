package org.ddol.idea.plugin.funl.navigation

import com.intellij.lang.documentation.DocumentationProvider
import com.intellij.lang.documentation.DocumentationProviderEx
import com.intellij.psi.PsiElement
import org.ddol.idea.plugin.funl.Icons.ICON_FUNCTION
import org.ddol.idea.plugin.funl.Icons.ICON_PARAMETER
import org.ddol.idea.plugin.funl.Icons.ICON_VALUE
import org.ddol.idea.plugin.funl.Texts.TEXT_COMPACT_SIGNATURE_FUNCTION_TEXT
import org.ddol.idea.plugin.funl.Texts.TEXT_COMPACT_SIGNATURE_PARAMETER_TEXT
import org.ddol.idea.plugin.funl.Texts.TEXT_COMPACT_SIGNATURE_VALUE_TEXT
import org.ddol.idea.plugin.funl.Texts.TEXT_SIGNATURE_FUNCTION_HTML
import org.ddol.idea.plugin.funl.Texts.TEXT_SIGNATURE_FUNCTION_TEXT
import org.ddol.idea.plugin.funl.Texts.TEXT_SIGNATURE_PARAMETER_HTML
import org.ddol.idea.plugin.funl.Texts.TEXT_SIGNATURE_PARAMETER_TEXT
import org.ddol.idea.plugin.funl.Texts.TEXT_SIGNATURE_VALUE_HTML
import org.ddol.idea.plugin.funl.Texts.TEXT_SIGNATURE_VALUE_TEXT
import org.ddol.idea.plugin.funl.psi.FunlFunction
import org.ddol.idea.plugin.funl.psi.FunlParameter
import org.ddol.idea.plugin.funl.psi.FunlTypes.*
import org.ddol.idea.plugin.funl.psi.FunlValue
import org.ddol.idea.plugin.funl.unreachableCode

/**
 * Implementation of [DocumentationProvider].
 */
object FunlDocumentationProvider : DocumentationProviderEx() {

    /**
     * Returns HTML that is shown in tip when hovering mouse together with pressed Cmd button
     * over the reference element. The [element] is the [PsiElement] that the reference is resolved to.
     */
    override fun getQuickNavigateInfo(element: PsiElement?, unused: PsiElement?) =
            if (element is FunlNamedElement) getSignatureDescriptionHTML(element) else null

    /**
     * Returns HTML that describes signature of the given [element].
     *
     * See also [getQuickNavigateInfo].
     */
    @Suppress("MemberVisibilityCanBePrivate")
    fun getSignatureDescriptionHTML(element: FunlNamedElement) = when (element) {
        is FunlValue -> TEXT_SIGNATURE_VALUE_HTML(element.name.orEmpty())
        is FunlParameter -> TEXT_SIGNATURE_PARAMETER_HTML(element.name.orEmpty())
        is FunlFunction -> TEXT_SIGNATURE_FUNCTION_HTML(element.name.orEmpty(), element.parameterList.map { it.name.orEmpty() })
        else -> unreachableCode()
    }

    /**
     * Returns plain text that describes signature of the given [element].
     */
    fun getSignatureDescriptionPlain(element: FunlNamedElement) = when (element) {
        is FunlValue -> TEXT_SIGNATURE_VALUE_TEXT(element.name.orEmpty())
        is FunlParameter -> TEXT_SIGNATURE_PARAMETER_TEXT(element.name.orEmpty())
        is FunlFunction -> TEXT_SIGNATURE_FUNCTION_TEXT(element.name.orEmpty(), element.parameterList.map { it.name.orEmpty() })
        else -> unreachableCode()
    }

    /**
     * Returns plain text that describes signature of the given [element] without keywords.
     */
    fun getCompactSignatureDescriptionPlain(element: FunlNamedElement) = when (element) {
        is FunlValue -> TEXT_COMPACT_SIGNATURE_VALUE_TEXT(element.name.orEmpty())
        is FunlParameter -> TEXT_COMPACT_SIGNATURE_PARAMETER_TEXT(element.name.orEmpty())
        is FunlFunction -> TEXT_COMPACT_SIGNATURE_FUNCTION_TEXT(element.name.orEmpty(), element.parameterList.map { it.name.orEmpty() })
        else -> unreachableCode()
    }

    /**
     * Returns the appropriate icon for the given named element.
     */
    fun getIconForNamedElement(element: FunlNamedElement) = when (element.node.elementType) {
        VALUE -> ICON_VALUE
        PARAMETER -> ICON_PARAMETER
        FUNCTION -> ICON_FUNCTION
        else -> unreachableCode()
    }
}
