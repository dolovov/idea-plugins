package org.ddol.idea.plugin.funl

import com.intellij.icons.AllIcons
import javax.swing.Icon

/**
 * Collection of all [Icon]s used in Funl plugin.
 */
object Icons {

    val ICON_FILE: Icon = AllIcons.Nodes.Function

    val ICON_FUNCTION: Icon = AllIcons.CodeStyle.Gear
    val ICON_VALUE: Icon = AllIcons.Actions.CreateFromUsage
    val ICON_PARAMETER: Icon = AllIcons.Actions.QuickfixOffBulb
    val ICON_OUTPUT: Icon = AllIcons.General.TbHidden
    val ICON_INCOMPLETE_STATEMENT: Icon = AllIcons.RunConfigurations.Unknown
}
