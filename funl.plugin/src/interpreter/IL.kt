package org.ddol.idea.plugin.funl.interpreter

import java.math.BigDecimal

/**
 * An abstract intermediate language (IL) instruction.
 */
sealed class ILInstruction(val code: String) {
    override fun toString() = code
}

/**
 * Call the IL code block previously marked with [ILBodyStart] and [ILBodyEnd] instructions.
 */
class ILCall(val name: String, val parametersCount: Int) : ILInstruction("call") {
    init {
        assertValidName(name)
        assertValidNumberOfParameters(parametersCount)
    }
    override fun toString() = super.toString() + "[name=$name,parametersCount=$parametersCount]"
}

/**
 * Create a new stack frame. Move the given number of operands from current frame to new frame.
 */
class ILCreateFrame(val operandsCount: Int) : ILInstruction("create_frame") {
    init { assertValidNumberOfOperands(operandsCount) }
    override fun toString() = super.toString() + "[operandsCount=$operandsCount]"
}

/**
 * Destroy current stack frame. Move the given number of operands from destroyed frame to previous frame.
 */
class ILDestroyFrame(val operandsCount: Int) : ILInstruction("destroy_frame") {
    init { assertValidNumberOfOperands(operandsCount) }
    override fun toString() = super.toString() + "[operandsCount=$operandsCount]"
}

/**
 * Put value to current stack frame.
 */
class ILPutValue(val name: String) : ILInstruction("put_value") {
    init { assertValidName(name) }
    override fun toString() = super.toString() + "[name=$name]"
}

/**
 * Get value from current stack frame.
 * If it's absent there, then get it from global scope.
 */
class ILGetValue(val name: String) : ILInstruction("get_value") {
    init { assertValidName(name) }
    override fun toString() = super.toString() + "[name=$name]"
}

/**
 * Math binary operations under operands on stack.
 */
object ILSummarize : ILInstruction("summarize")
object ILSubtract : ILInstruction("subtract")
object ILMultiply : ILInstruction("multiply")
object ILDivide : ILInstruction("divide")

/**
 * Math unary operations under operands on stack.
 */
object ILNegate : ILInstruction("negate")

/**
 * Put literal to operand stack.
 */
class ILLiteral(plainValue: String) : ILInstruction("literal") {
    val value = FunlWord(plainValue) // may throw exception
    override fun toString() = super.toString() + "[value=$value]"
}

/**
 * Print the operand from operand stack.
 */
object ILPrint : ILInstruction("print")

/**
 * This instruction signals that all the following instructions will up to [ILBodyEnd] represent a body of
 * some executable IL code block such as main program body or a function.
 */
class ILBodyStart(val name: String, val parametersCount: Int) : ILInstruction("body_start") {
    init {
        assertValidName(name)
        assertValidNumberOfParameters(parametersCount)
    }
    override fun toString() = super.toString() + "[name=$name,parametersCount=$parametersCount]"
}

/**
 * Signals the end of executable IL code block.
 */
object ILBodyEnd : ILInstruction("body_end")

/**
 * The name of the main function that represents main program body.
 */
const val PROGRAM_BODY = "__main__"

/**
 * The maximum number of allowed parameters for function.
 */
const val MAX_FUNCTION_PARAMETERS_COUNT = 32

/**
 * The maximum number of operands to move from current stack frame to a new stack frame.
 */
const val MAX_NUMBER_OF_OPERANDS_TO_MOVE_TO_NEW_FRAME = MAX_FUNCTION_PARAMETERS_COUNT

private fun assertValidNumberOfParameters(parametersCount: Int) {
    assert(parametersCount >= 0) { "Negative number of parameters: $parametersCount." }
    assert(parametersCount <= MAX_FUNCTION_PARAMETERS_COUNT) {
        "Too big number of parameters: $parametersCount. Max permitted is $MAX_FUNCTION_PARAMETERS_COUNT."
    }
}

private fun assertValidNumberOfOperands(operandsCount: Int) {
    assert(operandsCount >= 0) { "Negative number of operands: $operandsCount." }
    assert(operandsCount <= MAX_NUMBER_OF_OPERANDS_TO_MOVE_TO_NEW_FRAME) {
        "Too big number of operands: $operandsCount. Max permitted is $MAX_FUNCTION_PARAMETERS_COUNT."
    }
}

private fun assertValidName(name: String) {
    assert(name.isNotEmpty()) { "Empty name: [$name]." }
    assert(name.firstOrNull { it.isWhitespace() } == null) { "Name should not include whitespace: [$name]." }
}

typealias FunlWord = BigDecimal
