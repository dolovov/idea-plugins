package org.ddol.idea.plugin.funl.navigation

import com.intellij.psi.PsiElement
import org.ddol.idea.plugin.funl.psi.*
import org.ddol.idea.plugin.funl.unreachableCode

/**
 * Represents an [PsiElement]-independent signature for [FunlFunction], [FunlValue] or [FunlParameter].
 *
 * Used for fast searching elements in Funl program.
 */
@Suppress("DataClassPrivateConstructor")
data class ReferencedElementSignature private constructor(
        val isFunction: Boolean,
        val name: String,
        private val parametersCount: Int) {

    init {
        assert(name.isNotEmpty())
        assert(parametersCount >= 0)
        assert(isFunction || parametersCount == 0)
    }

    fun renameTo(newName: String) = ReferencedElementSignature(isFunction, newName, parametersCount)

    companion object {

        /**
         * Create [FunlReferenceExpression] for the given [FunlReferenceExpression].
         */
        fun createSignature(expression: FunlReferenceExpression) = with(expression.parent) {
            // use `with` to allow smart cast for `expression.parent`
            when (this) {

                // reference to a function
                is FunlFunctionCallExpression -> ReferencedElementSignature(
                        true,
                        expression.getName()!!,
                        this.functionCallArguments?.expressionList?.size ?: 0)

                // reference to a value or parameter
                else -> ReferencedElementSignature(false, expression.getName()!!, 0)
            }
        }

        /**
         * Create signature for the given [FunlNamedElement] and run the [block] on it.
         * If no valid signature can be created (ex: name of [FunlNamedElement] is empty), then [block] isn't run.
         */
        fun createValidSignatureOrNull(
                element: FunlNamedElement,
                block: (ReferencedElementSignature) -> Unit = {}): ReferencedElementSignature? {

            val signature = when (element) {
                is FunlValue -> internalCreateValidOrNull(element.name)
                is FunlParameter -> internalCreateValidOrNull(element.name)
                is FunlFunction -> internalCreateValidOrNull(element.name, element.parameterList.size)
                else -> unreachableCode("Unexpected ${FunlNamedElement::class.simpleName} element: $element")
            }

            signature?.let(block)

            return signature
        }

        private fun internalCreateValidOrNull(name: String?, size: Int? = null) = when {
            name.isNullOrEmpty() -> null
            else -> ReferencedElementSignature(size != null, name!!, size ?: 0)
        }
    }
}
