package org.ddol.idea.plugin.funl.tests

import com.intellij.testFramework.builders.EmptyModuleFixtureBuilder
import com.intellij.testFramework.fixtures.CodeInsightFixtureTestCase
import com.intellij.testFramework.fixtures.ModuleFixture
import junitparams.Parameters
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import kotlin.math.max
import kotlin.math.min

/**
 * Test code completion: For a small program in Funl language try to run completion at
 * the every available offset inside of the file and check that the suggested variants
 * match the expected.
 */
@RunWith(AdaptedJUnitParamsRunner::class)
class CodeCompletionTest : CodeInsightFixtureTestCase<EmptyModuleFixtureBuilder<ModuleFixture>>() {

    @Test
    @Parameters
    fun testCompletion(offset: Int, expectedLookupElementStrings: List<String>?) {

        val originalPositionDescription = positionDescriptionAtOffset(offset, "original")

        myFixture.editor.caretModel.moveToOffset(offset)
        myFixture.completeBasic()

        val modifiedPositionDescription = positionDescriptionAtOffset(offset, "modified")

        assertEquals(
                "\n$originalPositionDescription\n$modifiedPositionDescription",
                expectedLookupElementStrings,
                myFixture.lookupElementStrings)
    }

    @Suppress("unused")
    private fun parametersForTestCompletion() = buildParameters(163) { lookupStringsAt ->
        lookupStringsAt[0] = PROGRAM_LEVEL_KEYWORDS
        lookupStringsAt[1] = PROGRAM_LEVEL_KEYWORDS
        lookupStringsAt[23] = PROGRAM_LEVEL_KEYWORDS
        lookupStringsAt[24] = PROGRAM_LEVEL_KEYWORDS
        lookupStringsAt[25] = VALUE_KEYWORD
        lookupStringsAt[26] = VALUE_KEYWORD
        lookupStringsAt[27] = SINGLE_VARIANT_INSERTED_AUTOMATICALLY
        lookupStringsAt[36] = PROGRAM_LEVEL_KEYWORDS
        lookupStringsAt[37] = PROGRAM_LEVEL_KEYWORDS
        lookupStringsAt[38] = FUNC_KEYWORD
        lookupStringsAt[39] = FUNC_KEYWORD
        lookupStringsAt[40] = FUNC_KEYWORD
        lookupStringsAt[41] = SINGLE_VARIANT_INSERTED_AUTOMATICALLY
        lookupStringsAt[50] = s("foo", "x")
        lookupStringsAt[51] = s("foo", "x")
        lookupStringsAt[52] = SINGLE_VARIANT_INSERTED_AUTOMATICALLY
        lookupStringsAt[53] = PROGRAM_LEVEL_KEYWORDS
        lookupStringsAt[54] = PROGRAM_LEVEL_KEYWORDS
        lookupStringsAt[55] = FUNC_KEYWORD
        lookupStringsAt[56] = FUNC_KEYWORD
        lookupStringsAt[57] = FUNC_KEYWORD
        lookupStringsAt[58] = SINGLE_VARIANT_INSERTED_AUTOMATICALLY
        lookupStringsAt[68] = BLOCK_BODY_LEVEL_KEYWORDS
        lookupStringsAt[69] = BLOCK_BODY_LEVEL_KEYWORDS
        lookupStringsAt[70] = BLOCK_BODY_LEVEL_KEYWORDS
        lookupStringsAt[71] = BLOCK_BODY_LEVEL_KEYWORDS
        lookupStringsAt[72] = VALUE_KEYWORD
        lookupStringsAt[73] = VALUE_KEYWORD
        lookupStringsAt[74] = SINGLE_VARIANT_INSERTED_AUTOMATICALLY
        lookupStringsAt[80] = s("bar", "foo", "y")
        lookupStringsAt[81] = s("bar", "foo", "y")
        lookupStringsAt[82] = SINGLE_VARIANT_INSERTED_AUTOMATICALLY
        lookupStringsAt[84] = s("bar", "foo", "y")
        lookupStringsAt[85] = s("bar", "foo", "y")
        lookupStringsAt[87] = BLOCK_BODY_LEVEL_KEYWORDS
        lookupStringsAt[88] = BLOCK_BODY_LEVEL_KEYWORDS
        lookupStringsAt[89] = BLOCK_BODY_LEVEL_KEYWORDS
        lookupStringsAt[90] = BLOCK_BODY_LEVEL_KEYWORDS
        lookupStringsAt[113] = BLOCK_BODY_LEVEL_KEYWORDS
        lookupStringsAt[114] = BLOCK_BODY_LEVEL_KEYWORDS
        lookupStringsAt[115] = BLOCK_BODY_LEVEL_KEYWORDS
        lookupStringsAt[116] = BLOCK_BODY_LEVEL_KEYWORDS
        lookupStringsAt[117] = RETURN_KEYWORD
        lookupStringsAt[118] = RETURN_KEYWORD
        lookupStringsAt[119] = RETURN_KEYWORD
        lookupStringsAt[120] = RETURN_KEYWORD
        lookupStringsAt[121] = RETURN_KEYWORD
        lookupStringsAt[122] = SINGLE_VARIANT_INSERTED_AUTOMATICALLY
        lookupStringsAt[123] = s("bar", "foo", "y", "zzz")
        lookupStringsAt[126] = s("bar", "foo", "y", "zzz")
        lookupStringsAt[127] = s("bar", "foo", "y", "zzz")
        lookupStringsAt[128] = s("zzz")
        lookupStringsAt[129] = s("zzz")
        lookupStringsAt[130] = SINGLE_VARIANT_INSERTED_AUTOMATICALLY
        lookupStringsAt[131] = BLOCK_BODY_LEVEL_KEYWORDS
        lookupStringsAt[133] = PROGRAM_LEVEL_KEYWORDS
        lookupStringsAt[134] = PROGRAM_LEVEL_KEYWORDS
        lookupStringsAt[135] = SAY_KEYWORD
        lookupStringsAt[136] = SAY_KEYWORD
        lookupStringsAt[137] = SINGLE_VARIANT_INSERTED_AUTOMATICALLY
        lookupStringsAt[138] = s("bar", "foo", "test")
        lookupStringsAt[139] = s("bar")
        lookupStringsAt[140] = s("bar")
        lookupStringsAt[141] = SINGLE_VARIANT_INSERTED_AUTOMATICALLY
        lookupStringsAt[142] = s("bar", "foo", "test")
        lookupStringsAt[143] = s("bar", "foo", "test")
        lookupStringsAt[150] = s("bar", "foo", "test")
        lookupStringsAt[151] = s("bar", "foo", "test")
        lookupStringsAt[152] = s( "test")
        lookupStringsAt[153] = s( "test")
        lookupStringsAt[154] = s( "test")
        lookupStringsAt[155] = SINGLE_VARIANT_INSERTED_AUTOMATICALLY
        lookupStringsAt[156] = s("bar", "foo", "test")
        lookupStringsAt[157] = s("bar", "foo", "test")
        lookupStringsAt[163] = PROGRAM_LEVEL_KEYWORDS
    }

    @Before
    fun beforeTest(){
        setUp()
        myFixture.configureByFile(testFilePath("CodeCompletionTest.funl"))
    }

    @After
    fun afterTest() = tearDown()

    private fun positionDescriptionAtOffset(offset: Int, alias: String): String {
        val position = myFixture.editor.offsetToLogicalPosition(offset)

        var text = myFixture.file.text.substring(offset)
        text = text.substring(0, min(text.length, 10)).replace("\n", "\\n")

        return "${myFixture.file.virtualFile.name} ($alias) [$offset -> ${position.line + 1}:${position.column + 1}]:$text..."
    }
}

private fun buildParameters(continueAtLeastTillOffset: Int, block: (MutableMap<Int, List<String>?>) -> Unit): Array<Any> {

    val parameters = mutableMapOf<Int, List<String>?>()
    block(parameters) // fill in all explicitly specified parameters

    if (parameters.isNotEmpty()) {
        val minOffset = parameters.keys.min()!!
        assert(minOffset >= 0) { "Invalid offset in completion test parameters: $minOffset" }
    }

    // complement them with missed empty parameters:
    for (i in 0..max(parameters.keys.max() ?: 0, continueAtLeastTillOffset)) {
        if (i !in parameters)
            parameters[i] = NO_VARIANTS
    }

    return parameters.map { entry -> arrayOf(entry.key, entry.value) }.sortedBy { it[0] as Int }.toTypedArray()
}

private val PROGRAM_LEVEL_KEYWORDS = s("func", "val", "say")
private val BLOCK_BODY_LEVEL_KEYWORDS = s("val", "say", "return")
private val VALUE_KEYWORD = s("val")
private val FUNC_KEYWORD = s("func")
private val RETURN_KEYWORD = s("return")
private val SAY_KEYWORD = s("say")
private val NO_VARIANTS = s()
private val SINGLE_VARIANT_INSERTED_AUTOMATICALLY = null as List<String>?

// just shortener:
private fun s(vararg elements: String) = listOf(*elements)
