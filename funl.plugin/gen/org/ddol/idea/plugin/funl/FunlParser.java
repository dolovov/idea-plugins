// This is a generated file. Not intended for manual editing.
package org.ddol.idea.plugin.funl;

import com.intellij.lang.PsiBuilder;
import com.intellij.lang.PsiBuilder.Marker;
import static org.ddol.idea.plugin.funl.psi.FunlTypes.*;
import static org.ddol.idea.plugin.funl.psi.FunlParserUtil.*;
import com.intellij.psi.tree.IElementType;
import com.intellij.lang.ASTNode;
import com.intellij.psi.tree.TokenSet;
import com.intellij.lang.PsiParser;
import com.intellij.lang.LightPsiParser;

@SuppressWarnings({"SimplifiableIfStatement", "UnusedAssignment"})
public class FunlParser implements PsiParser, LightPsiParser {

  public ASTNode parse(IElementType t, PsiBuilder b) {
    parseLight(t, b);
    return b.getTreeBuilt();
  }

  public void parseLight(IElementType t, PsiBuilder b) {
    boolean r;
    b = adapt_builder_(t, b, this, EXTENDS_SETS_);
    Marker m = enter_section_(b, 0, _COLLAPSE_, null);
    if (t == BLOCK_BODY) {
      r = block_body(b, 0);
    }
    else if (t == EXPRESSION) {
      r = expression(b, 0, -1);
    }
    else if (t == FUNCTION) {
      r = function(b, 0);
    }
    else if (t == FUNCTION_CALL_ARGUMENTS) {
      r = function_call_arguments(b, 0);
    }
    else if (t == INCOMPLETE) {
      r = incomplete(b, 0);
    }
    else if (t == OUTPUT) {
      r = output(b, 0);
    }
    else if (t == PARAMETER) {
      r = parameter(b, 0);
    }
    else if (t == RETURN) {
      r = return_$(b, 0);
    }
    else if (t == VALUE) {
      r = value(b, 0);
    }
    else {
      r = parse_root_(t, b, 0);
    }
    exit_section_(b, 0, m, t, r, true, TRUE_CONDITION);
  }

  protected boolean parse_root_(IElementType t, PsiBuilder b, int l) {
    return program(b, l + 1);
  }

  public static final TokenSet[] EXTENDS_SETS_ = new TokenSet[] {
    create_token_set_(DIFFERENCE_EXPRESSION, EXPRESSION, FUNCTION_CALL_EXPRESSION, LITERAL_EXPRESSION,
      PARENTHESES_EXPRESSION, PRODUCT_EXPRESSION, QUOTIENT_EXPRESSION, REFERENCE_EXPRESSION,
      SUM_EXPRESSION, UNARY_MINUS_EXPRESSION, UNARY_PLUS_EXPRESSION),
  };

  /* ********************************************************** */
  // !( 'val' | 'func' | 'say' | <<isModeOn "BLOCK_BODY">> ( 'return' | '}' ) | <<eof>> )
  static boolean any_body_statement_recover(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "any_body_statement_recover")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NOT_);
    r = !any_body_statement_recover_0(b, l + 1);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  // 'val' | 'func' | 'say' | <<isModeOn "BLOCK_BODY">> ( 'return' | '}' ) | <<eof>>
  private static boolean any_body_statement_recover_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "any_body_statement_recover_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, KEYWORD_VAL);
    if (!r) r = consumeToken(b, KEYWORD_FUNC);
    if (!r) r = consumeToken(b, KEYWORD_SAY);
    if (!r) r = any_body_statement_recover_0_3(b, l + 1);
    if (!r) r = eof(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  // <<isModeOn "BLOCK_BODY">> ( 'return' | '}' )
  private static boolean any_body_statement_recover_0_3(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "any_body_statement_recover_0_3")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = isModeOn(b, l + 1, "BLOCK_BODY");
    r = r && any_body_statement_recover_0_3_1(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  // 'return' | '}'
  private static boolean any_body_statement_recover_0_3_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "any_body_statement_recover_0_3_1")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, KEYWORD_RETURN);
    if (!r) r = consumeToken(b, BRACE_CLOSE);
    exit_section_(b, m, null, r);
    return r;
  }

  /* ********************************************************** */
  // '{' <<enterMode "BLOCK_BODY">> ( value | output | return | incomplete )+ '}' <<exitMode "BLOCK_BODY">>
  public static boolean block_body(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "block_body")) return false;
    if (!nextTokenIs(b, BRACE_OPEN)) return false;
    boolean r, p;
    Marker m = enter_section_(b, l, _NONE_, BLOCK_BODY, null);
    r = consumeToken(b, BRACE_OPEN);
    p = r; // pin = 1
    r = r && report_error_(b, enterMode(b, l + 1, "BLOCK_BODY"));
    r = p && report_error_(b, block_body_2(b, l + 1)) && r;
    r = p && report_error_(b, consumeToken(b, BRACE_CLOSE)) && r;
    r = p && exitMode(b, l + 1, "BLOCK_BODY") && r;
    exit_section_(b, l, m, r, p, null);
    return r || p;
  }

  // ( value | output | return | incomplete )+
  private static boolean block_body_2(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "block_body_2")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = block_body_2_0(b, l + 1);
    int c = current_position_(b);
    while (r) {
      if (!block_body_2_0(b, l + 1)) break;
      if (!empty_element_parsed_guard_(b, "block_body_2", c)) break;
      c = current_position_(b);
    }
    exit_section_(b, m, null, r);
    return r;
  }

  // value | output | return | incomplete
  private static boolean block_body_2_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "block_body_2_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = value(b, l + 1);
    if (!r) r = output(b, l + 1);
    if (!r) r = return_$(b, l + 1);
    if (!r) r = incomplete(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  /* ********************************************************** */
  // '=' expression
  static boolean expression_body(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "expression_body")) return false;
    if (!nextTokenIs(b, OP_EQ)) return false;
    boolean r, p;
    Marker m = enter_section_(b, l, _NONE_);
    r = consumeToken(b, OP_EQ);
    p = r; // pin = 1
    r = r && expression(b, l + 1, -1);
    exit_section_(b, l, m, r, p, null);
    return r || p;
  }

  /* ********************************************************** */
  // signature ( expression_body | block_body )
  public static boolean function(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "function")) return false;
    if (!nextTokenIs(b, KEYWORD_FUNC)) return false;
    boolean r, p;
    Marker m = enter_section_(b, l, _NONE_, FUNCTION, null);
    r = signature(b, l + 1);
    p = r; // pin = 1
    r = r && function_1(b, l + 1);
    exit_section_(b, l, m, r, p, null);
    return r || p;
  }

  // expression_body | block_body
  private static boolean function_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "function_1")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = expression_body(b, l + 1);
    if (!r) r = block_body(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  /* ********************************************************** */
  // '(' ( expression ( ',' expression)* )? ')'
  public static boolean function_call_arguments(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "function_call_arguments")) return false;
    if (!nextTokenIs(b, PAREN_OPEN)) return false;
    boolean r, p;
    Marker m = enter_section_(b, l, _NONE_, FUNCTION_CALL_ARGUMENTS, null);
    r = consumeToken(b, PAREN_OPEN);
    p = r; // pin = 1
    r = r && report_error_(b, function_call_arguments_1(b, l + 1));
    r = p && consumeToken(b, PAREN_CLOSE) && r;
    exit_section_(b, l, m, r, p, null);
    return r || p;
  }

  // ( expression ( ',' expression)* )?
  private static boolean function_call_arguments_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "function_call_arguments_1")) return false;
    function_call_arguments_1_0(b, l + 1);
    return true;
  }

  // expression ( ',' expression)*
  private static boolean function_call_arguments_1_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "function_call_arguments_1_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = expression(b, l + 1, -1);
    r = r && function_call_arguments_1_0_1(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  // ( ',' expression)*
  private static boolean function_call_arguments_1_0_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "function_call_arguments_1_0_1")) return false;
    int c = current_position_(b);
    while (true) {
      if (!function_call_arguments_1_0_1_0(b, l + 1)) break;
      if (!empty_element_parsed_guard_(b, "function_call_arguments_1_0_1", c)) break;
      c = current_position_(b);
    }
    return true;
  }

  // ',' expression
  private static boolean function_call_arguments_1_0_1_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "function_call_arguments_1_0_1_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, COMMA);
    r = r && expression(b, l + 1, -1);
    exit_section_(b, m, null, r);
    return r;
  }

  /* ********************************************************** */
  // <<incompleteStatementToken>>
  public static boolean incomplete(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "incomplete")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, INCOMPLETE, "<incomplete>");
    r = incompleteStatementToken(b, l + 1);
    exit_section_(b, l, m, r, false, any_body_statement_recover_parser_);
    return r;
  }

  /* ********************************************************** */
  // 'say' expression
  public static boolean output(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "output")) return false;
    if (!nextTokenIs(b, KEYWORD_SAY)) return false;
    boolean r, p;
    Marker m = enter_section_(b, l, _NONE_, OUTPUT, null);
    r = consumeToken(b, KEYWORD_SAY);
    p = r; // pin = 1
    r = r && expression(b, l + 1, -1);
    exit_section_(b, l, m, r, p, null);
    return r || p;
  }

  /* ********************************************************** */
  // identifier
  public static boolean parameter(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "parameter")) return false;
    if (!nextTokenIs(b, IDENTIFIER)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, IDENTIFIER);
    exit_section_(b, m, PARAMETER, r);
    return r;
  }

  /* ********************************************************** */
  // ( function | value | output | incomplete )+
  static boolean program(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "program")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = program_0(b, l + 1);
    int c = current_position_(b);
    while (r) {
      if (!program_0(b, l + 1)) break;
      if (!empty_element_parsed_guard_(b, "program", c)) break;
      c = current_position_(b);
    }
    exit_section_(b, m, null, r);
    return r;
  }

  // function | value | output | incomplete
  private static boolean program_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "program_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = function(b, l + 1);
    if (!r) r = value(b, l + 1);
    if (!r) r = output(b, l + 1);
    if (!r) r = incomplete(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  /* ********************************************************** */
  // 'return' expression
  public static boolean return_$(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "return_$")) return false;
    if (!nextTokenIs(b, KEYWORD_RETURN)) return false;
    boolean r, p;
    Marker m = enter_section_(b, l, _NONE_, RETURN, null);
    r = consumeToken(b, KEYWORD_RETURN);
    p = r; // pin = 1
    r = r && expression(b, l + 1, -1);
    exit_section_(b, l, m, r, p, null);
    return r || p;
  }

  /* ********************************************************** */
  // 'func' identifier '(' ( parameter (',' parameter)* )? ')'
  static boolean signature(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "signature")) return false;
    boolean r, p;
    Marker m = enter_section_(b, l, _NONE_);
    r = consumeTokens(b, 1, KEYWORD_FUNC, IDENTIFIER, PAREN_OPEN);
    p = r; // pin = 1
    r = r && report_error_(b, signature_3(b, l + 1));
    r = p && consumeToken(b, PAREN_CLOSE) && r;
    exit_section_(b, l, m, r, p, signature_recover_parser_);
    return r || p;
  }

  // ( parameter (',' parameter)* )?
  private static boolean signature_3(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "signature_3")) return false;
    signature_3_0(b, l + 1);
    return true;
  }

  // parameter (',' parameter)*
  private static boolean signature_3_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "signature_3_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = parameter(b, l + 1);
    r = r && signature_3_0_1(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  // (',' parameter)*
  private static boolean signature_3_0_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "signature_3_0_1")) return false;
    int c = current_position_(b);
    while (true) {
      if (!signature_3_0_1_0(b, l + 1)) break;
      if (!empty_element_parsed_guard_(b, "signature_3_0_1", c)) break;
      c = current_position_(b);
    }
    return true;
  }

  // ',' parameter
  private static boolean signature_3_0_1_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "signature_3_0_1_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, COMMA);
    r = r && parameter(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  /* ********************************************************** */
  // !( 'val' | 'func' | 'say' | '{' | '=' | <<eof>> )
  static boolean signature_recover(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "signature_recover")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NOT_);
    r = !signature_recover_0(b, l + 1);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  // 'val' | 'func' | 'say' | '{' | '=' | <<eof>>
  private static boolean signature_recover_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "signature_recover_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, KEYWORD_VAL);
    if (!r) r = consumeToken(b, KEYWORD_FUNC);
    if (!r) r = consumeToken(b, KEYWORD_SAY);
    if (!r) r = consumeToken(b, BRACE_OPEN);
    if (!r) r = consumeToken(b, OP_EQ);
    if (!r) r = eof(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  /* ********************************************************** */
  // 'val' identifier expression_body
  public static boolean value(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "value")) return false;
    if (!nextTokenIs(b, KEYWORD_VAL)) return false;
    boolean r, p;
    Marker m = enter_section_(b, l, _NONE_, VALUE, null);
    r = consumeTokens(b, 1, KEYWORD_VAL, IDENTIFIER);
    p = r; // pin = 1
    r = r && expression_body(b, l + 1);
    exit_section_(b, l, m, r, p, null);
    return r || p;
  }

  /* ********************************************************** */
  // Expression root: expression
  // Operator priority table:
  // 0: BINARY(sum_expression) BINARY(difference_expression)
  // 1: BINARY(product_expression) BINARY(quotient_expression)
  // 2: ATOM(unary_minus_expression) ATOM(unary_plus_expression)
  // 3: ATOM(literal_expression) ATOM(parentheses_expression) POSTFIX(function_call_expression) ATOM(reference_expression)
  public static boolean expression(PsiBuilder b, int l, int g) {
    if (!recursion_guard_(b, l, "expression")) return false;
    addVariant(b, "<expression>");
    boolean r, p;
    Marker m = enter_section_(b, l, _NONE_, "<expression>");
    r = unary_minus_expression(b, l + 1);
    if (!r) r = unary_plus_expression(b, l + 1);
    if (!r) r = literal_expression(b, l + 1);
    if (!r) r = parentheses_expression(b, l + 1);
    if (!r) r = reference_expression(b, l + 1);
    p = r;
    r = r && expression_0(b, l + 1, g);
    exit_section_(b, l, m, null, r, p, null);
    return r || p;
  }

  public static boolean expression_0(PsiBuilder b, int l, int g) {
    if (!recursion_guard_(b, l, "expression_0")) return false;
    boolean r = true;
    while (true) {
      Marker m = enter_section_(b, l, _LEFT_, null);
      if (g < 0 && consumeTokenSmart(b, OP_PLUS)) {
        r = expression(b, l, 0);
        exit_section_(b, l, m, SUM_EXPRESSION, r, true, null);
      }
      else if (g < 0 && consumeTokenSmart(b, OP_MINUS)) {
        r = expression(b, l, 0);
        exit_section_(b, l, m, DIFFERENCE_EXPRESSION, r, true, null);
      }
      else if (g < 1 && consumeTokenSmart(b, OP_MULT)) {
        r = expression(b, l, 1);
        exit_section_(b, l, m, PRODUCT_EXPRESSION, r, true, null);
      }
      else if (g < 1 && consumeTokenSmart(b, OP_DIV)) {
        r = expression(b, l, 1);
        exit_section_(b, l, m, QUOTIENT_EXPRESSION, r, true, null);
      }
      else if (g < 3 && leftMarkerIs(b, REFERENCE_EXPRESSION) && function_call_arguments(b, l + 1)) {
        r = true;
        exit_section_(b, l, m, FUNCTION_CALL_EXPRESSION, r, true, null);
      }
      else {
        exit_section_(b, l, m, null, false, false, null);
        break;
      }
    }
    return r;
  }

  // '-' expression
  public static boolean unary_minus_expression(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "unary_minus_expression")) return false;
    if (!nextTokenIsSmart(b, OP_MINUS)) return false;
    boolean r, p;
    Marker m = enter_section_(b, l, _NONE_, UNARY_MINUS_EXPRESSION, "<expression>");
    r = consumeTokenSmart(b, OP_MINUS);
    p = r; // pin = 1
    r = r && expression(b, l + 1, -1);
    exit_section_(b, l, m, r, p, null);
    return r || p;
  }

  // '+' expression
  public static boolean unary_plus_expression(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "unary_plus_expression")) return false;
    if (!nextTokenIsSmart(b, OP_PLUS)) return false;
    boolean r, p;
    Marker m = enter_section_(b, l, _NONE_, UNARY_PLUS_EXPRESSION, "<expression>");
    r = consumeTokenSmart(b, OP_PLUS);
    p = r; // pin = 1
    r = r && expression(b, l + 1, -1);
    exit_section_(b, l, m, r, p, null);
    return r || p;
  }

  // number
  public static boolean literal_expression(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "literal_expression")) return false;
    if (!nextTokenIsSmart(b, NUMBER)) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, LITERAL_EXPRESSION, "<expression>");
    r = consumeTokenSmart(b, NUMBER);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  // '(' expression ')'
  public static boolean parentheses_expression(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "parentheses_expression")) return false;
    if (!nextTokenIsSmart(b, PAREN_OPEN)) return false;
    boolean r, p;
    Marker m = enter_section_(b, l, _NONE_, PARENTHESES_EXPRESSION, "<expression>");
    r = consumeTokenSmart(b, PAREN_OPEN);
    p = r; // pin = 1
    r = r && report_error_(b, expression(b, l + 1, -1));
    r = p && consumeToken(b, PAREN_CLOSE) && r;
    exit_section_(b, l, m, r, p, null);
    return r || p;
  }

  // identifier
  public static boolean reference_expression(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "reference_expression")) return false;
    if (!nextTokenIsSmart(b, IDENTIFIER)) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, REFERENCE_EXPRESSION, "<expression>");
    r = consumeTokenSmart(b, IDENTIFIER);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  final static Parser any_body_statement_recover_parser_ = new Parser() {
    public boolean parse(PsiBuilder b, int l) {
      return any_body_statement_recover(b, l + 1);
    }
  };
  final static Parser signature_recover_parser_ = new Parser() {
    public boolean parse(PsiBuilder b, int l) {
      return signature_recover(b, l + 1);
    }
  };
}
