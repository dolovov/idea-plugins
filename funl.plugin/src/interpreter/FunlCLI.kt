package org.ddol.idea.plugin.funl.interpreter

import com.intellij.core.CoreASTFactory
import com.intellij.core.CoreApplicationEnvironment
import com.intellij.core.CoreApplicationEnvironment.registerExtensionPoint
import com.intellij.core.CoreProjectEnvironment
import com.intellij.lang.LanguageASTFactory
import com.intellij.lang.LanguageParserDefinitions
import com.intellij.lang.MetaLanguage
import com.intellij.openapi.Disposable
import com.intellij.openapi.extensions.Extensions
import com.intellij.psi.FileTypeFileViewProviders
import com.intellij.psi.FileViewProviderFactory
import com.intellij.psi.PsiFileFactory
import com.intellij.psi.SingleRootFileViewProvider
import com.intellij.psi.impl.search.PsiSearchHelperImpl
import com.intellij.psi.search.PsiSearchHelper
import com.intellij.psi.search.searches.ReferencesSearch
import com.intellij.util.LocalTimeCounter
import org.ddol.idea.plugin.funl.FunlFileType
import org.ddol.idea.plugin.funl.FunlLanguage
import org.ddol.idea.plugin.funl.FunlParserDefinition
import org.ddol.idea.plugin.funl.psi.FunlFile
import java.io.File
import kotlin.system.exitProcess
import kotlin.text.Charsets.UTF_8

/**
 * Command-line interface for running Funl interpreter.
 */
fun main(args: Array<String>) {

    // check basic preconditions
    val file = checkBasicPreconditions(args)

    // parse Funl program, build PSI tree
    val funlFile = parseFunlProgram(file)

    // generate IL by the given PSI tree
    val instructions = generateIL(funlFile, standardJVMOutput) { exitProcess(it) }

    // execute IL
    executeIL(instructions, standardJVMOutput)
}

private fun checkBasicPreconditions(args: Array<String>): File {
    if (args.size != 1)
        printPreconditionFailedMessageAndExit("Exactly one command-line argument is expected")

    val filename = args[0]
    if (filename.isBlank() || !filename.endsWith(".${FunlFileType.defaultExtension}"))
        printPreconditionFailedMessageAndExit("Invalid Funl file name: $filename")

    val file = File(filename)
    if (!file.exists())
        printPreconditionFailedMessageAndExit("File does not exist: $filename")

    if (!file.isFile)
        printPreconditionFailedMessageAndExit("Not a file: $filename")

    if (!file.canRead())
        printPreconditionFailedMessageAndExit("Not enough permissions to read the file: $filename")

    return file
}

private fun parseFunlProgram(file: File): FunlFile {

    registerExtensionPoint(Extensions.getRootArea(), MetaLanguage.EP_NAME, MetaLanguage::class.java)
    registerExtensionPoint(Extensions.getRootArea(), ReferencesSearch.EP_NAME.name, ReferencesSearch::class.java)

    val disposable = Disposable {}
    val appEnvironment = CoreApplicationEnvironment(disposable)

    appEnvironment.addExplicitExtension(LanguageASTFactory.INSTANCE, FunlLanguage, CoreASTFactory())
    appEnvironment.addExplicitExtension(LanguageParserDefinitions.INSTANCE, FunlLanguage, FunlParserDefinition())

    appEnvironment.registerFileType(FunlFileType, FunlFileType.defaultExtension)
    appEnvironment.addExplicitExtension(FileTypeFileViewProviders.INSTANCE, FunlFileType, FileViewProviderFactory {
        virtualFile, _, manager, eventSystemEnabled -> SingleRootFileViewProvider(manager, virtualFile, eventSystemEnabled) })

    val projectEnvironment = CoreProjectEnvironment(disposable, appEnvironment)
    projectEnvironment.project.registerService(PsiSearchHelper::class.java, PsiSearchHelperImpl::class.java)

    val psiFile = PsiFileFactory.getInstance(projectEnvironment.project).createFileFromText(
            file.name,
            FunlFileType,
            file.readText(UTF_8),
            LocalTimeCounter.currentTime(),
            true)

    if (psiFile is FunlFile)
        return psiFile
    else
       printErrorMessageAndExit("Internal error: Got $psiFile instead of ${FunlFile::class}")
}

private fun printPreconditionFailedMessageAndExit(message: String): Nothing {
    printErrorMessage(message)
    System.err.println("Usage: org.ddol.idea.plugin.funl.interpreter.Interpreter <filename.funl> ")
    exitProcess(1)
}

private fun printErrorMessageAndExit(message: String): Nothing {
    printErrorMessage(message)
    exitProcess(1)
}

private fun printErrorMessage(message: String) = System.err.println("Error: $message")
