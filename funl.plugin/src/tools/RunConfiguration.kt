package org.ddol.idea.plugin.funl.tools

import com.intellij.execution.DefaultExecutionResult
import com.intellij.execution.ExecutionResult
import com.intellij.execution.Executor
import com.intellij.execution.configurations.*
import com.intellij.execution.impl.ConsoleViewImpl
import com.intellij.execution.process.NopProcessHandler
import com.intellij.execution.process.ProcessOutputTypes
import com.intellij.execution.runners.ExecutionEnvironment
import com.intellij.execution.runners.ProgramRunner
import com.intellij.openapi.application.ReadAction
import com.intellij.openapi.fileChooser.FileChooser.chooseFile
import com.intellij.openapi.fileChooser.FileChooserDescriptorFactory.createSingleFileDescriptor
import com.intellij.openapi.fileEditor.FileEditorManager
import com.intellij.openapi.options.SettingsEditor
import com.intellij.openapi.progress.EmptyProgressIndicator
import com.intellij.openapi.progress.ProgressIndicator
import com.intellij.openapi.progress.ProgressManager
import com.intellij.openapi.progress.Task
import com.intellij.openapi.project.Project
import com.intellij.openapi.ui.LabeledComponent
import com.intellij.openapi.ui.TextFieldWithBrowseButton
import com.intellij.openapi.util.JDOMExternalizerUtil
import com.intellij.openapi.util.io.FileUtil.toSystemDependentName
import com.intellij.openapi.util.io.FileUtil.toSystemIndependentName
import com.intellij.openapi.vfs.LocalFileSystem
import com.intellij.openapi.vfs.VirtualFile
import com.intellij.psi.PsiFileFactory
import com.intellij.util.ui.JBUI.insetsTop
import org.ddol.idea.plugin.funl.FunlFileType
import org.ddol.idea.plugin.funl.Icons.ICON_FILE
import org.ddol.idea.plugin.funl.Texts.TEXT_RUN_CONFIGURATION_DESCRIPTION
import org.ddol.idea.plugin.funl.Texts.TEXT_RUN_CONFIGURATION_ERROR_NO_FILE_SPECIFIED
import org.ddol.idea.plugin.funl.Texts.TEXT_RUN_CONFIGURATION_FILE_PATH
import org.ddol.idea.plugin.funl.Texts.TEXT_RUN_CONFIGURATION_NAME
import org.ddol.idea.plugin.funl.interpreter.OutputType
import org.ddol.idea.plugin.funl.interpreter.OutputType.STDERR
import org.ddol.idea.plugin.funl.interpreter.OutputType.STDOUT
import org.ddol.idea.plugin.funl.interpreter.executeIL
import org.ddol.idea.plugin.funl.interpreter.generateIL
import org.ddol.idea.plugin.funl.psi.FunlFile
import org.jdom.Element
import java.awt.BorderLayout.WEST
import java.awt.GridBagConstraints
import java.awt.GridBagConstraints.*
import java.awt.GridBagLayout
import java.awt.event.ActionListener
import java.io.CharArrayWriter
import java.io.PrintWriter
import javax.swing.JPanel

/**
 * Run configuration type for Funl programs.
 */
class FunlConfigurationType : ConfigurationTypeBase(
        "FunlConfiguration",
        TEXT_RUN_CONFIGURATION_NAME,
        TEXT_RUN_CONFIGURATION_DESCRIPTION,
        ICON_FILE) {

    init {
        addFactory(FunlConfigurationFactory(this))
    }
}

/**
 * Run configuration factory for Funl programs.
 */
class FunlConfigurationFactory(configurationType: ConfigurationType) : ConfigurationFactory(configurationType) {

    override fun createTemplateConfiguration(project: Project) = FunlRunConfiguration(project, this, "")
}

/**
 * Run configuration for Funl program.
 */
class FunlRunConfiguration(
        project: Project,
        factory: ConfigurationFactory,
        name: String) : LocatableConfigurationBase(project, factory, name) {

    var path: String? = null

    override fun getConfigurationEditor() = FunlSettingsEditor(project)

    override fun checkConfiguration() {
        if (getVirtualFile() == null)
            throw RuntimeConfigurationException(TEXT_RUN_CONFIGURATION_ERROR_NO_FILE_SPECIFIED)
    }

    override fun getState(executor: Executor, environment: ExecutionEnvironment) =
            getVirtualFile()?.let { FunlRunProfileState(project, it) }

    override fun suggestedName() = getVirtualFile()?.name

    override fun writeExternal(element: Element) {
        super.writeExternal(element)
        JDOMExternalizerUtil.writeField(element, ::path.name, path)
    }

    override fun readExternal(element: Element) {
        super.readExternal(element)
        path = JDOMExternalizerUtil.readField(element, ::path.name)
    }

    fun getVirtualFile(): VirtualFile? = path?.let { LocalFileSystem.getInstance().findFileByPath(it) }
}

/**
 * The [SettingsEditor] that allow editing "Funl file path" in run configuration.
 */
@Suppress("JoinDeclarationAndAssignment")
class FunlSettingsEditor(private val project: Project) : SettingsEditor<FunlRunConfiguration>() {

    private val panel: JPanel
    private val pathField: LabeledComponent<TextFieldWithBrowseButton>

    init {
        pathField = LabeledComponent()
        pathField.labelLocation = WEST
        pathField.text = TEXT_RUN_CONFIGURATION_FILE_PATH
        pathField.component = TextFieldWithBrowseButton(
                ActionListener {
                    val toSelect = getVirtualFileFromEditor() ?: getDefaultVirtualFile()
                    val selected = chooseFile(createSingleFileDescriptor(FunlFileType), pathField.component, project, toSelect)
                    if (selected != null)
                        setVirtualFileToEditor(selected)
                },
                this)

        panel = JPanel(GridBagLayout())
        panel.add(pathField, GridBagConstraints(RELATIVE, 1, 1, 1, 1.0, 0.0, NORTHWEST, HORIZONTAL, insetsTop(6), 0, 0))
    }

    override fun createEditor() = panel

    override fun applyEditorTo(configuration: FunlRunConfiguration) {
        configuration.path = getVirtualFileFromEditor()?.path
    }

    override fun resetEditorFrom(configuration: FunlRunConfiguration) = setVirtualFileToEditor(configuration.getVirtualFile())

    private fun getVirtualFileFromEditor(): VirtualFile? {
        val path = toSystemIndependentName(pathField.component.text.trim())
        return if (path.isEmpty()) null else LocalFileSystem.getInstance().findFileByPath(path)
    }

    private fun setVirtualFileToEditor(file: VirtualFile?) {
        pathField.component.text = if (file != null) toSystemDependentName(file.path) else ""
    }

    private fun getDefaultVirtualFile(): VirtualFile? =
            FileEditorManager.getInstance(project).selectedFiles.firstOrNull { it.fileType == FunlFileType }
}

/**
 * Launches Funl interpreter.
 *
 * NOTE: In fact the interpreter is executed just in a new thread. Not in a separate OS process.
 */
class FunlRunProfileState(
        private val project: Project,
        private val file: VirtualFile) : RunProfileState {

    override fun execute(executor: Executor, runner: ProgramRunner<*>): ExecutionResult? {

        val processHandler = NopProcessHandler()
        val output: (OutputType, String) -> Unit = { outputType, message ->

            val processOutputType = when (outputType) {
                STDOUT -> ProcessOutputTypes.STDOUT
                STDERR -> ProcessOutputTypes.STDERR
            }

            processHandler.notifyTextAvailable(message, processOutputType)
        }

        val consoleView = ConsoleViewImpl(project, true)
        consoleView.attachToProcess(processHandler)

        ProgressManager.getInstance().runProcessWithProgressAsynchronously(
                object : Task.Backgroundable(project, "", false, ALWAYS_BACKGROUND) {

                    override fun run(indicator: ProgressIndicator) {
                        ReadAction.run<Exception> {

                            // parse Funl program, build PSI tree
                            val funlFile = PsiFileFactory.getInstance(project).createFileFromText(
                                    file.name,
                                    file.fileType,
                                    String(file.contentsToByteArray(), file.charset),
                                    file.modificationStamp,
                                    true) as FunlFile

                            // generate IL by the given PSI tree
                            val instructions = generateIL(funlFile, output) { indicator.cancel() }

                            // execute IL
                            indicator.checkCanceled()
                            executeIL(instructions, output)
                        }
                    }

                    override fun onThrowable(t: Throwable) {
                        val writer = CharArrayWriter()
                        t.printStackTrace(PrintWriter(writer))
                        output(STDERR, writer.toString())

                        processHandler.destroyProcess()
                    }

                    override fun onSuccess() {
                        processHandler.destroyProcess()
                    }

                    override fun onCancel() {
                        processHandler.destroyProcess()
                    }
                },
                EmptyProgressIndicator())

        return DefaultExecutionResult(consoleView, processHandler)
    }
}
