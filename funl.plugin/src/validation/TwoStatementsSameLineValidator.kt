package org.ddol.idea.plugin.funl.validation

import com.intellij.psi.PsiElement
import org.ddol.idea.plugin.funl.FunlParserDefinition.Companion.ANY_BODY_ALL_STATEMENTS
import org.ddol.idea.plugin.funl.Texts.TEXT_ERROR_TWO_STATEMENTS_SAME_LINE
import org.ddol.idea.plugin.funl.psi.findFirstLeafElement
import org.ddol.idea.plugin.funl.psi.getPreviousSiblingIfNoWhitespaceWithNewLine
import org.ddol.idea.plugin.funl.validation.FunlIssueLevel.ERROR

/**
 * Validator: Reports if there are two subsequent [PsiElement]s located at the same line of code that normally
 * should be located at different lines.
 */
object TwoStatementsSameLineValidator : FunlValidator<PsiElement> {

    const val SUGGESTION_MOVE_ELEMENT_NEXT_LINE = "moveElementToNextLine"

    override val matchingElementTypes = ANY_BODY_ALL_STATEMENTS

    override fun validate(element: PsiElement, sink: FunlIssueSink) {
        if (!isLineExclusiveElement(element))
            return

        val previousElement = getPreviousSiblingIfNoWhitespaceWithNewLine(element) ?: return
        if (isLineExclusiveElement(previousElement))
            sink.addIssue(
                    FunlIssue(ERROR, findFirstLeafElement(element)!!, TEXT_ERROR_TWO_STATEMENTS_SAME_LINE)
                            .withFixSuggestion(SUGGESTION_MOVE_ELEMENT_NEXT_LINE, element))
    }

    private fun isLineExclusiveElement(element: PsiElement): Boolean {
        val elementType = element.node.elementType

        return when {
            matchingElementTypes.contains(elementType) -> true
            else -> false
        }
    }
}
