package org.ddol.idea.plugin.funl.psi

import com.intellij.psi.tree.IElementType
import org.ddol.idea.plugin.funl.FunlLanguage

/**
 * Class that represents each Funl element type.
 */
class FunlElementType(debugName: String) : IElementType(debugName, FunlLanguage)
