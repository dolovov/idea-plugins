// This is a generated file. Not intended for manual editing.
package org.ddol.idea.plugin.funl.psi;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiPolyVariantReference;
import com.intellij.codeInsight.daemon.EmptyResolveMessageProvider;
import org.ddol.idea.plugin.funl.navigation.FunlElementWithIdentifier;
import com.intellij.psi.PsiElementResolveResult;
import org.ddol.idea.plugin.funl.navigation.FunlNamedElement;

public interface FunlReferenceExpression extends FunlExpression, PsiPolyVariantReference, EmptyResolveMessageProvider, FunlElementWithIdentifier {

  @NotNull
  PsiElement getIdentifier();

  boolean isFunctionCall();

  @NotNull
  FunlReferenceExpression getReference();

  @NotNull
  PsiElementResolveResult[] multiResolve(boolean unused);

  @NotNull
  FunlNamedElement[] getVariants();

  @Nullable
  String getName();

}
