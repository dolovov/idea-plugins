package org.ddol.idea.plugin.funl.navigation

import com.intellij.lang.refactoring.RefactoringSupportProvider
import com.intellij.psi.PsiElement

/**
 * Refactoring support for Funl.
 */
class FunlRefactoringSupportProvider : RefactoringSupportProvider() {

    /**
     * Allow inplace renaming of [FunlNamedElement]s.
     */
    override fun isMemberInplaceRenameAvailable(element: PsiElement, unused: PsiElement?) = element is FunlNamedElement

    /**
     * Safe delete of [FunlNamedElement]s.
     */
    override fun isSafeDeleteAvailable(element: PsiElement) = element is FunlNamedElement
}
