package org.ddol.idea.plugin.funl.tests

import com.intellij.testFramework.builders.EmptyModuleFixtureBuilder
import com.intellij.testFramework.fixtures.CodeInsightFixtureTestCase
import com.intellij.testFramework.fixtures.ModuleFixture
import org.ddol.idea.plugin.funl.FunlFileType

/**
 * Tests for renaming in Funl.
 *
 * 12. rename local value with shadowing of global value
 * 13. rename local value with shadowing of parameter
 */
class RenamingTests : CodeInsightFixtureTestCase<EmptyModuleFixtureBuilder<ModuleFixture>>() {

    fun testRenamingOfGlobalValue() = doSuccessfulTest("bar",
            """
                val foo<caret> = 1
                val baz = 2 * foo
                func foo(foo) = foo*baz
                func bar(foo) {
                    val foo = 1
                    return foo
                }
                say foo
            """,
            """
                val bar = 1
                val baz = 2 * bar
                func foo(foo) = foo*baz
                func bar(foo) {
                    val foo = 1
                    return foo
                }
                say bar
            """
    )

    fun testRenamingOfGlobalValueFailsDueToConflict() = doFailedTest("bar",
            """
                val foo<caret> = 1
                val bar = 2
            """,
            "Global value 'bar' is already declared in file"
    )

    fun testRenamingOfGlobalValueFailsDueToShadowing1() = doFailedTest("bar",
            """
                val foo<caret> = 1
                func baz(bar) = foo * 3
            """,
            "'foo' will resolve to another value or parameter after renaming"
    )

    fun testRenamingOfGlobalValueFailsDueToShadowing2() = doFailedTest("bar",
            """
                val foo<caret> = 1
                func baz() {
                    val bar = 2
                    return foo
                }
            """,
            "'foo' will resolve to another value or parameter after renaming"
    )

    fun testRenamingOfFunction1() = doSuccessfulTest("bar",
            """
                val foo = 1
                func foo<caret>(foo) = foo*5
                func baz(foo) = foo*foo(foo)
                say foo+foo(foo)
            """,
            """
                val foo = 1
                func bar(foo) = foo*5
                func baz(foo) = foo*bar(foo)
                say foo+bar(foo)
            """
    )

    fun testRenamingOfFunction2() = doSuccessfulTest("bar",
            """
                val foo = 1
                val bar = 2
                func foo<caret>(foo) = foo*5
                func bar(foo,bar) = foo*bar*foo(foo)
                say foo+foo(foo)+bar(foo,bar)
            """,
            """
                val foo = 1
                val bar = 2
                func bar(foo) = foo*5
                func bar(foo,bar) = foo*bar*bar(foo)
                say foo+bar(foo)+bar(foo,bar)
            """
    )

    fun testRenamingOfFunctionFailsDueToConflict() = doFailedTest("bar",
            """
                func foo<caret>(p1, p2) = 1
                func bar(p1, p2) = 2
            """,
            "Function 'bar' is already declared in file"
    )

    fun testRenamingOfParameter() = doSuccessfulTest("bar",
            """
                val foo = 1
                func foo(foo<caret>) = 5 / foo
            """,
            """
                val foo = 1
                func foo(bar) = 5 / bar
            """
    )

    fun testRenamingOfParameterFailsDueToConflict() = doFailedTest("bar",
            """
                val foo = 1
                func foo(foo<caret>, baz, bar) = 2
            """,
            "Function parameter 'bar' is already declared in function 'foo'"
    )

    fun testRenamingOfParameterFailsDueToShadowing1() = doFailedTest("bar",
            """
                val bar = 1
                func foo(foo<caret>) = bar
            """,
            "'bar' will resolve to another value or parameter after renaming"
    )

    fun testRenamingOfParameterFailsDueToShadowing2() = doFailedTest("bar",
            """
                func foo(foo<caret>) {
                    val bar = 1
                    return foo
                }
            """,
            "'foo' will resolve to another value or parameter after renaming"
    )

    fun testRenamingOfLocalValue() = doSuccessfulTest("bar",
            """
                val foo = 1
                func foo(foo) {
                    val foo<caret> = 2
                    return foo+3
                }
            """,
            """
                val foo = 1
                func foo(foo) {
                    val bar = 2
                    return bar+3
                }
            """
    )

    fun testRenamingOfLocalValueFailsDueToConflict() = doFailedTest("bar",
            """
                func foo() {
                    val foo<caret> = 1
                    val bar = 2
                    return 3
                }
            """,
            "Value 'bar' is already declared in function 'foo'"
    )

    fun testRenamingOfLocalValueFailsDueToShadowing1() = doFailedTest("bar",
            """
                val bar = 1
                func foo() {
                    val foo<caret> = 2
                    return bar
                }
            """,
            "'bar' will resolve to another value or parameter after renaming"
    )

    fun testRenamingOfLocalValueFailsDueToShadowing2() = doFailedTest("bar",
            """
                func foo(bar) {
                    val foo<caret> = 2
                    return bar
                }
            """,
            "'bar' will resolve to another value or parameter after renaming"
    )

    private fun doSuccessfulTest(newName: String, before: String, after: String) {
        myFixture.configureByText(FunlFileType, before.trimIndent())
        myFixture.renameElementAtCaret(newName)
        myFixture.checkResult(after.trimIndent())
    }

    private fun doFailedTest(newName: String, before: String, expectedErrorMessage: String) {
        myFixture.configureByText(FunlFileType, before.trimIndent())
        try {
            myFixture.renameElementAtCaret(newName)
        } catch (e: Exception) {
            if (!e.message!!.startsWith(expectedErrorMessage))
                fail("Exception caught with unexpected error message: $e.\nExpected: $expectedErrorMessage")
            return
        }

        fail("This test should normally fail")
    }
}
