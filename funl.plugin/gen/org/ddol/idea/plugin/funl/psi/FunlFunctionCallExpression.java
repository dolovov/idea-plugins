// This is a generated file. Not intended for manual editing.
package org.ddol.idea.plugin.funl.psi;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.psi.PsiElement;

public interface FunlFunctionCallExpression extends FunlExpression {

  @Nullable
  FunlFunctionCallArguments getFunctionCallArguments();

  @NotNull
  FunlReferenceExpression getReferenceExpression();

}
