package org.ddol.idea.plugin.funl.annotators

import com.intellij.codeInsight.intention.IntentionAction
import com.intellij.lang.annotation.AnnotationHolder
import com.intellij.lang.annotation.Annotator
import com.intellij.lang.annotation.HighlightSeverity
import com.intellij.psi.PsiElement
import org.ddol.idea.plugin.funl.validation.FunlIssue
import org.ddol.idea.plugin.funl.validation.FunlIssueLevel.*
import org.ddol.idea.plugin.funl.validation.FunlIssueSink
import org.ddol.idea.plugin.funl.validation.FunlValidator
import kotlin.reflect.KClass
import kotlin.reflect.full.safeCast

/**
 * An adapter that allows to implement [Annotator] that delegates validating logic
 * to one of existing [FunlValidator]s.
 */
abstract class FunlValidatorAdapter<T : PsiElement> : Annotator {

    override fun annotate(element: PsiElement, holder: AnnotationHolder) {
        val typedElement = elementClass.safeCast(element) ?: return
        validator.validate(typedElement, DefaultAnnotatorSink(holder))
    }

    /**
     * Override this property to specify the PSI element base class.
     */
    protected abstract val elementClass: KClass<T>

    /**
     * Override this property to specify validator.
     */
    protected abstract val validator: FunlValidator<T>

    /**
     * Override this method if you need to suggest code fixes based on results of validation.
     */
    open fun produceFixes(alias: String, element: PsiElement, consume: (IntentionAction) -> Unit) {}

    private inner class DefaultAnnotatorSink(private val holder: AnnotationHolder) : FunlIssueSink {

        override fun addIssue(issue: FunlIssue) {
            val highlightSeverity = when (issue.level) {
                ERROR -> HighlightSeverity.ERROR
                WARNING -> HighlightSeverity.WARNING
                WEAK_WARNING -> HighlightSeverity.WEAK_WARNING
            }

            val annotation = holder.createAnnotation(highlightSeverity, issue.range, issue.message)

            issue.getFixSuggestions().forEach { alias, element ->
                produceFixes(alias, element) { fix ->
                    annotation.registerFix(fix)
                }
            }
        }
    }
}

