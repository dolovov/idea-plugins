package org.ddol.idea.plugin.funl.annotators

import com.intellij.codeInsight.intention.IntentionAction
import com.intellij.openapi.application.ApplicationManager
import com.intellij.openapi.command.WriteCommandAction
import com.intellij.openapi.editor.Editor
import com.intellij.openapi.fileEditor.FileEditorManager
import com.intellij.openapi.project.Project
import com.intellij.psi.PsiElement
import com.intellij.psi.PsiFile
import com.intellij.psi.TokenType.WHITE_SPACE
import org.ddol.idea.plugin.funl.Texts.TEXT_FIX_DELETE_FUNCTION
import org.ddol.idea.plugin.funl.Texts.TEXT_FIX_DELETE_VALUE
import org.ddol.idea.plugin.funl.Texts.TEXT_FIX_INSERT_RETURN_STATEMENT
import org.ddol.idea.plugin.funl.Texts.TEXT_FIX_MOVE_STATEMENT_TO_NEXT_LINE
import org.ddol.idea.plugin.funl.psi.FunlTypes.FUNCTION
import org.ddol.idea.plugin.funl.psi.FunlTypes.VALUE
import org.ddol.idea.plugin.funl.psi.createWhitespaceNode
import org.ddol.idea.plugin.funl.tools.fixFunctionBlockBodyWithoutReturnStatement
import org.ddol.idea.plugin.funl.unreachableCode


const val FIX_GROUP_SYNTAX = "Funl Syntax"

/**
 * Basic fix.
 */
abstract class BasicElementFix(val element: PsiElement) : IntentionAction {

    final override fun getFamilyName() = FIX_GROUP_SYNTAX

    final override fun startInWriteAction() = true

    override fun isAvailable(unused1: Project, unused2: Editor?, unused3: PsiFile?) = true

    final override fun invoke(project: Project, editor: Editor?, file: PsiFile?) {
        ApplicationManager.getApplication().invokeLater {
            object : WriteCommandAction.Simple<Void>(project) {
                override fun run() {
                    invokeInWriteAction(project)
                }
            }.execute()
        }

    }

    abstract fun invokeInWriteAction(project: Project)
}

/**
 * Deletes the given PSI element.
 */
class DeleteElementFix(element: PsiElement) : BasicElementFix(element) {

    override fun getText() = when (element.node.elementType) {
        FUNCTION -> TEXT_FIX_DELETE_FUNCTION
        VALUE -> TEXT_FIX_DELETE_VALUE
        else -> unreachableCode()
    }

    override fun invokeInWriteAction(project: Project) {
        element.delete()
    }
}

/**
 * Move the statements represented by the given PSI element to the next line.
 */
class MoveStatementToNextLineFix(statement: PsiElement) : BasicElementFix(statement) {

    override fun getText() = TEXT_FIX_MOVE_STATEMENT_TO_NEXT_LINE

    override fun invokeInWriteAction(project: Project) {
        val prevSibling = element.prevSibling
        if (prevSibling?.node?.elementType == WHITE_SPACE)
            prevSibling.parent.node.removeChild(prevSibling.node)

        element.parent.node.addChild(createWhitespaceNode("\n"), element.node)
        FileEditorManager.getInstance(project).selectedTextEditor?.caretModel?.moveCaretRelatively(0, 1, false, false, false)
    }
}

/**
 * Add a return statement to the function's block body.
 */
class FunctionBlockBodyWithoutReturnStatementFix(closingBraceElement: PsiElement) : BasicElementFix(closingBraceElement) {

    override fun getText() = TEXT_FIX_INSERT_RETURN_STATEMENT

    override fun invokeInWriteAction(project: Project) {
        FileEditorManager.getInstance(project).selectedTextEditor?.let { nonNullEditor ->
            fixFunctionBlockBodyWithoutReturnStatement(nonNullEditor, element)
        }
    }
}
