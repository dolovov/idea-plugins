package org.ddol.idea.plugin.funl.tools

import com.intellij.lang.refactoring.NamesValidator
import com.intellij.openapi.project.Project
import org.ddol.idea.plugin.funl.FunlParserDefinition.Companion.KEYWORD_TOKENS
import org.ddol.idea.plugin.funl.FunlParserDefinition.Companion.createLexer
import org.ddol.idea.plugin.funl.psi.FunlTypes.IDENTIFIER

/**
 * Validator of Funl [IDENTIFIER]s.
 */
class FunlNamesValidator : NamesValidator {

    private val lexer = createLexer()

    override fun isKeyword(name: String, unused: Project?): Boolean = synchronized(lexer) {
        lexer.start(name)
        return lexer.tokenEnd == name.length && KEYWORD_TOKENS.contains(lexer.tokenType)
    }

    override fun isIdentifier(name: String, unused: Project?): Boolean = synchronized(lexer) {
        lexer.start(name)
        lexer.tokenEnd == name.length && IDENTIFIER == lexer.tokenType
    }
}
