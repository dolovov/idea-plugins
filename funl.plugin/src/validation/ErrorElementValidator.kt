package org.ddol.idea.plugin.funl.validation

import com.intellij.psi.PsiErrorElement
import com.intellij.psi.TokenType.ERROR_ELEMENT
import com.intellij.psi.tree.TokenSet.create
import org.ddol.idea.plugin.funl.validation.FunlIssueLevel.ERROR

/**
 * Validator that reports every encountered [PsiErrorElement].
 */
object ErrorElementValidator : FunlValidator<PsiErrorElement> {

    override val matchingElementTypes = create(ERROR_ELEMENT)

    override fun validate(element: PsiErrorElement, sink: FunlIssueSink) {
        sink.addIssue(FunlIssue(ERROR, element, element.errorDescription))
    }
}
