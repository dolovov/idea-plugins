package org.ddol.idea.plugin.funl.interpreter

import com.intellij.util.containers.MultiMap
import org.ddol.idea.plugin.funl.interpreter.OutputType.*

/**
 * Execution stack. Consists of [FunlStackFrame]s.
 */
class FunlStack<R> {
    private val frames = mutableListOf<FunlStackFrame<R>>()

    /**
     * Create a new frame.
     * Move [operandsCount] operands from operands stack of old frame to operands stack of new frame.
     */
    fun createFrame(operandsCount: Int) {
        assertExecution(operandsCount >= 0) { "Invalid number of operands: $operandsCount" }

        val newFrame = FunlStackFrame<R>()

        if (operandsCount > 0) {
            val oldFrame = currentFrame()
            repeat(operandsCount) { newFrame.push(oldFrame.pop()) }
        }

        frames.add(newFrame)
    }

    /**
     * Destroy a frame.
     * Move [operandsCount] operands from operands stack of destroyed frame to operands stack of previous frame.
     */
    fun destroyFrame(operandsCount: Int) {
        assertExecution(frames.size > 0) { "No more frames to destroy" }
        assertExecution(operandsCount >= 0) { "Invalid number of operands: $operandsCount" }
        assertExecution(operandsCount == 0 || frames.size > 1) { "Impossible to move $operandsCount operand(s) because destroying the last frame" }

        val destroyedFrame = frames.removeLast()

        if (operandsCount > 0) {
            val previousFrame = currentFrame()
            repeat(operandsCount) { previousFrame.push(destroyedFrame.pop()) }
        }
    }

    fun firstFrame() = frames.first()

    fun currentFrame() = frames.last()
}

/**
 * Stack frame. Has:
 * - [localValues] - named map of values and parameters visible in this frame
 * - [operandStack] - operand stack
 */
class FunlStackFrame<R> {
    private val localValues = MultiMap.createLinked<String, FunlWord>()
    private val operandStack = mutableListOf<FunlWord>()
    private var returnAddress: R? = null

    fun getValue(name: String): FunlWord? {
        if (localValues.containsKey(name))
            return localValues.get(name).last()

        return null
    }

    fun putValue(name: String, value: FunlWord) = localValues.putValue(name, value)

    fun push(operand: FunlWord) = operandStack.add(operand)

    fun pop() = operandStack.removeLast()

    fun setReturnAddress(returnAddress: R) {
        assertExecution(this.returnAddress == null) { "Return address already specified: ${this.returnAddress}" }
        this.returnAddress = returnAddress
    }

    fun getReturnAddress() = returnAddress
}

private fun <T> MutableList<T>.removeLast() = this.removeAt(this.size - 1)

fun assertExecution(value: Boolean, lazyMessage: () -> String) {
    if (!value)
        failExecution(lazyMessage())
}

fun failExecution(message: String): Nothing = throw RuntimeException("Execution error: $message")

enum class OutputType { STDOUT, STDERR }

val standardJVMOutput: (OutputType, String) -> Unit = { outputType, message ->
    when (outputType) {
        STDOUT -> System.out
        STDERR -> System.err
    }.print(message)
}
