package org.ddol.idea.plugin.funl.navigation

import com.intellij.psi.ElementDescriptionLocation
import com.intellij.psi.ElementDescriptionProvider
import com.intellij.psi.PsiElement
import com.intellij.usageView.UsageViewShortNameLocation
import com.intellij.usageView.UsageViewTypeLocation
import org.ddol.idea.plugin.funl.Texts.TEXT_ELEMENT_TYPE_FUNCTION
import org.ddol.idea.plugin.funl.Texts.TEXT_ELEMENT_TYPE_PARAMETER
import org.ddol.idea.plugin.funl.Texts.TEXT_ELEMENT_TYPE_VALUE
import org.ddol.idea.plugin.funl.psi.FunlFunction
import org.ddol.idea.plugin.funl.psi.FunlParameter
import org.ddol.idea.plugin.funl.psi.FunlTypes.*
import org.ddol.idea.plugin.funl.psi.FunlValue

/**
 * Implementation of [ElementDescriptionProvider].
 */
class FunlElementDescriptionProvider : ElementDescriptionProvider {

    /**
     * Return texts that are used to display tip for named element such as [FunlFunction], [FunlParameter]
     * or [FunlValue] when hovering mouse together with pressed Cmd button over this element.
     */
    override fun getElementDescription(element: PsiElement, location: ElementDescriptionLocation) = when (element) {
        is FunlNamedElement -> when (location) {
            is UsageViewShortNameLocation -> element.name.orEmpty()
            is UsageViewTypeLocation -> getElementTypeLowercase(element)
            else -> null
        }
        else -> null
    }

    private fun getElementTypeLowercase(element: FunlNamedElement) = when (element.node.elementType) {
        VALUE -> TEXT_ELEMENT_TYPE_VALUE
        PARAMETER -> TEXT_ELEMENT_TYPE_PARAMETER
        FUNCTION -> TEXT_ELEMENT_TYPE_FUNCTION
        else -> null
    }
}
