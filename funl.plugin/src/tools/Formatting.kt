package org.ddol.idea.plugin.funl.tools

import com.intellij.formatting.*
import com.intellij.formatting.Wrap.createWrap
import com.intellij.formatting.WrapType.NONE
import com.intellij.lang.ASTNode
import com.intellij.openapi.util.TextRange
import com.intellij.psi.PsiElement
import com.intellij.psi.PsiFile
import com.intellij.psi.codeStyle.CodeStyleSettings
import com.intellij.psi.formatter.FormatterUtil
import com.intellij.psi.formatter.FormatterUtil.isWhitespaceOrEmpty
import org.ddol.idea.plugin.funl.FunlLanguage
import org.ddol.idea.plugin.funl.FunlParserDefinition.Companion.ANY_BODY_ALL_STATEMENTS
import org.ddol.idea.plugin.funl.FunlParserDefinition.Companion.ANY_BODY_KEYWORD_TOKENS
import org.ddol.idea.plugin.funl.FunlParserDefinition.Companion.FUNL_FILE
import org.ddol.idea.plugin.funl.FunlParserDefinition.Companion.OPERATOR_TOKENS
import org.ddol.idea.plugin.funl.psi.FunlTypes.*

/**
 * Formatting for Funl program code.
 */
class FunlFormattingBlock(
        private val myNode: ASTNode,
        private val myIndent: Indent,
        private val mySpacingBuilder: SpacingBuilder) : ASTBlock {

    // lazily evaluate the list of children blocks
    private val mySubBlocks: MutableList<FunlFormattingBlock> by lazy {

        myNode.getChildren(null).filter { child -> !isWhitespaceOrEmpty(child) }.map { child ->

            val indent = when (myNode.elementType) {
                BLOCK_BODY -> when (child.elementType) {
                    BRACE_OPEN, BRACE_CLOSE -> Indent.getNoneIndent()
                    else -> Indent.getNormalIndent()
                }
                FUNCTION, VALUE, RETURN, OUTPUT -> Indent.getContinuationWithoutFirstIndent()
                PARENTHESES_EXPRESSION -> Indent.getNormalIndent()
                else -> Indent.getNoneIndent()
            }

            FunlFormattingBlock(child, indent, mySpacingBuilder)
        }.toMutableList()
    }

    override fun getNode() = myNode

    override fun isLeaf() = myNode.firstChildNode == null

    override fun getTextRange() = myNode.textRange!!

    override fun getWrap() = createWrap(NONE, false)!! // no wrapping

    override fun getAlignment(): Alignment? = null // no alignment

    override fun getIndent() = myIndent

    override fun getSpacing(child1: Block?, child2: Block) = mySpacingBuilder.getSpacing(this, child1, child2)

    override fun getSubBlocks(): MutableList<FunlFormattingBlock> = mySubBlocks

    override fun getChildAttributes(newChildIndex: Int): ChildAttributes {

        val indent = when (myNode.elementType) {
            BLOCK_BODY, PARENTHESES_EXPRESSION -> Indent.getNormalIndent()
            FUNL_FILE -> Indent.getNoneIndent()
            else -> null
        }

        return ChildAttributes(indent, null)
    }

    override fun isIncomplete() = FormatterUtil.isIncomplete(myNode)
}

class FunlFormattingModelBuilder : FormattingModelBuilder {

    override fun getRangeAffectingIndent(file: PsiFile?, offset: Int, elementAtOffset: ASTNode?): TextRange? = null

    override fun createModel(element: PsiElement, settings: CodeStyleSettings): FormattingModel {
        return FormattingModelProvider.createFormattingModelForPsiFile(
                element.containingFile,
                FunlFormattingBlock(element.node, Indent.getNoneIndent(), createSpacingBuilder(settings)),
                settings)
    }

    private fun createSpacingBuilder(settings: CodeStyleSettings) =
            SpacingBuilder(settings, FunlLanguage)
                    .around(ANY_BODY_ALL_STATEMENTS).blankLines(0)
                    .after(ANY_BODY_KEYWORD_TOKENS).spacing(1, 1, 0, false, 0)
                    .after(PAREN_OPEN).spaces(0)
                    .before(PAREN_CLOSE).spacing(0, 0, 0, false, 0)
                    .between(IDENTIFIER, PAREN_OPEN).spacing(0, 0, 0, false, 0)
                    .between(PAREN_CLOSE, BLOCK_BODY).spacing(1, 1, 0, false, 0)
                    .after(BRACE_OPEN).lineBreakInCode()
                    .before(BRACE_CLOSE).lineBreakInCode()
                    .before(COMMA).spacing(0, 0, 0, false, 0)
                    .after(COMMA).spaces(1)
                    .between(REFERENCE_EXPRESSION, FUNCTION_CALL_ARGUMENTS).spacing(0, 0, 0, false, 0)
                    .afterInside(OP_MINUS, UNARY_MINUS_EXPRESSION).spacing(0, 0, 0, false, 0)
                    .afterInside(OP_PLUS, UNARY_PLUS_EXPRESSION).spacing(0, 0, 0, false, 0)
                    .before(OPERATOR_TOKENS).spacing(1, 1, 0, false, 0)
                    .after(OPERATOR_TOKENS).spacing(1, 1, 0, true, 0)
}
