package org.ddol.idea.plugin.funl.annotators

import org.ddol.idea.plugin.funl.validation.UnresolvedReferenceValidator
import org.ddol.idea.plugin.funl.psi.FunlReferenceExpression

/**
 * Annotator: Detect and highlight unresolved references.
 */
class UnresolvedReferencesAnnotator : FunlValidatorAdapter<FunlReferenceExpression>() {
    override val elementClass = FunlReferenceExpression::class
    override val validator = UnresolvedReferenceValidator
}

