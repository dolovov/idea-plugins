package org.ddol.idea.plugin.funl.psi

import com.intellij.lang.PsiBuilder
import com.intellij.lang.parser.GeneratedParserUtilBase
import com.intellij.openapi.util.Key
import com.intellij.psi.TokenType.WHITE_SPACE
import gnu.trove.TObjectLongHashMap
import org.ddol.idea.plugin.funl.psi.FunlParserUtil.enterMode
import org.ddol.idea.plugin.funl.psi.FunlParserUtil.exitMode
import org.ddol.idea.plugin.funl.psi.FunlParserUtil.isModeOff
import org.ddol.idea.plugin.funl.psi.FunlParserUtil.isModeOn
import org.ddol.idea.plugin.funl.psi.FunlTypes.*

/**
 * Extension of [GeneratedParserUtilBase] for the needs of more precise "recoverWhile" rules.
 *
 * LEGAL NOTE: The code for methods [isModeOn], [isModeOff], [enterMode] and [exitMode] are taken from the
 * org.intellij.erlang.parser.ErlangParserUtil class (see https://github.com/ignatov/intellij-erlang).
 */
object FunlParserUtil : GeneratedParserUtilBase() {

    private val MODES_KEY = Key.create<TObjectLongHashMap<String>>("MODES_KEY")

    private const val BLOCK_BODY_MODE = "BLOCK_BODY"

    private fun getParsingModes(builder: PsiBuilder): TObjectLongHashMap<String> {
        return builder.getUserDataUnprotected<TObjectLongHashMap<String>>(MODES_KEY) ?: {
            val tmp = TObjectLongHashMap<String>()
            builder.putUserDataUnprotected(MODES_KEY, tmp)
            tmp
        }.invoke()
    }

    @JvmStatic
    fun isModeOn(
            builder: PsiBuilder,
            @Suppress("UNUSED_PARAMETER") level: Int,
            mode: String) = getParsingModes(builder).get(mode) > 0L

    @JvmStatic
    fun isModeOff(
            builder: PsiBuilder,
            @Suppress("UNUSED_PARAMETER") level: Int,
            mode: String) = getParsingModes(builder).get(mode) == 0L

    @JvmStatic
    fun enterMode(
            builder: PsiBuilder,
            @Suppress("UNUSED_PARAMETER") level: Int,
            mode: String): Boolean {

        val flags = getParsingModes(builder)
        if (!flags.increment(mode))
            flags.put(mode, 1L)

        return true
    }

    @JvmStatic
    fun exitMode(
            builder: PsiBuilder,
            @Suppress("UNUSED_PARAMETER") level: Int,
            mode: String): Boolean {

        val flags = getParsingModes(builder)
        val count = flags.get(mode)

        when {
            count == 1L -> flags.remove(mode)
            count > 1L -> flags.put(mode, count - 1)
            else -> builder.error("Could not exit inactive $mode mode at offset ${builder.currentOffset}")
        }

        return true
    }

    /**
     * Consume a non-whitespace token that might be a part of [FunlIncomplete] statement.
     */
    @JvmStatic
    fun incompleteStatementToken(builder: PsiBuilder, level: Int): Boolean {

        if (eof(builder, level))
            return false

        when (builder.tokenType) {
            WHITE_SPACE, BLOCK_COMMENT, LINE_COMMENT -> return false
        }

        if (isModeOn(builder, level, BLOCK_BODY_MODE) && builder.tokenType == BRACE_CLOSE)
            return false

        consumeToken(builder, builder.tokenType)
        return true
    }
}
