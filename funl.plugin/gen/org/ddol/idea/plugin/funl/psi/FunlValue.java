// This is a generated file. Not intended for manual editing.
package org.ddol.idea.plugin.funl.psi;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.psi.PsiElement;
import org.ddol.idea.plugin.funl.navigation.FunlNamedElement;
import com.intellij.psi.NavigatablePsiElement;
import org.ddol.idea.plugin.funl.navigation.FunlElementWithIdentifier;
import org.ddol.idea.plugin.funl.tools.FunlItemPresentation;

public interface FunlValue extends FunlNamedElement, NavigatablePsiElement {

  @Nullable
  FunlExpression getExpression();

  @Nullable
  PsiElement getIdentifier();

  @Nullable
  String getName();

  @Nullable
  FunlElementWithIdentifier setName(String newName);

  @Nullable
  PsiElement getNameIdentifier();

  int getTextOffset();

  @NotNull
  FunlItemPresentation getPresentation();

  boolean isGlobal();

}
