package org.ddol.idea.plugin.funl.tools

import com.intellij.codeInsight.daemon.impl.HighlightRangeExtension
import com.intellij.openapi.editor.DefaultLanguageHighlighterColors
import com.intellij.openapi.editor.HighlighterColors
import com.intellij.openapi.editor.colors.TextAttributesKey
import com.intellij.openapi.fileTypes.SyntaxHighlighterBase
import com.intellij.openapi.fileTypes.SyntaxHighlighterFactory
import com.intellij.openapi.project.Project
import com.intellij.openapi.vfs.VirtualFile
import com.intellij.psi.PsiFile
import com.intellij.psi.TokenType.BAD_CHARACTER
import com.intellij.psi.tree.IElementType
import org.ddol.idea.plugin.funl.FunlParserDefinition.Companion.createLexer
import org.ddol.idea.plugin.funl.psi.FunlFile
import org.ddol.idea.plugin.funl.psi.FunlTypes.*

/**
 * Syntax highlighting for Funl.
 */
class FunlSyntaxHighlighter : SyntaxHighlighterBase() {

    override fun getHighlightingLexer() = createLexer()

    override fun getTokenHighlights(tokenType: IElementType): Array<TextAttributesKey> {
        return when (tokenType) {
            OP_PLUS, OP_MINUS, OP_MULT, OP_DIV, OP_EQ -> OPERATOR_KEYS
            PAREN_OPEN, PAREN_CLOSE -> PARENTHESES_KEYS
            BRACE_OPEN, BRACE_CLOSE -> BRACE_KEYS
            COMMA -> COMMA_KEYS
            KEYWORD_FUNC, KEYWORD_VAL, KEYWORD_RETURN, KEYWORD_SAY -> KEYWORD_KEYS
            IDENTIFIER -> IDENTIFIER_KEYS
            NUMBER -> NUMBER_KEYS
            LINE_COMMENT -> LINE_COMMENT_KEYS
            BLOCK_COMMENT -> BLOCK_COMMENT_KEYS
            BAD_CHARACTER -> BAD_CHARACTER_KEYS
            else -> EMPTY_KEYS
        }
    }

    companion object {

        private val OPERATOR_KEYS = arrayOf(DefaultLanguageHighlighterColors.OPERATION_SIGN)
        private val PARENTHESES_KEYS = arrayOf(DefaultLanguageHighlighterColors.PARENTHESES)
        private val BRACE_KEYS = arrayOf(DefaultLanguageHighlighterColors.BRACES)
        private val COMMA_KEYS = arrayOf(DefaultLanguageHighlighterColors.COMMA)

        private val IDENTIFIER_KEYS = arrayOf(DefaultLanguageHighlighterColors.IDENTIFIER)

        private val KEYWORD_KEYS = arrayOf(DefaultLanguageHighlighterColors.KEYWORD)
        private val NUMBER_KEYS = arrayOf(DefaultLanguageHighlighterColors.NUMBER)

        private val LINE_COMMENT_KEYS = arrayOf(DefaultLanguageHighlighterColors.LINE_COMMENT)
        private val BLOCK_COMMENT_KEYS = arrayOf(DefaultLanguageHighlighterColors.BLOCK_COMMENT)
        private val BAD_CHARACTER_KEYS = arrayOf(HighlighterColors.BAD_CHARACTER)
        private val EMPTY_KEYS = arrayOf<TextAttributesKey>()
    }
}

class FunlSyntaxHighlighterFactory : SyntaxHighlighterFactory() {

    override fun getSyntaxHighlighter(unused1: Project?, unused2: VirtualFile?) = FunlSyntaxHighlighter()
}

class FunlHighlightRangeExtension : HighlightRangeExtension {

    override fun isForceHighlightParents(file: PsiFile) = file is FunlFile
}
