package org.ddol.idea.plugin.funl.interpreter

import com.intellij.psi.PsiElement
import com.intellij.psi.PsiFile
import com.intellij.psi.tree.IElementType
import com.intellij.util.containers.MultiMap
import org.ddol.idea.plugin.funl.interpreter.OutputType.STDERR
import org.ddol.idea.plugin.funl.psi.*
import org.ddol.idea.plugin.funl.validation.*
import org.ddol.idea.plugin.funl.validation.FunlIssueLevel.ERROR

/**
 * Generate IL code for the given [FunlFile].
 */
fun generateIL(file: FunlFile, output: (OutputType, String) -> Unit, onFailure: (Int) -> Unit): List<ILInstruction> {

    val visitor = ILGeneratingVisitor()
    file.accept(visitor)

    val logicalPositionEvaluator by lazy { FastLogicalPositionEvaluator(file) }

    visitor.getIssues().forEach { issue ->
        val position = logicalPositionEvaluator.getLogicalPosition(issue.range.startOffset)
        val message = "${issue.level}($position): ${issue.message}\n"
        output(STDERR, message)
    }

    val errorCount = visitor.getErrorCount()
    if (errorCount > 0) {
        output(STDERR, "\n$errorCount error(s) encountered. Terminating.\n")
        onFailure(1)
        return emptyList() // don't return any IL instructions if there are errors
    }

    return visitor.getInstructions()
}

/**
 * Implementation of [FunlVisitor] that collects IL instructions for every PSI element.
 */
private class ILGeneratingVisitor : FunlVisitor() {

    private val instructions = InstructionsCollector()
    private val issues = ILGeneratorIssueSink()

    private val validators = Validators(issues)
            .add(ErrorElementValidator)
            .add(IncompleteStatementValidator)
            .add(TwoStatementsSameLineValidator)
            .add(ReturnStatementIssueValidator)
            .add(DuplicatedAndShadowedDeclarationsValidator)
            .add(UnresolvedReferenceValidator)
            .add(UnusedElementValidator)

    override fun visitElement(element: PsiElement) {
        validators.validate(element)
        element.acceptChildren(this) // make it recursive
    }

    override fun visitFile(file: PsiFile?) {
        if (file !is FunlFile)
            throw RuntimeException("Unexpected file type: $file")

        // first build IL for the main program body
        instructions.add(ILBodyStart(PROGRAM_BODY, 0))
        super.visitFile(file)
        instructions.add(ILBodyEnd)

        instructions.add(ILCreateFrame(0)) // create the first (empty) stack frame
        instructions.add(ILCall(PROGRAM_BODY, 0)) // run the program
    }

    override fun visitValue(value: FunlValue) {
        super.visitValue(value)
        instructions.add(ILPutValue(value.name.orEmpty())) // put operand from stack to value
    }

    override fun visitParameter(parameter: FunlParameter) {
        super.visitParameter(parameter)
        instructions.add(ILPutValue(parameter.name.orEmpty())) // put operand from stack as value
    }

    override fun visitFunction(function: FunlFunction) {
        // build IL for every function
        instructions.add(ILBodyStart(function.name.orEmpty(), function.parameterList.size))
        super.visitFunction(function)
        instructions.add(ILBodyEnd)
    }

    override fun visitReferenceExpression(expression: FunlReferenceExpression) {
        super.visitReferenceExpression(expression)

        if (!expression.isFunctionCall)
            instructions.add(ILGetValue(expression.getName().orEmpty())) // get value to operand stack
    }

    override fun visitFunctionCallExpression(functionCall: FunlFunctionCallExpression) {
        super.visitFunctionCallExpression(functionCall)

        (functionCall.referenceExpression.resolve() as? FunlFunction)?.let { function ->
            instructions.add(ILCreateFrame(function.parameterList.size))
            instructions.add(ILCall(function.name.orEmpty(), function.parameterList.size))
            instructions.add(ILDestroyFrame(1))
        }
    }

    @Suppress("RedundantOverride")
    override fun visitParenthesesExpression(expression: FunlParenthesesExpression) {
        super.visitParenthesesExpression(expression)
        // do nothing specific
    }

    override fun visitSumExpression(expression: FunlSumExpression) {
        super.visitSumExpression(expression)
        instructions.add(ILSummarize)
    }

    override fun visitDifferenceExpression(expression: FunlDifferenceExpression) {
        super.visitDifferenceExpression(expression)
        instructions.add(ILSubtract)
    }

    override fun visitProductExpression(expression: FunlProductExpression) {
        super.visitProductExpression(expression)
        instructions.add(ILMultiply)
    }

    override fun visitQuotientExpression(expression: FunlQuotientExpression) {
        super.visitQuotientExpression(expression)
        instructions.add(ILDivide)
    }

    override fun visitUnaryMinusExpression(expression: FunlUnaryMinusExpression) {
        super.visitUnaryMinusExpression(expression)
        instructions.add(ILNegate)
    }

    @Suppress("RedundantOverride")
    override fun visitUnaryPlusExpression(expression: FunlUnaryPlusExpression) {
        super.visitUnaryPlusExpression(expression)
        // do nothing specific
    }

    override fun visitLiteralExpression(literal: FunlLiteralExpression) {
        super.visitLiteralExpression(literal)
        instructions.add(ILLiteral(literal.number.text))
    }

    override fun visitOutput(output: FunlOutput) {
        super.visitOutput(output)
        instructions.add(ILPrint)
    }

    override fun visitIncomplete(incomplete: FunlIncomplete) {
        validators.validate(incomplete)
        // important: do not visit child elements - do need to trigger extra error messages
    }

    fun getInstructions() = instructions.getAll()

    fun getIssues() = issues.getAllIssues()

    fun getErrorCount() = issues.getErrorCount()
}

/**
 * Collector of instructions.
 */
private class InstructionsCollector {

    private val bodies = mutableListOf<Body>()
    private val freeInstructions = mutableListOf<ILInstruction>()

    private val instructions = mutableListOf<ILInstruction>()

    fun add(instruction: ILInstruction) {

        when (instruction) {

            is ILBodyStart -> {
                // start recording a new body
                val body = Body()
                body.addInstruction(instruction)
                bodies.add(body)
            }

            is ILBodyEnd -> {
                // end body, flush it to `instructions`
                val body = bodies.removeAt(bodies.size - 1)
                body.addInstruction(instruction)
                body.outputTo(instructions)
            }

            else -> {
                // if there is a body, then add instruction to it
                // otherwise - to `freeInstructions`
                if (bodies.isNotEmpty())
                    bodies.last().addInstruction(instruction)
                else
                    freeInstructions.add(instruction)
            }
        }
    }

    fun getAll(): List<ILInstruction> {
        assertExecution(bodies.isEmpty()) { "There are unclosed IL bodies: $bodies" }

        val result = mutableListOf<ILInstruction>()
        result.addAll(instructions)
        result.addAll(freeInstructions)

        return result
    }

    /**
     * Just a group of IL instructions that represent a single IL body.
     *
     * The goal here is to avoid generating IL code like this:
     *   body_start[name=__main__,parametersCount=0]
     *   ...
     *   body_start[name=myFunction,parametersCount=2]
     *   ...
     *   ...
     *   body_end
     *   ...
     *   body_end
     *
     * But instead generate it like the following:
     *   body_start[name=__main__,parametersCount=0]
     *   ...
     *   ...
     *   body_end
     *   body_start[name=myFunction,parametersCount=2]
     *   ...
     *   ...
     *   body_end
     *
     * So that bodies are not nested one inside of another.
     */
    private class Body {
        private val instructions = mutableListOf<ILInstruction>()

        fun addInstruction(instruction: ILInstruction) {
            instructions.add(instruction)
        }

        fun outputTo(output: MutableList<ILInstruction>) {
            output.addAll(instructions)
            instructions.clear()
        }

        override fun toString(): String {
            return "Body{$instructions}"
        }
    }
}

/**
 * A [FunlIssueSink] for [ILGeneratingVisitor].
 */
private class ILGeneratorIssueSink : FunlIssueSink {

    private val issues = mutableListOf<FunlIssue>()
    private var errorCount = 0

    override fun addIssue(issue: FunlIssue) {
        issues.add(issue)
        if (issue.level == ERROR) errorCount++
    }

    fun getAllIssues(): List<FunlIssue> {
        issues.sort()
        return issues
    }

    fun getErrorCount() = errorCount
}

/**
 * Handy work with all the necessary [FunlValidator]s.
 */
private class Validators(private val sink: FunlIssueSink) {

    private val validatorsByElementTypes = MultiMap.create<IElementType, FunlValidator<PsiElement>>()

    fun <T: PsiElement> add(validator: FunlValidator<T>): Validators {

        validator.matchingElementTypes.types.forEach { elementType ->
            @Suppress("UNCHECKED_CAST")
            validatorsByElementTypes.putValue(elementType, validator as FunlValidator<PsiElement>)
        }

        return this
    }

    fun validate(element: PsiElement) {

        val elementType = element.node.elementType
        if (validatorsByElementTypes.containsKey(elementType))
            validatorsByElementTypes[elementType].forEach { it.validate(element, sink) }
    }
}

/**
 * Represents a logical position in program source code. A-la: <line>:<column>
 * The numbering for lines and columns is started from 1.
 */
private class LogicalPosition(private val line: Int, private val column: Int) {
    override fun toString() = "$line:$column"
}

/**
 * Fast evaluation of [LogicalPosition] in the given [FunlFile].
 */
private class FastLogicalPositionEvaluator(file: FunlFile) {

    private val newlineOffsets = mutableListOf<Int>()

    init {
        val text = file.node.text
        for (i in 0 until text.length)
            if (text[i] == '\n') newlineOffsets.add(i)
    }

    fun getLogicalPosition(offset: Int): LogicalPosition {
        assert(offset > 0)

        var linesMet = 1
        var remainingOffset = offset + 1

        newlineOffsets.forEach { newlineOffset ->
            if (newlineOffset > offset)
                return@forEach
            else {
                linesMet++
                remainingOffset = offset - newlineOffset
            }
        }

        return LogicalPosition(linesMet, remainingOffset)
    }
}
