package org.ddol.idea.plugin.funl.tools

import com.intellij.lang.Commenter

/**
 * "Comment with Line Comment" and "Comment with Block Comment" support in Funl.
 */
class FunlCommenter : Commenter {

    override fun getLineCommentPrefix() = "//"

    override fun getBlockCommentPrefix() = "/*"

    override fun getBlockCommentSuffix() = "*/"

    override fun getCommentedBlockCommentPrefix(): String? = null

    override fun getCommentedBlockCommentSuffix(): String? = null
}
