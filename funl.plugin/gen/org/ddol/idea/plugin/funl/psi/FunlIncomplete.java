// This is a generated file. Not intended for manual editing.
package org.ddol.idea.plugin.funl.psi;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.psi.PsiElement;
import com.intellij.psi.NavigatablePsiElement;
import org.ddol.idea.plugin.funl.tools.FunlItemPresentation;

public interface FunlIncomplete extends NavigatablePsiElement {

  @NotNull
  FunlItemPresentation getPresentation();

}
