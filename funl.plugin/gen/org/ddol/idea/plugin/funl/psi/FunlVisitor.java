// This is a generated file. Not intended for manual editing.
package org.ddol.idea.plugin.funl.psi;

import org.jetbrains.annotations.*;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.PsiElement;
import org.ddol.idea.plugin.funl.navigation.FunlNamedElement;
import com.intellij.psi.NavigatablePsiElement;
import com.intellij.psi.PsiPolyVariantReference;
import com.intellij.codeInsight.daemon.EmptyResolveMessageProvider;
import org.ddol.idea.plugin.funl.navigation.FunlElementWithIdentifier;

public class FunlVisitor extends PsiElementVisitor {

  public void visitBlockBody(@NotNull FunlBlockBody o) {
    visitPsiElement(o);
  }

  public void visitDifferenceExpression(@NotNull FunlDifferenceExpression o) {
    visitExpression(o);
  }

  public void visitExpression(@NotNull FunlExpression o) {
    visitPsiElement(o);
  }

  public void visitFunction(@NotNull FunlFunction o) {
    visitNamedElement(o);
    // visitNavigatablePsiElement(o);
  }

  public void visitFunctionCallArguments(@NotNull FunlFunctionCallArguments o) {
    visitPsiElement(o);
  }

  public void visitFunctionCallExpression(@NotNull FunlFunctionCallExpression o) {
    visitExpression(o);
  }

  public void visitIncomplete(@NotNull FunlIncomplete o) {
    visitNavigatablePsiElement(o);
  }

  public void visitLiteralExpression(@NotNull FunlLiteralExpression o) {
    visitExpression(o);
  }

  public void visitOutput(@NotNull FunlOutput o) {
    visitNavigatablePsiElement(o);
  }

  public void visitParameter(@NotNull FunlParameter o) {
    visitNamedElement(o);
  }

  public void visitParenthesesExpression(@NotNull FunlParenthesesExpression o) {
    visitExpression(o);
  }

  public void visitProductExpression(@NotNull FunlProductExpression o) {
    visitExpression(o);
  }

  public void visitQuotientExpression(@NotNull FunlQuotientExpression o) {
    visitExpression(o);
  }

  public void visitReferenceExpression(@NotNull FunlReferenceExpression o) {
    visitExpression(o);
    // visitPsiPolyVariantReference(o);
    // visitEmptyResolveMessageProvider(o);
    // visitElementWithIdentifier(o);
  }

  public void visitReturn(@NotNull FunlReturn o) {
    visitPsiElement(o);
  }

  public void visitSumExpression(@NotNull FunlSumExpression o) {
    visitExpression(o);
  }

  public void visitUnaryMinusExpression(@NotNull FunlUnaryMinusExpression o) {
    visitExpression(o);
  }

  public void visitUnaryPlusExpression(@NotNull FunlUnaryPlusExpression o) {
    visitExpression(o);
  }

  public void visitValue(@NotNull FunlValue o) {
    visitNamedElement(o);
    // visitNavigatablePsiElement(o);
  }

  public void visitNamedElement(@NotNull FunlNamedElement o) {
    visitPsiElement(o);
  }

  public void visitNavigatablePsiElement(@NotNull NavigatablePsiElement o) {
    visitElement(o);
  }

  public void visitPsiElement(@NotNull PsiElement o) {
    visitElement(o);
  }

}
