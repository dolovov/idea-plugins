// This is a generated file. Not intended for manual editing.
package org.ddol.idea.plugin.funl.psi.impl;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.util.PsiTreeUtil;
import static org.ddol.idea.plugin.funl.psi.FunlTypes.*;
import org.ddol.idea.plugin.funl.psi.*;

public class FunlSumExpressionImpl extends FunlExpressionImpl implements FunlSumExpression {

  public FunlSumExpressionImpl(ASTNode node) {
    super(node);
  }

  public void accept(@NotNull FunlVisitor visitor) {
    visitor.visitSumExpression(this);
  }

  public void accept(@NotNull PsiElementVisitor visitor) {
    if (visitor instanceof FunlVisitor) accept((FunlVisitor)visitor);
    else super.accept(visitor);
  }

  @Override
  @NotNull
  public List<FunlExpression> getExpressionList() {
    return PsiTreeUtil.getChildrenOfTypeAsList(this, FunlExpression.class);
  }

}
