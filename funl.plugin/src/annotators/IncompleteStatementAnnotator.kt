package org.ddol.idea.plugin.funl.annotators

import org.ddol.idea.plugin.funl.validation.IncompleteStatementValidator
import org.ddol.idea.plugin.funl.psi.FunlIncomplete

/**
 * Annotation: Detect and highlight incomplete statements.
 */
class IncompleteStatementAnnotator : FunlValidatorAdapter<FunlIncomplete>() {
    override val elementClass = FunlIncomplete::class
    override val validator = IncompleteStatementValidator
}
