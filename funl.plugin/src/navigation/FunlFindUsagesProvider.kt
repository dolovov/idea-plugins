package org.ddol.idea.plugin.funl.navigation

import com.intellij.lang.cacheBuilder.DefaultWordsScanner
import com.intellij.lang.findUsages.FindUsagesProvider
import com.intellij.psi.PsiElement
import org.ddol.idea.plugin.funl.FunlParserDefinition.Companion.COMMENT_TOKENS
import org.ddol.idea.plugin.funl.FunlParserDefinition.Companion.IDENTIFIER_TOKENS
import org.ddol.idea.plugin.funl.FunlParserDefinition.Companion.LITERAL_TOKENS
import org.ddol.idea.plugin.funl.FunlParserDefinition.Companion.createLexer
import org.ddol.idea.plugin.funl.Texts.TEXT_ELEMENT_TYPE_FUNCTION
import org.ddol.idea.plugin.funl.Texts.TEXT_ELEMENT_TYPE_PARAMETER
import org.ddol.idea.plugin.funl.Texts.TEXT_ELEMENT_TYPE_VALUE
import org.ddol.idea.plugin.funl.Texts.TEXT_UNNAMED_ELEMENT
import org.ddol.idea.plugin.funl.psi.FunlTypes.*

/**
 * To enable "Find Usages" feature in Funl.
 */
class FunlFindUsagesProvider : FindUsagesProvider {

    override fun getWordsScanner() = DefaultWordsScanner(createLexer(), IDENTIFIER_TOKENS, COMMENT_TOKENS, LITERAL_TOKENS)

    override fun getNodeText(element: PsiElement, unused: Boolean) = getDescriptiveName(element)

    override fun getDescriptiveName(element: PsiElement) = when (element) {
        is FunlNamedElement -> element.name.orEmpty()
        else -> TEXT_UNNAMED_ELEMENT
    }

    override fun getType(element: PsiElement) = when (element.node.elementType) {
        FUNCTION -> TEXT_ELEMENT_TYPE_FUNCTION
        PARAMETER -> TEXT_ELEMENT_TYPE_PARAMETER
        VALUE -> TEXT_ELEMENT_TYPE_VALUE
        else -> ""
    }

    override fun getHelpId(psiElement: PsiElement): String? = null

    override fun canFindUsagesFor(element: PsiElement) = element is FunlNamedElement
}
