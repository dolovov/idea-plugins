package org.ddol.idea.plugin.funl.tests

import com.intellij.codeInsight.TargetElementUtil.*
import com.intellij.openapi.editor.LogicalPosition
import com.intellij.testFramework.builders.EmptyModuleFixtureBuilder
import com.intellij.testFramework.fixtures.CodeInsightFixtureTestCase
import com.intellij.testFramework.fixtures.ModuleFixture
import com.intellij.util.containers.MultiMap
import junitparams.Parameters
import org.ddol.idea.plugin.funl.psi.FunlTypes.IDENTIFIER
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

/**
 * Tests for resolving references and finding usages.
 */
@RunWith(AdaptedJUnitParamsRunner::class)
class ReferencesTests : CodeInsightFixtureTestCase<EmptyModuleFixtureBuilder<ModuleFixture>>() {

    @Test
    @Parameters
    fun testFindUsages(referencedElementPosition: LogicalPosition, referencePositions: List<LogicalPosition>) {

        editor.caretModel.moveToLogicalPosition(referencedElementPosition)
        val referencedElement = findTargetElement(editor, ELEMENT_NAME_ACCEPTED or REFERENCED_ELEMENT_ACCEPTED)!!
        val foundReferences = myFixture.findUsages(referencedElement).map { it.element }

        val expectedReferences = referencePositions.map { findElementAt(it) }

        assertEquals(
                "Element at $referencedElementPosition, expected references at $referencePositions",
                expectedReferences,
                foundReferences)
    }

    @Suppress("unused")
    private fun parametersForTestFindUsages() = REFERENCE_LOGICAL_POSITIONS.entrySet().map {
        arrayOf(it.key, it.value.filterNotNull())
    }.toTypedArray()


    @Test
    @Parameters
    fun testResolveReferences(referencePosition: LogicalPosition, referencedElementPosition: LogicalPosition) {

        val foundReferencedElement = file.findReferenceAt(lp2o(referencePosition))!!.resolve()!!
        val expectedReferencedElement = findElementAt(referencedElementPosition)

        assertEquals(
                "Reference at $referencePosition, expected referenced element at $referencedElementPosition",
                expectedReferencedElement,
                foundReferencedElement)
    }

    @Suppress("unused")
    private fun parametersForTestResolveReferences() = REFERENCE_LOGICAL_POSITIONS.entrySet().flatMap {
        it.value.filterNotNull().map { v -> arrayOf<Any>(v, it.key) }
    }.toTypedArray()


    @Before
    fun beforeTest(){
        setUp()
        myFixture.configureByFile(testFilePath("ReferencesTests.funl"))
    }

    @After
    fun afterTest() = tearDown()

    private fun lp2o(logicalPosition: LogicalPosition): Int = editor.logicalPositionToOffset(logicalPosition)

    private fun findElementAt(logicalPosition: LogicalPosition) = with (file.findElementAt(lp2o(logicalPosition))!!) {
        if (this.node.elementType == IDENTIFIER) this.parent else this
    }

    companion object {
        // referenced element logical position -> reference logical position
        private val REFERENCE_LOGICAL_POSITIONS = MultiMap.create<LogicalPosition, LogicalPosition?>()

        init {
            REFERENCE_LOGICAL_POSITIONS.putValue(lp(1, 5), null)
            REFERENCE_LOGICAL_POSITIONS.putValue(lp(2, 5), lp(29, 11))
            REFERENCE_LOGICAL_POSITIONS.putValue(lp(3, 5), lp(29, 33))
            REFERENCE_LOGICAL_POSITIONS.putValue(lp(3, 5), lp(30, 11))
            REFERENCE_LOGICAL_POSITIONS.putValue(lp(5, 6), null)
            REFERENCE_LOGICAL_POSITIONS.putValue(lp(6, 6), lp(32, 5))
            REFERENCE_LOGICAL_POSITIONS.putValue(lp(7, 6), lp(32, 26))
            REFERENCE_LOGICAL_POSITIONS.putValue(lp(7, 6), lp(33, 5))
            REFERENCE_LOGICAL_POSITIONS.putValue(lp(10, 3), null)
            REFERENCE_LOGICAL_POSITIONS.putValue(lp(11, 3), lp(14, 13))
            REFERENCE_LOGICAL_POSITIONS.putValue(lp(12, 3), lp(14, 33))
            REFERENCE_LOGICAL_POSITIONS.putValue(lp(12, 3), lp(15, 16))
            REFERENCE_LOGICAL_POSITIONS.putValue(lp(20, 7), null)
            REFERENCE_LOGICAL_POSITIONS.putValue(lp(21, 7), lp(24, 13))
            REFERENCE_LOGICAL_POSITIONS.putValue(lp(22, 7), lp(24, 34))
            REFERENCE_LOGICAL_POSITIONS.putValue(lp(22, 7), lp(25, 16))
        }
    }
}

private fun lp(line: Int, column: Int) = LogicalPosition(line - 1, column - 1)
