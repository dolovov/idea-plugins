// This is a generated file. Not intended for manual editing.
package org.ddol.idea.plugin.funl.psi;

import com.intellij.psi.tree.IElementType;
import com.intellij.psi.PsiElement;
import com.intellij.lang.ASTNode;
import org.ddol.idea.plugin.funl.psi.impl.*;

public interface FunlTypes {

  IElementType BLOCK_BODY = new FunlElementType("BLOCK_BODY");
  IElementType DIFFERENCE_EXPRESSION = new FunlElementType("DIFFERENCE_EXPRESSION");
  IElementType EXPRESSION = new FunlElementType("EXPRESSION");
  IElementType FUNCTION = new FunlElementType("FUNCTION");
  IElementType FUNCTION_CALL_ARGUMENTS = new FunlElementType("FUNCTION_CALL_ARGUMENTS");
  IElementType FUNCTION_CALL_EXPRESSION = new FunlElementType("FUNCTION_CALL_EXPRESSION");
  IElementType INCOMPLETE = new FunlElementType("INCOMPLETE");
  IElementType LITERAL_EXPRESSION = new FunlElementType("LITERAL_EXPRESSION");
  IElementType OUTPUT = new FunlElementType("OUTPUT");
  IElementType PARAMETER = new FunlElementType("PARAMETER");
  IElementType PARENTHESES_EXPRESSION = new FunlElementType("PARENTHESES_EXPRESSION");
  IElementType PRODUCT_EXPRESSION = new FunlElementType("PRODUCT_EXPRESSION");
  IElementType QUOTIENT_EXPRESSION = new FunlElementType("QUOTIENT_EXPRESSION");
  IElementType REFERENCE_EXPRESSION = new FunlElementType("REFERENCE_EXPRESSION");
  IElementType RETURN = new FunlElementType("RETURN");
  IElementType SUM_EXPRESSION = new FunlElementType("SUM_EXPRESSION");
  IElementType UNARY_MINUS_EXPRESSION = new FunlElementType("UNARY_MINUS_EXPRESSION");
  IElementType UNARY_PLUS_EXPRESSION = new FunlElementType("UNARY_PLUS_EXPRESSION");
  IElementType VALUE = new FunlElementType("VALUE");

  IElementType BLOCK_COMMENT = new FunlTokenType("block_comment");
  IElementType BRACE_CLOSE = new FunlTokenType("}");
  IElementType BRACE_OPEN = new FunlTokenType("{");
  IElementType COMMA = new FunlTokenType(",");
  IElementType IDENTIFIER = new FunlTokenType("identifier");
  IElementType KEYWORD_FUNC = new FunlTokenType("func");
  IElementType KEYWORD_RETURN = new FunlTokenType("return");
  IElementType KEYWORD_SAY = new FunlTokenType("say");
  IElementType KEYWORD_VAL = new FunlTokenType("val");
  IElementType LINE_COMMENT = new FunlTokenType("line_comment");
  IElementType NUMBER = new FunlTokenType("number");
  IElementType OP_DIV = new FunlTokenType("/");
  IElementType OP_EQ = new FunlTokenType("=");
  IElementType OP_MINUS = new FunlTokenType("-");
  IElementType OP_MULT = new FunlTokenType("*");
  IElementType OP_PLUS = new FunlTokenType("+");
  IElementType PAREN_CLOSE = new FunlTokenType(")");
  IElementType PAREN_OPEN = new FunlTokenType("(");

  class Factory {
    public static PsiElement createElement(ASTNode node) {
      IElementType type = node.getElementType();
       if (type == BLOCK_BODY) {
        return new FunlBlockBodyImpl(node);
      }
      else if (type == DIFFERENCE_EXPRESSION) {
        return new FunlDifferenceExpressionImpl(node);
      }
      else if (type == FUNCTION) {
        return new FunlFunctionImpl(node);
      }
      else if (type == FUNCTION_CALL_ARGUMENTS) {
        return new FunlFunctionCallArgumentsImpl(node);
      }
      else if (type == FUNCTION_CALL_EXPRESSION) {
        return new FunlFunctionCallExpressionImpl(node);
      }
      else if (type == INCOMPLETE) {
        return new FunlIncompleteImpl(node);
      }
      else if (type == LITERAL_EXPRESSION) {
        return new FunlLiteralExpressionImpl(node);
      }
      else if (type == OUTPUT) {
        return new FunlOutputImpl(node);
      }
      else if (type == PARAMETER) {
        return new FunlParameterImpl(node);
      }
      else if (type == PARENTHESES_EXPRESSION) {
        return new FunlParenthesesExpressionImpl(node);
      }
      else if (type == PRODUCT_EXPRESSION) {
        return new FunlProductExpressionImpl(node);
      }
      else if (type == QUOTIENT_EXPRESSION) {
        return new FunlQuotientExpressionImpl(node);
      }
      else if (type == REFERENCE_EXPRESSION) {
        return new FunlReferenceExpressionImpl(node);
      }
      else if (type == RETURN) {
        return new FunlReturnImpl(node);
      }
      else if (type == SUM_EXPRESSION) {
        return new FunlSumExpressionImpl(node);
      }
      else if (type == UNARY_MINUS_EXPRESSION) {
        return new FunlUnaryMinusExpressionImpl(node);
      }
      else if (type == UNARY_PLUS_EXPRESSION) {
        return new FunlUnaryPlusExpressionImpl(node);
      }
      else if (type == VALUE) {
        return new FunlValueImpl(node);
      }
      throw new AssertionError("Unknown element type: " + type);
    }
  }
}
