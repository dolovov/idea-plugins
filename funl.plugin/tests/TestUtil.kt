package org.ddol.idea.plugin.funl.tests

import com.intellij.openapi.util.io.FileUtil
import com.intellij.openapi.vfs.CharsetToolkit
import com.intellij.testFramework.EdtTestUtil
import com.intellij.testFramework.TestRunnerUtil
import com.intellij.util.ThrowableRunnable
import junitparams.JUnitParamsRunner
import org.junit.runner.notification.RunNotifier
import org.junit.runners.model.FrameworkMethod
import java.io.File
import java.nio.file.Paths


val TEST_DATA_PATH = Paths.get("testData").toAbsolutePath().toString()

fun testFile(name: String) = File(TEST_DATA_PATH, name)

fun testFilePath(name: String) = testFile(name).absolutePath.toString()

fun doLoadFile(name: String) = FileUtil.loadFile(testFile(name), CharsetToolkit.UTF8, true)


/**
 * [JUnitParamsRunner] adapted to running IntelliJ IDEA unit tests with fixtures.
 */
class AdaptedJUnitParamsRunner(clazz: Class<Any>) : JUnitParamsRunner(clazz) {

    override fun runChild(method: FrameworkMethod?, notifier: RunNotifier?) {

        TestRunnerUtil.replaceIdeEventQueueSafely()

        EdtTestUtil.runInEdtAndWait(ThrowableRunnable {
            super.runChild(method, notifier)
        })
    }
}
