# Funl plugin for IntelliJ IDEA
## What is Funl?

_Funl_ stands for "Funny Language".

Funl is a very simple procedural programming language that was designed just out of curiosity and for fun.

- It has just one data type (float)
- It allows declaring immutable variables (named "values")
- It allows declaring functions that may accept no or several parameters and must always return a value
- It has a built-on operator for printing values to console
- It does not support conditional constructions, cycles and exception handling
- And the whole Funl program always sits inside of a single file (*.funl)

Example:

![Example](images/example.png)

## What is Funl plugin for IntelliJ IDEA?

It's a plugin for IntelliJ platform that makes it easy developing with Funl language:

- Parsing Funl source code and pointing to syntax errors
- Syntax highlighting [[link](src/tools/SyntaxHighlighting.kt)]
- Formatting [[link](src/tools/Formatting.kt)]
- Annotators (the recommended way to implement inspections for custom languages)
    - Incomplete (invalid, broken) statement [[link](src/annotators/IncompleteStatementAnnotator.kt)]
    - Unresolved reference to value, parameter or function [[link](src/annotators/UnresolvedReferencesAnnotator.kt)]
    - Duplicated value, parameter or function declarations [[link](src/annotators/DuplicatedAndShadowedDeclarationsAnnotator.kt)]
    - Shadowing of global values by local function values or parameters, etc [[link](src/annotators/DuplicatedAndShadowedDeclarationsAnnotator.kt)]
    - Return statement issues (absent, multiple, or located at the wrong place) [[link](src/annotators/ReturnStatementIssueAnnotator.kt)]
    - Two independent statements at the same line of code [[link](src/annotators/TwoStatementsSameLineAnnotator.kt)]
    - Unused element (warning) [[link](src/annotators/UnusedElementAnnotator.kt)]
- Reference navigation (Ctrl-B) and Find Usages (Alt-F7)
- Code completion for keywords and visible variables, parameters and functions [[link](src/tools/CodeCompletion.kt)]
    ![Keyword completion 1](images/keyword-completion1.png)
    ![Keyword completion 2](images/keyword-completion2.png)
    ![Value and function completion](images/value-function-completion.png)
- Refactoring support
    - Renaming (including inline renaming) with name validation and name conflict and shadowing detection [[link1](src/navigation/RenamingRefactoring.kt)], [[link2](src/navigation/FunlRefactoringSupportProvider.kt)], [[link3](src/tools/FunlNamesValidator.kt)]
        ![Name conflict](images/renaming-conflict.png)    
        ![Shadowing](images/renaming-shadowing.png)
    - Safe delete
- Smart enter processor with fixers [[link](src/tools/SmartEnterProcessing.kt)]
    - Fixer for completing unclosed parentheses expression
    - Fixer for typing values
    - Fixer for adding "return" statement to the function with block body
- Commenter [[link](src/tools/FunlCommenter.kt)]
- Paired brace matcher [[link](src/tools/FunlPairedBraceMatcher.kt)]
- Structure view [[link](src/tools/StructureView.kt)]
- Run configuration [[link](src/tools/RunConfiguration.kt)]
    ![Run configuration](images/run-configuration.png)
    ![Running](images/running.png)

## How Funl program is compiled and executed?

Funl plugin allows compiling and executing Funl programs. This includes the following steps:

1. Lexical analysis of the program (by the lexer generated with [JFlex](http://jflex.de/))
2. Parsing and building AST and PSI trees (by the parser generated with [Grammar-Kit](https://github.com/JetBrains/Grammar-Kit))
3. Semantic validation of the program (if there are no errors found at previous steps)
    - There is a set of special "validators" for semantic checks. See [[validation](src/validation)] package for the full list of validators.
    - FYI: The set of "validators" is almost identical to the set of annotators used in IDEA editor to highlight semantic errors. Also, all annotators are just wrappers around the corresponding validators.
4. Generation of instructions for an abstract intermediate language (IL)
    - Currently there are 14 IL instructions that allow creating/destroying stack frames, getting and putting a value to current stack frame, performing basic arithmetic operations on a stack, etc. See the full list of instructions in [[IL.kt](src/interpreter/IL.kt)] file.
5. Executing IL instructions.

The plugin does not allow producing binary code (such as JVM bytecode or x86 instructions). This feature has not been implemented yet. It could be implemented later, by transforming IL instructions to some kind of binary code instructions.

## Why this has been made?

This project has been started just out of curiosity: I wanted to try to build something based on IntelliJ Platform SDK. In the end this led to creation of Funl programming language and a plugin for IntelliJ IDEA that brings the compiler and the basic tooling for Funl (syntax highlighting, annotators, code completion, etc).

The plugin itself is built with [Grammar-Kit](https://github.com/JetBrains/Grammar-Kit).

- Grammar file: [[Funl.bnf](src/Funl.bnf)]
- Lexer file: [[FunlLexer.flex](src/FunlLexer.flex)]

The plugin is fully written in Kotlin (> 5k lines of code) except for parser and lexer Java classes generated with the help of [Grammar-Kit](https://github.com/JetBrains/Grammar-Kit) and [JFlex](http://jflex.de/).
