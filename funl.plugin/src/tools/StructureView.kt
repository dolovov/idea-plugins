package org.ddol.idea.plugin.funl.tools

import com.intellij.ide.structureView.*
import com.intellij.ide.util.treeView.smartTree.TreeElement
import com.intellij.lang.PsiStructureViewFactory
import com.intellij.navigation.ItemPresentation
import com.intellij.openapi.editor.Editor
import com.intellij.psi.NavigatablePsiElement
import com.intellij.psi.PsiFile
import org.ddol.idea.plugin.funl.FunlParserDefinition.Companion.FUNL_FILE
import org.ddol.idea.plugin.funl.FunlParserDefinition.Companion.PROGRAM_ALL_STATEMENTS
import org.ddol.idea.plugin.funl.Icons.ICON_FUNCTION
import org.ddol.idea.plugin.funl.Icons.ICON_INCOMPLETE_STATEMENT
import org.ddol.idea.plugin.funl.Icons.ICON_OUTPUT
import org.ddol.idea.plugin.funl.Icons.ICON_VALUE
import org.ddol.idea.plugin.funl.Texts.TEXT_STRUCTURE_VIEW_INCOMPLETE_FUNCTION
import org.ddol.idea.plugin.funl.Texts.TEXT_STRUCTURE_VIEW_INCOMPLETE_STATEMENT
import org.ddol.idea.plugin.funl.Texts.TEXT_STRUCTURE_VIEW_INCOMPLETE_VALUE
import org.ddol.idea.plugin.funl.Texts.TEXT_STRUCTURE_VIEW_OUTPUT
import org.ddol.idea.plugin.funl.navigation.FunlDocumentationProvider.getCompactSignatureDescriptionPlain
import org.ddol.idea.plugin.funl.psi.*
import javax.swing.Icon

/**
 * To enable "Structure View" for Funl program.
 */
class FunlStructureViewBuilderFactory : PsiStructureViewFactory {

    override fun getStructureViewBuilder(file: PsiFile): StructureViewBuilder? {
        if (file.node.elementType != FUNL_FILE)
            return null

        return object : TreeBasedStructureViewBuilder() {
            override fun createStructureViewModel(editor: Editor?) = FunlStructureViewModel(file, editor)
        }
    }
}

private class FunlStructureViewModel(file: PsiFile, editor: Editor?) :
        StructureViewModelBase(file, editor, FunlStructureViewElement(file)),
        StructureViewModel.ElementInfoProvider {

    init {
        withSuitableClasses(
                FunlFile::class.java,
                FunlFunction::class.java,
                FunlValue::class.java,
                FunlOutput::class.java,
                FunlIncomplete::class.java)
    }

    override fun isAlwaysShowsPlus(element: StructureViewTreeElement?) = false

    override fun isAlwaysLeaf(element: StructureViewTreeElement?) = false
}

private class FunlStructureViewElement(private val element: NavigatablePsiElement) : StructureViewTreeElement {

    init {
        val elementType = element.node.elementType
        assert(elementType == FUNL_FILE || PROGRAM_ALL_STATEMENTS.contains(elementType))
    }

    override fun getValue() = element

    override fun getPresentation() = element.presentation!!

    override fun getChildren(): Array<TreeElement> {
        if (element.node.elementType == FUNL_FILE)
            return element.children
                    .filter { PROGRAM_ALL_STATEMENTS.contains(it.node.elementType) }
                    .map { FunlStructureViewElement(it as NavigatablePsiElement) }
                    .toTypedArray()

        return emptyArray()
    }

    override fun navigate(requestFocus: Boolean) = element.navigate(requestFocus)

    override fun canNavigate() = element.canNavigate()

    override fun canNavigateToSource() = element.canNavigateToSource()
}

class FunlItemPresentation(
        private val text: String,
        private val icon: Icon) : ItemPresentation {

    override fun getLocationString(): String? = null

    override fun getIcon(unused: Boolean) = icon

    override fun getPresentableText() = text

    companion object {

        fun createPresentation(function: FunlFunction): FunlItemPresentation {
            val name = if (function.name.isNullOrEmpty())
                TEXT_STRUCTURE_VIEW_INCOMPLETE_FUNCTION
            else
                getCompactSignatureDescriptionPlain(function)

            return FunlItemPresentation(name, ICON_FUNCTION)
        }

        fun createPresentation(value: FunlValue): FunlItemPresentation {
            val name = if (value.name.isNullOrEmpty())
                TEXT_STRUCTURE_VIEW_INCOMPLETE_VALUE
            else
                getCompactSignatureDescriptionPlain(value)

            return FunlItemPresentation(name, ICON_VALUE)
        }

        fun createPresentation(output: FunlOutput) = FunlItemPresentation(
                TEXT_STRUCTURE_VIEW_OUTPUT(output.expression?.text.orEmpty()),
                ICON_OUTPUT)

        fun createPresentation(@Suppress("UNUSED_PARAMETER") incomplete: FunlIncomplete) = FunlItemPresentation(
                TEXT_STRUCTURE_VIEW_INCOMPLETE_STATEMENT,
                ICON_INCOMPLETE_STATEMENT)
    }
}
