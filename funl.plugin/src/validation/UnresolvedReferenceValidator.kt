package org.ddol.idea.plugin.funl.validation

import com.intellij.psi.tree.TokenSet.create
import org.ddol.idea.plugin.funl.Texts.TEXT_ERROR_UNRESOLVED_REFERENCE
import org.ddol.idea.plugin.funl.psi.FunlReferenceExpression
import org.ddol.idea.plugin.funl.psi.FunlTypes.REFERENCE_EXPRESSION
import org.ddol.idea.plugin.funl.validation.FunlIssueLevel.ERROR

/**
 * Validator: Reports unresolved references.
 */
object UnresolvedReferenceValidator : FunlValidator<FunlReferenceExpression> {

    override val matchingElementTypes = create(REFERENCE_EXPRESSION)

    override fun validate(element: FunlReferenceExpression, sink: FunlIssueSink) {

        val name = element.getName()?.takeIf { it.isNotEmpty() } ?: return

        if (element.multiResolve(false).isEmpty())
            sink.addIssue(FunlIssue(ERROR, element.textRange, TEXT_ERROR_UNRESOLVED_REFERENCE(name)))
    }
}
