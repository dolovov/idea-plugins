// This is a generated file. Not intended for manual editing.
package org.ddol.idea.plugin.funl.psi.impl;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.util.PsiTreeUtil;
import static org.ddol.idea.plugin.funl.psi.FunlTypes.*;
import com.intellij.extapi.psi.ASTWrapperPsiElement;
import org.ddol.idea.plugin.funl.psi.*;
import org.ddol.idea.plugin.funl.navigation.FunlElementWithIdentifier;
import org.ddol.idea.plugin.funl.tools.FunlItemPresentation;

public class FunlFunctionImpl extends ASTWrapperPsiElement implements FunlFunction {

  public FunlFunctionImpl(ASTNode node) {
    super(node);
  }

  public void accept(@NotNull FunlVisitor visitor) {
    visitor.visitFunction(this);
  }

  public void accept(@NotNull PsiElementVisitor visitor) {
    if (visitor instanceof FunlVisitor) accept((FunlVisitor)visitor);
    else super.accept(visitor);
  }

  @Override
  @Nullable
  public FunlBlockBody getBlockBody() {
    return findChildByClass(FunlBlockBody.class);
  }

  @Override
  @Nullable
  public FunlExpression getExpression() {
    return findChildByClass(FunlExpression.class);
  }

  @Override
  @NotNull
  public List<FunlParameter> getParameterList() {
    return PsiTreeUtil.getChildrenOfTypeAsList(this, FunlParameter.class);
  }

  @Override
  @Nullable
  public PsiElement getIdentifier() {
    return findChildByType(IDENTIFIER);
  }

  @Nullable
  public String getName() {
    return InjectedMethods.getName(this);
  }

  @Nullable
  public FunlElementWithIdentifier setName(String newName) {
    return InjectedMethods.setName(this, newName);
  }

  @Nullable
  public PsiElement getNameIdentifier() {
    return InjectedMethods.getNameIdentifier(this);
  }

  public int getTextOffset() {
    return InjectedMethods.getTextOffset(this);
  }

  @NotNull
  public FunlItemPresentation getPresentation() {
    return InjectedMethods.getPresentation(this);
  }

}
