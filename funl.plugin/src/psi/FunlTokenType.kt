package org.ddol.idea.plugin.funl.psi

import com.intellij.psi.tree.IElementType
import org.ddol.idea.plugin.funl.FunlLanguage

/**
 * Class that represents each Funl token type.
 */
class FunlTokenType(val shortName: String) : IElementType(shortName, FunlLanguage) {

    override fun toString() = when {
        shortName.length == 1 -> shortName
        else -> shortName.replace('_', ' ')
    }
}
