package org.ddol.idea.plugin.funl.validation

import com.intellij.openapi.util.TextRange
import com.intellij.psi.PsiElement
import com.intellij.psi.TokenType.WHITE_SPACE
import com.intellij.psi.tree.TokenSet.create
import org.ddol.idea.plugin.funl.Texts.TEXT_ERROR_ABSENT_RETURN_STATEMENT
import org.ddol.idea.plugin.funl.Texts.TEXT_ERROR_MULTIPLE_RETURN_STATEMENTS
import org.ddol.idea.plugin.funl.Texts.TEXT_ERROR_RETURN_STATEMENT_INCORRECT_POSITION
import org.ddol.idea.plugin.funl.psi.*
import org.ddol.idea.plugin.funl.psi.FunlTypes.*
import org.ddol.idea.plugin.funl.validation.FunlIssueLevel.ERROR

/**
 * Validator: Report duplicated, missed or misplaced [FunlReturn] statements inside [FunlBlockBody].
 */
object ReturnStatementIssueValidator : FunlValidator<FunlFunction> {

    const val SUGGESTION_INSERT_RETURN_STATEMENT = "insertReturnStatement"

    override val matchingElementTypes = create(FUNCTION)

    override fun validate(element: FunlFunction, sink: FunlIssueSink) {

        // validate function only if it has block body
        element.blockBody?.let { blockBody ->
            val returnStatements = blockBody.children.filter { it.node.elementType == RETURN }
            when (returnStatements.size) {
                0 -> reportAbsentReturnStatement(blockBody, sink)
                1 -> {
                    val returnStatement = returnStatements.first()
                    if (!isReturnStatementInCorrectPosition(returnStatement))
                        reportReturnStatementIncorrectPosition(returnStatement, sink)
                }
                else -> reportMultipleReturnStatement(returnStatements, sink)
            }
        }

    }

    /**
     * Check if the [FunlReturn] statement is at the correct position inside of the function's block body.
     */
    private fun isReturnStatementInCorrectPosition(returnStatement: PsiElement) =
            getNextSiblingIgnoreElements(returnStatement, WHITE_SPACE)?.node?.elementType == BRACE_CLOSE

    /**
     * Report absence of return statement in function's block body.
     */
    private fun reportAbsentReturnStatement(blockBody: FunlBlockBody, sink: FunlIssueSink) {

        blockBody.lastChild.takeIf { it.node.elementType == BRACE_CLOSE }?.let { closingBraceElement ->

            val precedingWhitespace = closingBraceElement.prevSibling.takeIf { it.node.elementType == WHITE_SPACE }
            val closingBraceRange = closingBraceElement.textRange

            // the highlighting rang will include trailing '}' character and preceding whitespace (if exists)
            val highlightingRange = TextRange(
                    precedingWhitespace?.textRange?.startOffset ?: closingBraceRange.startOffset,
                    closingBraceRange.endOffset)

            sink.addIssue(
                    FunlIssue(ERROR, highlightingRange, TEXT_ERROR_ABSENT_RETURN_STATEMENT)
                            .withFixSuggestion(SUGGESTION_INSERT_RETURN_STATEMENT, closingBraceElement))
        }
    }

    /**
     * Report that the return statement that is at the wrong position inside of function's block body.
     */
    private fun reportReturnStatementIncorrectPosition(returnStatement: PsiElement, sink: FunlIssueSink) =
            sink.addIssue(FunlIssue(ERROR, findFirstLeafElement(returnStatement)!!, TEXT_ERROR_RETURN_STATEMENT_INCORRECT_POSITION))

    /**
     * Report that multiple return statements inside of the same function's block body.
     */
    private fun reportMultipleReturnStatement(returnStatements: List<PsiElement>, sink: FunlIssueSink) {
        returnStatements.forEach { returnStatement ->
            sink.addIssue(FunlIssue(ERROR, returnStatement, TEXT_ERROR_MULTIPLE_RETURN_STATEMENTS))
        }
    }
}
