package org.ddol.idea.plugin.funl;

import com.intellij.lexer.FlexLexer;
import com.intellij.psi.tree.IElementType;

import static com.intellij.psi.TokenType.BAD_CHARACTER;
import static com.intellij.psi.TokenType.WHITE_SPACE;
import static org.ddol.idea.plugin.funl.psi.FunlTypes.*;

%%

%{
  public FunlLexer() {
    this((java.io.Reader)null);
  }
%}

%public
%class FunlLexer
%implements FlexLexer
%function advance
%type IElementType
%unicode
%eof{  return;
%eof}

EOL=\R
WHITE_SPACE=\s+

IDENTIFIER=[:letter:]+[a-zA-Z_0-9]*
NUMBER=[0-9]+|[0-9]+\.[0-9]+|[0-9]+\.|\.[0-9]+
LINE_COMMENT="//"[^\r\n]*
BLOCK_COMMENT = "/*"(([^"*"]|[\r\n])*("*"+[^"*""/"])?)("*"|"*"+"/")?

%%
<YYINITIAL> {
  {WHITE_SPACE}        { return WHITE_SPACE; }

  "func"               { return KEYWORD_FUNC; }
  "return"             { return KEYWORD_RETURN; }
  "val"                { return KEYWORD_VAL; }
  "say"                { return KEYWORD_SAY; }
  "+"                  { return OP_PLUS; }
  "-"                  { return OP_MINUS; }
  "*"                  { return OP_MULT; }
  "/"                  { return OP_DIV; }
  "="                  { return OP_EQ; }
  "("                  { return PAREN_OPEN; }
  ")"                  { return PAREN_CLOSE; }
  "{"                  { return BRACE_OPEN; }
  "}"                  { return BRACE_CLOSE; }
  ","                  { return COMMA; }

  {IDENTIFIER}         { return IDENTIFIER; }
  {NUMBER}             { return NUMBER; }
  {LINE_COMMENT}       { return LINE_COMMENT; }
  {BLOCK_COMMENT}      { return BLOCK_COMMENT; }

}

[^] { return BAD_CHARACTER; }
