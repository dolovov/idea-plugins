package org.ddol.idea.plugin.funl.tools

import com.intellij.lang.ASTNode
import com.intellij.lang.SmartEnterProcessorWithFixers
import com.intellij.openapi.editor.Editor
import com.intellij.psi.PsiElement
import com.intellij.psi.PsiFile
import com.intellij.psi.TokenType.ERROR_ELEMENT
import com.intellij.psi.TokenType.WHITE_SPACE
import com.intellij.psi.impl.source.tree.TreeUtil.*
import com.intellij.psi.tree.IElementType
import com.intellij.psi.tree.TokenSet
import org.ddol.idea.plugin.funl.FunlParserDefinition.Companion.COMMON_BODY_ALL_STATEMENTS
import org.ddol.idea.plugin.funl.FunlParserDefinition.Companion.ANY_BODY_ALL_STATEMENTS
import org.ddol.idea.plugin.funl.FunlParserDefinition.Companion.FUNL_FILE
import org.ddol.idea.plugin.funl.psi.FunlBlockBody
import org.ddol.idea.plugin.funl.psi.FunlTypes.*
import org.ddol.idea.plugin.funl.psi.FunlValue
import org.ddol.idea.plugin.funl.psi.getTreeNextIgnoreElements
import org.ddol.idea.plugin.funl.psi.getTreePreviousIgnoreElements
import org.ddol.idea.plugin.funl.unreachableCode

/**
 * Support of "Smart Enter" feature for Funl.
 * Activated by pressing Shift+Cmd+Enter in MacOS.
 */
class FunlSmartEnterProcessor : SmartEnterProcessorWithFixers() {

    init {
        addEnterProcessors(FunlEnterProcessor)

        addFixers(
                ParenthesesExpressionFixer,
                ValueAssignmentFixer,
                FunctionBlockBodyWithoutReturnStatementFixer)
    }

    /**
     * Collect all parents of the [PsiElement] where smart enter was triggered.
     */
    override fun collectAdditionalElements(element: PsiElement, result: MutableList<PsiElement>) {
        var parent = element.parent
        while (true) {
            if (parent == null || parent.node.elementType == FUNL_FILE)
                return

            result.add(parent)
            parent = parent.parent
        }
    }
}

private object FunlEnterProcessor : SmartEnterProcessorWithFixers.FixEnterProcessor() {
    override fun doEnter(unused1: PsiElement?, unused2: PsiFile?, unused3: Editor, unused4: Boolean) = true
}

/**
 * Fixer for completing of parentheses:
 * 1. Type: "val x = ("
 * 2. Press: Shift+Cmd+Enter
 * 3. Result "val x = ( <cursor> )"
 */
private object ParenthesesExpressionFixer : SmartEnterProcessorWithFixers.Fixer<FunlSmartEnterProcessor>() {
    override fun apply(editor: Editor, unused: FunlSmartEnterProcessor, element: PsiElement) {

        val parenthesesExpressionNode = when (element.node.elementType) {
            PARENTHESES_EXPRESSION -> element.node // either the element itself is parentheses expression
            WHITE_SPACE -> { // or is a whitespace immediately after parentheses expression
                prevLeaf(element.node)?.let { findParent(it, PARENTHESES_EXPRESSION_TS, ANY_BODY_ALL_STATEMENTS) }
            }
            else -> null
        } ?: return

        ensureNodeNotMatches(parenthesesExpressionNode.lastChildNode, PAREN_CLOSE) { lastChildNode ->
            val firstChildNode = parenthesesExpressionNode.firstChildNode
            val lastValidChildNodeLength = getTreePreviousIgnoreElements(lastChildNode, WHITE_SPACE, ERROR_ELEMENT, PAREN_OPEN)?.textLength ?: 0
            handleTrailingSpaces(editor, parenthesesExpressionNode, firstChildNode)
            insertFix(editor, " ", firstChildNode.textRange.endOffset, null)
            insertFix(editor, " )", firstChildNode.textRange.endOffset + 1 + lastValidChildNodeLength, 0)
        }
    }

    private val PARENTHESES_EXPRESSION_TS = TokenSet.create(PARENTHESES_EXPRESSION)
}

/**
 * Fixer for typing [FunlValue]s:
 * 1. Type: "val x"
 * 2. Press: Shift+Cmd+Enter
 * 3. Result "val x = <cursor>"
 */
private object ValueAssignmentFixer : SmartEnterProcessorWithFixers.Fixer<FunlSmartEnterProcessor>() {
    override fun apply(editor: Editor, unused: FunlSmartEnterProcessor, element: PsiElement) {

        val valueNode = when (element.node.elementType) {
            VALUE -> element.node // either the element itself is value
            WHITE_SPACE -> { // or is a whitespace immediately after value
                prevLeaf(element.node)?.let { findParent(it, VALUE_TS, ANY_BODY_ALL_STATEMENTS) }
            }
            else -> null
        } ?: return

        ensureNodeMatches(valueNode.firstChildNode, KEYWORD_VAL) { valKeyword ->
            ensureNodeMatches(getTreeNextIgnoreElements(valKeyword, WHITE_SPACE), IDENTIFIER) { identifier ->
                ensureNodeNotMatches(getTreeNextIgnoreElements(identifier, WHITE_SPACE), OP_EQ) {
                    handleTrailingSpaces(editor, valueNode, identifier)
                    insertFix(editor, " = ", identifier.textRange.endOffset, 3)
                }
            }
        }
    }

    private val VALUE_TS = TokenSet.create(VALUE)
}

/**
 * A facade-function for [FunctionBlockBodyWithoutReturnStatementFixer.doApply].
 */
fun fixFunctionBlockBodyWithoutReturnStatement(editor: Editor, element: PsiElement) =
    FunctionBlockBodyWithoutReturnStatementFixer.doApply(editor, element)

/**
 * Fixer for adding "return" statement in function's [FunlBlockBody]:
 * 1. Type some function code:
 *    func foo() {
 *      val x = 1
 *      ...
 *    }"
 * 2. Put cursor somewhere between the last statement in block body and '}' and press Shift+Cmd+Enter.
 * 3. Result:
 *    func foo() {
 *      val x = 1
 *      ...
 *      return <cursor>
 *    }
 */
private object FunctionBlockBodyWithoutReturnStatementFixer : SmartEnterProcessorWithFixers.Fixer<FunlSmartEnterProcessor>() {

    override fun apply(editor: Editor, unused: FunlSmartEnterProcessor, element: PsiElement) = doApply(editor, element)

    internal fun doApply(editor: Editor, element: PsiElement) {

        // various preconditions
        if (element.parent.node.elementType != BLOCK_BODY
                || element.parent.lastChild.node.elementType != BRACE_CLOSE
                || element.node.elementType == RETURN
                || findSiblingBackward(element.parent.lastChild.node, RETURN) != null
                || findSibling(element.node, COMMON_BODY_ALL_STATEMENTS) != null)
            return

        val replacementStartOffset = getReplacementStartOffset(element)
        val replacementEndOffset = element.parent.lastChild.textRange.startOffset
        val fixText = getFixText(element)

        if (replacementStartOffset >= replacementEndOffset)
            // insert
            editor.document.insertString(replacementStartOffset, fixText)
        else
            // replace
            editor.document.replaceString(replacementStartOffset, replacementEndOffset, fixText)

        editor.caretModel.moveToOffset(replacementStartOffset + fixText.length - 1)
    }

    private fun getReplacementStartOffset(element: PsiElement): Int {
        return when (element.node.elementType) {
            BRACE_OPEN, in COMMON_BODY_ALL_STATEMENTS -> element.textRange.endOffset
            WHITE_SPACE -> element.prevSibling.textRange.endOffset
            BRACE_CLOSE, ERROR_ELEMENT -> getReplacementStartOffset(element.prevSibling)
            else -> unreachableCode()
        }
    }

    private fun getFixText(element: PsiElement): String {
        return when (element.node.elementType) {
            BRACE_OPEN -> "\nreturn \n"
            in COMMON_BODY_ALL_STATEMENTS -> "\n\nreturn \n"
            WHITE_SPACE, BRACE_CLOSE, ERROR_ELEMENT -> getFixText(element.prevSibling)
            else -> unreachableCode()
        }
    }
}

private fun ensureNodeMatches(node: ASTNode?, elementType: IElementType, block: (ASTNode) -> Unit) {
    if (node != null && node.elementType == elementType)
        block(node)
}

private fun ensureNodeNotMatches(node: ASTNode?, elementType: IElementType, block: (ASTNode) -> Unit) {
    if (node != null && node.elementType != elementType)
        block(node)
}

private fun handleTrailingSpaces(
        editor: Editor,
        fixedElementNode: ASTNode,
        lastValidNode: ASTNode) {

    val whitespaceAfterNode = nextLeaf(lastValidNode)?.let {
        when (it.elementType) {
            ERROR_ELEMENT -> nextLeaf(it)
            WHITE_SPACE -> it
            else -> null
        }
    } ?: return

    val nodeAfterWhitespace = if (whitespaceAfterNode.treeParent == fixedElementNode) whitespaceAfterNode.treeNext else null

    val whitespaceText = whitespaceAfterNode.text
    val newLineIndex = whitespaceText.indexOf('\n')

    val newWhitespaceText = when {
        nodeAfterWhitespace != null -> if (nodeAfterWhitespace.elementType == BRACE_CLOSE) "\n" else ""
        newLineIndex == -1 -> ""
        newLineIndex == 0 -> return
        else -> whitespaceText.substring(newLineIndex)
    }

    val whitespaceTextRange = whitespaceAfterNode.textRange
    editor.document.replaceString(whitespaceTextRange.startOffset, whitespaceTextRange.endOffset, newWhitespaceText)
}

private fun insertFix(editor: Editor, fix: String, insertOffset: Int, caretOffset: Int?) {
    editor.document.insertString(insertOffset, fix)

    if (caretOffset != null)
        editor.caretModel.moveToOffset(insertOffset + caretOffset)
}
