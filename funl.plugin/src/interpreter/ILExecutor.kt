package org.ddol.idea.plugin.funl.interpreter

import org.ddol.idea.plugin.funl.interpreter.OutputType.STDOUT

/**
 * Execute the given IL code.
 */
fun executeIL(instructions: List<ILInstruction>, output: (OutputType, String) -> Unit)
        = loadAndValidate(instructions).execute(output)

/**
 * Represents a program to execute.
 * - [functionTable] a map (table) of functions, where key is [FunctionSignature] which can be used to address
 *   a concrete function and body is the list of [ILInstruction]s
 * - [freeInstructions] the instructions to start executing immediately
 */
private class Program(
        private val functionTable: Map<FunctionSignature, List<ILInstruction>>,
        private val freeInstructions: List<ILInstruction>) : Iterator<ILInstruction> {

    private val stack = FunlStack<Iterator<ILInstruction>>()
    private var instructionPointer = freeInstructions.iterator()

    /**
     * Execute the program.
     */
    fun execute(output: (OutputType, String) -> Unit) {

        forEach { instruction ->
            when (instruction) {

                is ILCreateFrame -> stack.createFrame(instruction.operandsCount)
                is ILDestroyFrame -> stack.destroyFrame(instruction.operandsCount)

                is ILPutValue -> stack.currentFrame().putValue(instruction.name, stack.currentFrame().pop())
                is ILGetValue -> {
                    val result = stack.currentFrame().getValue(instruction.name)
                            ?: stack.firstFrame().getValue(instruction.name)
                            ?: failExecution("Unresolved value or parameter reference: ${instruction.name}")
                    stack.currentFrame().push(result)
                }

                is ILSummarize, is ILSubtract, is ILMultiply, is ILDivide -> {
                    val right = stack.currentFrame().pop()
                    val left = stack.currentFrame().pop()

                    val result = when (instruction) {
                        is ILSummarize -> left + right
                        is ILSubtract -> left - right
                        is ILMultiply -> left * right
                        is ILDivide -> left / right // may cause arithmetic exception
                        else -> failExecution("Unexpected instruction type: $instruction")
                    }

                    stack.currentFrame().push(result)
                }

                is ILLiteral -> stack.currentFrame().push(instruction.value)
                is ILNegate -> stack.currentFrame().push(stack.currentFrame().pop().negate())

                is ILPrint -> output(STDOUT, "${stack.currentFrame().pop()}\n")

                is ILCall -> {
                    val signature = FunctionSignature.create(instruction)
                    val function = functionTable[signature] ?: failExecution("Unresolved function $instruction")

                    stack.currentFrame().setReturnAddress(instructionPointer)
                    instructionPointer = function.iterator()
                }
                is ILBodyStart -> {}
                is ILBodyEnd -> {
                    stack.currentFrame().getReturnAddress()?.let { restoredInstructionPointer ->
                        instructionPointer = restoredInstructionPointer
                    }
                }
            }
        }
    }

    override fun hasNext() = instructionPointer.hasNext()

    override fun next() = instructionPointer.next()
}

/**
 * Validate the given list of [ILInstruction]s.
 * Build function tables and "free instructions" list.
 */
private fun loadAndValidate(instructions: List<ILInstruction>): Program {

    if (instructions.isEmpty())
        failExecution("No instructions supplied")

    val functionTable = mutableMapOf<FunctionSignature, List<ILInstruction>>()
    val freeInstructions = mutableListOf<ILInstruction>()

    val iterator = instructions.iterator()
    iterator.forEach { instruction ->
        when (instruction) {

            is ILBodyStart -> {
                val signature = FunctionSignature.create(instruction)
                assertExecution(signature !in functionTable) { "Function $instruction declared more than once" }

                functionTable[signature] = readInstructionsTillBodyEnd(instruction, iterator)
            }

            is ILBodyEnd -> failExecution("Unpaired $instruction instruction")

            else -> freeInstructions.add(instruction)
        }
    }

    if (freeInstructions.isEmpty())
        failExecution("No free instructions - nothing to execute")

    return Program(functionTable, freeInstructions)
}

private fun readInstructionsTillBodyEnd(bodyStart: ILBodyStart, iterator: Iterator<ILInstruction>): List<ILInstruction> {
    val result = mutableListOf<ILInstruction>(bodyStart)

    iterator.forEach { instruction ->
        result.add(instruction)
        if (instruction === ILBodyEnd)
            return result
    }

    failExecution("Unclosed IL body: $result")
}

private data class FunctionSignature(val name: String, val parametersCount: Int) {

    companion object {
        fun create(bodyStart: ILBodyStart) = FunctionSignature(bodyStart.name, bodyStart.parametersCount)
        fun create(call: ILCall) = FunctionSignature(call.name, call.parametersCount)
    }
}
