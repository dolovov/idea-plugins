package org.ddol.idea.plugin.funl.psi

import com.intellij.extapi.psi.PsiFileBase
import com.intellij.psi.FileViewProvider
import com.intellij.psi.PsiFile
import org.ddol.idea.plugin.funl.FunlFileType
import org.ddol.idea.plugin.funl.FunlLanguage
import org.ddol.idea.plugin.funl.Texts.TEXT_PLUGIN_FILE_TYPE

/**
 * An implementation of [PsiFile] for Funl language.
 */
class FunlFile(viewProvider: FileViewProvider) : PsiFileBase(viewProvider, FunlLanguage) {

    override fun getFileType() = FunlFileType

    override fun toString() = TEXT_PLUGIN_FILE_TYPE
}
