package org.ddol.idea.plugin.funl

import com.intellij.lang.ASTNode
import com.intellij.lang.Language
import com.intellij.lang.LanguageUtil.canStickTokensTogetherByLexer
import com.intellij.lang.ParserDefinition
import com.intellij.lexer.FlexAdapter
import com.intellij.openapi.fileTypes.FileTypeConsumer
import com.intellij.openapi.fileTypes.FileTypeFactory
import com.intellij.openapi.fileTypes.LanguageFileType
import com.intellij.openapi.project.Project
import com.intellij.psi.FileViewProvider
import com.intellij.psi.PsiElement
import com.intellij.psi.tree.IFileElementType
import com.intellij.psi.tree.TokenSet
import com.intellij.psi.tree.TokenSet.*
import org.ddol.idea.plugin.funl.psi.FunlFile
import org.ddol.idea.plugin.funl.psi.FunlTypes
import org.ddol.idea.plugin.funl.psi.FunlTypes.*

class FunlParserDefinition : ParserDefinition {

    override fun createLexer(unused: Project?) = createLexer()

    override fun createParser(unused: Project?) = FunlParser()

    override fun getFileNodeType() = FUNL_FILE

    override fun getCommentTokens() = COMMENT_TOKENS

    override fun getStringLiteralElements(): TokenSet = EMPTY

    override fun createElement(node: ASTNode): PsiElement = FunlTypes.Factory.createElement(node)

    override fun createFile(viewProvider: FileViewProvider) = FunlFile(viewProvider)

    override fun spaceExistanceTypeBetweenTokens(left: ASTNode, right: ASTNode): ParserDefinition.SpaceRequirements {
        return when (left.elementType) {
            LINE_COMMENT -> ParserDefinition.SpaceRequirements.MUST_LINE_BREAK
            else -> canStickTokensTogetherByLexer(left, right, createLexer(left.psi.project))
        }
    }

    @Suppress("unused", "MemberVisibilityCanBePrivate")
    companion object {
        val FUNL_FILE = IFileElementType(FunlLanguage)

        val IDENTIFIER_TOKENS = create(IDENTIFIER)
        val LITERAL_TOKENS = create(LITERAL_EXPRESSION)
        val COMMENT_TOKENS = create(LINE_COMMENT, BLOCK_COMMENT)
        val KEYWORD_TOKENS = create(KEYWORD_FUNC, KEYWORD_RETURN, KEYWORD_VAL, KEYWORD_SAY)
        val OPERATOR_TOKENS = create(OP_PLUS, OP_MINUS, OP_MULT, OP_DIV, OP_EQ)
        val EXPRESSION_TOKENS = create(SUM_EXPRESSION, DIFFERENCE_EXPRESSION, PRODUCT_EXPRESSION, QUOTIENT_EXPRESSION, UNARY_MINUS_EXPRESSION, UNARY_PLUS_EXPRESSION, LITERAL_EXPRESSION, PARENTHESES_EXPRESSION, REFERENCE_EXPRESSION, EXPRESSION)
        val NAMED_ELEMENT_TOKENS = create(FUNCTION, PARAMETER, VALUE)

        // common statements for function body and program body
        val COMMON_BODY_VALID_STATEMENTS = create(VALUE, OUTPUT)
        val COMMON_BODY_ALL_STATEMENTS = orSet(COMMON_BODY_VALID_STATEMENTS, create(INCOMPLETE))
        val COMMON_BODY_KEYWORD_TOKENS = create(KEYWORD_VAL, KEYWORD_SAY)

        // program statements
        val PROGRAM_VALID_STATEMENTS = orSet(COMMON_BODY_VALID_STATEMENTS, create(FUNCTION))
        val PROGRAM_ALL_STATEMENTS = orSet(COMMON_BODY_ALL_STATEMENTS, create(FUNCTION))
        val PROGRAM_KEYWORD_TOKENS = orSet(COMMON_BODY_KEYWORD_TOKENS, create(KEYWORD_FUNC))

        // function block body statements
        val BLOCK_BODY_VALID_STATEMENTS = orSet(COMMON_BODY_VALID_STATEMENTS, create(RETURN))
        val BLOCK_BODY_ALL_STATEMENTS = orSet(COMMON_BODY_ALL_STATEMENTS, create(RETURN))
        val BLOCK_BODY_KEYWORD_TOKENS = orSet(COMMON_BODY_KEYWORD_TOKENS, create(KEYWORD_RETURN))

        // all possible enclosing statements
        val ANY_BODY_VALID_STATEMENTS = orSet(COMMON_BODY_VALID_STATEMENTS, create(FUNCTION), create(RETURN))
        val ANY_BODY_ALL_STATEMENTS = orSet(COMMON_BODY_ALL_STATEMENTS, create(FUNCTION), create(RETURN))
        val ANY_BODY_KEYWORD_TOKENS = orSet(COMMON_BODY_KEYWORD_TOKENS, create(KEYWORD_FUNC), create(KEYWORD_RETURN))

        fun createLexer() = FunlLexerAdapter()
    }
}

object FunlLanguage : Language("Funl")

object FunlFileType : LanguageFileType(FunlLanguage) {

    override fun getName() = Texts.TEXT_PLUGIN_FILE_TYPE

    override fun getDescription() = Texts.TEXT_PLUGIN_FILE_DESCRIPTION

    override fun getDefaultExtension() = "funl"

    override fun getIcon() = Icons.ICON_FILE
}

class FunlFileTypeFactory : FileTypeFactory() {

    override fun createFileTypes(consumer: FileTypeConsumer) = consumer.consume(FunlFileType)
}

class FunlLexerAdapter : FlexAdapter(FunlLexer())

fun unreachableCode(message: String = ""): Nothing =
        throw RuntimeException("Normally, unreachable code" + if (message.isNotEmpty()) ": $message" else "")
