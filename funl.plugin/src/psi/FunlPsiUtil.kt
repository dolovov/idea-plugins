@file:Suppress("unused")

package org.ddol.idea.plugin.funl.psi

import com.intellij.lang.ASTFactory
import com.intellij.lang.ASTNode
import com.intellij.psi.PsiElement
import com.intellij.psi.TokenType
import com.intellij.psi.TokenType.WHITE_SPACE
import com.intellij.psi.impl.source.codeStyle.CodeEditUtil.setNodeGenerated
import com.intellij.psi.impl.source.tree.TreeUtil
import com.intellij.psi.impl.source.tree.TreeUtil.*
import com.intellij.psi.tree.IElementType
import org.ddol.idea.plugin.funl.navigation.FunlElementWithIdentifier
import org.ddol.idea.plugin.funl.navigation.FunlNamedElement
import org.ddol.idea.plugin.funl.psi.FunlTypes.IDENTIFIER
import org.ddol.idea.plugin.funl.tools.FunlNamesValidator

/**
 * Get the list of [FunlFunction]s in the given [FunlFile] that satisfy the given [predicate].
 */
fun getFunctions(file: FunlFile, predicate: (FunlFunction) -> Boolean = { true }): List<FunlFunction> =
        getChildren(file, predicate)

/**
 * Get the list of global [FunlValue]s in the given [FunlFile] that satisfy the given [predicate].
 */
fun getGlobalValues(file: FunlFile, predicate: (FunlValue) -> Boolean = { true }): List<FunlValue> =
        getChildren(file, predicate)

/**
 * Get the list of local [FunlValue]s in the given [FunlBlockBody] that satisfy the given [predicate].
 */
fun getLocalValues(blockBody: FunlBlockBody, predicate: (FunlValue) -> Boolean = { true }): List<FunlValue> =
        getChildren(blockBody, predicate)

/**
 * Get the list of [FunlParameter]s in the given [FunlFunction] that satisfy the given [predicate].
 */
fun getParameters(function: FunlFunction, predicate: (FunlParameter) -> Boolean = { true }): List<FunlParameter> =
        getChildren(function, predicate)

/**
 * Returns the previous sibling [PsiElement] of the given [PsiElement] if and only if
 * there is no whitespace with new line characters between them.
 */
fun getPreviousSiblingIfNoWhitespaceWithNewLine(element: PsiElement): PsiElement? {
    val previous = element.prevSibling ?: return null
    return if (previous.node.elementType == WHITE_SPACE)
        if (previous.textContains('\n') || previous.textContains('\r'))
            null
        else
            previous.prevSibling
    else
        previous
}

/**
 * Returns the previous sibling [PsiElement] of the given [PsiElement] ignoring any
 * of the specified [ignoredTypes] between them.
 */
fun getPreviousSiblingIgnoreElements(element: PsiElement, vararg ignoredTypes: IElementType) =
        getTreePreviousIgnoreElements(element.node, *ignoredTypes)?.psi

/**
 * Returns the tree previous [ASTNode] of the given [ASTNode] ignoring any
 * of the specified [ignoredTypes] between them.
 */
fun getTreePreviousIgnoreElements(node: ASTNode, vararg ignoredTypes: IElementType): ASTNode? {
    var previous = node.treePrev
    if (ignoredTypes.isEmpty())
        return previous

    while (previous != null) {
        if (ignoredTypes.firstOrNull { it == previous.elementType } == null)
            return previous

        previous = previous.treePrev
    }

    return null
}

/**
 * Returns the next sibling [PsiElement] of the given [PsiElement] ignoring any
 * of the specified [ignoredTypes] between them.
 */
fun getNextSiblingIgnoreElements(element: PsiElement, vararg ignoredTypes: IElementType) =
        getTreeNextIgnoreElements(element.node, *ignoredTypes)?.psi

/**
 * Returns the tree next [ASTNode] of the given [ASTNode] ignoring any
 * of the specified [ignoredTypes] between them.
 */
fun getTreeNextIgnoreElements(node: ASTNode, vararg ignoredTypes: IElementType): ASTNode? {
    var next = node.treeNext
    if (ignoredTypes.isEmpty())
        return next

    while (next != null) {
        if (ignoredTypes.firstOrNull { it == next.elementType } == null)
            return next

        next = next.treeNext
    }

    return null
}

/**
 * A handy wrapper for [TreeUtil.findFirstLeaf] that allows to work with [PsiElement]s.
 */
fun findFirstLeafElement(element: PsiElement) = findFirstLeaf(element.node)?.psi

/**
 * Create a new leaf [ASTNode] for [TokenType.WHITE_SPACE].
 */
fun createWhitespaceNode(whitespace: String): ASTNode {
    // lightweight validation - allow either ' ' or '\n' characters:
    assert(whitespace.firstOrNull { it != ' ' && it != '\n' } == null)

    return ASTFactory.whitespace(whitespace)
}

/**
 * Rename a any instance of [FunlElementWithIdentifier].
 *
 * It is assumed that the validation should already be performed by [FunlNamesValidator].
 */
fun renameElement(element: FunlElementWithIdentifier, newName: String): FunlElementWithIdentifier? {

    val oldIdentifier = element.getIdentifier() ?: return null

    val oldName = element.getName() ?: return null
    if (oldName == newName)
        return null

    val oldNode = oldIdentifier.node ?: return null
    element.node.replaceChild(oldNode, createIdentifierNode(newName))
    return element
}

private inline fun <reified T : FunlNamedElement> getChildren(parent: PsiElement, predicate: (T) -> Boolean): List<T> {
    val children = mutableListOf<T>()

    var child: PsiElement? = parent.firstChild
    while (child != null) {
        if (child is T && predicate(child))
            children.add(child)

        child = child.nextSibling
    }

    return children
}

private fun createIdentifierNode(name: String): ASTNode = ASTFactory.leaf(IDENTIFIER, name).also { setNodeGenerated(it, true) }
