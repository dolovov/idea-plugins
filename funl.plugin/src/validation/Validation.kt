package org.ddol.idea.plugin.funl.validation

import com.intellij.codeInsight.intention.IntentionAction
import com.intellij.codeInspection.QuickFix
import com.intellij.lang.annotation.Annotator
import com.intellij.openapi.util.TextRange
import com.intellij.psi.PsiElement
import com.intellij.psi.tree.TokenSet
import java.util.*

/**
 * Validator for a Funl PSI element of a certain type.
 */
interface FunlValidator<in T : PsiElement> {

    /**
     * Override this property to specify the exact element types (using [TokenSet]) that match this validator.
     * Make sure that [matchingElementTypes] fully conform to [T] generic type.
     */
    val matchingElementTypes: TokenSet

    fun validate(element: T, sink: FunlIssueSink)
}

/**
 * Various levels of issues that may occur in the code.
 */
enum class FunlIssueLevel(private val humanReadableName: String) {
    ERROR("error"),
    WARNING("warn"),
    WEAK_WARNING("info");

    override fun toString() = humanReadableName
}

/**
 * An issue in Funl source code.
 * - [level] a level of the issue such as [FunlIssueLevel.ERROR], [FunlIssueLevel.WARNING], etc
 * - [range] the text range where the issue happened (typically this field is used later in [Annotator] to
 *   highlight the appropriate part of the source code)
 * - [message] an error/warning/notice message
 */
data class FunlIssue(val level: FunlIssueLevel, val range: TextRange, val message: String) : Comparable<FunlIssue> {

    constructor(level: FunlIssueLevel, element: PsiElement, message: String) : this(level, element.textRange, message)

    private val fixSuggestions = LinkedHashMap<String, PsiElement>()

    init {
        assert(message.isNotBlank())
    }

    /**
     * Add a "fix suggestion":
     * - [alias] the alias the determines the type of fix
     * - [element] the PSI element where the fix should be offered at
     *
     * Besides the fact that this is absolutely not a goal of [FunlValidator] to suggest any [QuickFix] or
     * [IntentionAction] that could be applied to the source code to make it valid, it's definitely a good idea
     * for [FunlValidator] to give suggestions about which types of fixes could be potentially applied and where.
     * So that an [Annotator] that could delegate validation logic to [FunlValidator] would be aware of
     * potential fixes and would be able to add concrete [QuickFix]s or [IntentionAction]s on it's own.
     */
    fun withFixSuggestion(alias: String, element: PsiElement): FunlIssue {
        fixSuggestions[alias] = element
        return this
    }

    fun getFixSuggestions(): Map<String, PsiElement> = fixSuggestions

    override fun compareTo(other: FunlIssue): Int {
        val startOffsetDiff = this.range.startOffset - other.range.startOffset
        if (startOffsetDiff != 0)
            return startOffsetDiff

        val reversedLevelDiff = this.level.ordinal - other.level.ordinal
        if (reversedLevelDiff != 0)
            return -reversedLevelDiff

        return this.message.compareTo(other.message)
    }
}

/**
 * A sink to collect issues for further processing.
 */
interface FunlIssueSink {

    fun addIssue(issue: FunlIssue)
}
