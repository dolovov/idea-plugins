package org.ddol.idea.plugin.funl.validation

import com.intellij.psi.tree.TokenSet.create
import com.intellij.util.containers.isNullOrEmpty
import org.ddol.idea.plugin.funl.FunlParserDefinition.Companion.FUNL_FILE
import org.ddol.idea.plugin.funl.Texts.TEXT_ERROR_CONFLICTING_DECLARATIONS
import org.ddol.idea.plugin.funl.Texts.TEXT_WARNING_NAME_SHADOWING
import org.ddol.idea.plugin.funl.navigation.FunlDocumentationProvider.getSignatureDescriptionPlain
import org.ddol.idea.plugin.funl.navigation.FunlNamedElement
import org.ddol.idea.plugin.funl.navigation.ReferencedElementSignature
import org.ddol.idea.plugin.funl.navigation.collectDeclarations
import org.ddol.idea.plugin.funl.psi.FunlFile
import org.ddol.idea.plugin.funl.psi.FunlParameter
import org.ddol.idea.plugin.funl.psi.FunlValue
import org.ddol.idea.plugin.funl.validation.FunlIssueLevel.ERROR
import org.ddol.idea.plugin.funl.validation.FunlIssueLevel.WARNING

/**
 * Validator:
 * - Reports as error duplicated declarations in Funl program.
 * - Reports as warning name shadowing ([FunlValue]s/[FunlParameter]s).
 */
object DuplicatedAndShadowedDeclarationsValidator : FunlValidator<FunlFile> {

    override val matchingElementTypes = create(FUNL_FILE)

    override fun validate(element: FunlFile, sink: FunlIssueSink) {

        // optimization: let's scan the whole file at one shot and find all declarations there
        val declarations = collectDeclarations(element)

        // check for duplicated global values
        declarations.globalValues().forEach { _, globalValues -> reportConflictingDeclarations(globalValues, sink) }

        // pass through functions...
        declarations.functions().forEach { _, functions ->

            // check for duplicated functions
            reportConflictingDeclarations(functions, sink)

            functions.forEach { function ->

                val globalValuesBeforeFunction = declarations.globalValuesBeforeFunction(function)
                val functionParameters = declarations.parameters(function)

                functionParameters.forEach { parameterSignature, parameters ->

                    // check for duplicated parameters
                    reportConflictingDeclarations(parameters, sink)

                    // check whether a parameter shadows a global value
                    reportShadowedDeclarations(parameterSignature, parameters, globalValuesBeforeFunction, sink)
                }

                declarations.localValues(function).forEach { localValueSignature, localValues ->

                    // check for duplicated local values
                    reportConflictingDeclarations(localValues, sink)

                    // check whether a local value shadows a parameter
                    if (!reportShadowedDeclarations(localValueSignature, localValues, functionParameters, sink)) {

                        // check whether a local value shadows a global value
                        reportShadowedDeclarations(localValueSignature, localValues, globalValuesBeforeFunction, sink)
                    }
                }
            }
        }
    }

    /**
     * Report conflicting declarations (if there are any).
     *
     * Parameters:
     * - [siblingsWithSameSignature]: sibling [FunlNamedElement]s of the same type and having the same signature
     * - [sink]: used to report issue
     *
     * Returns true if there are reported issues.
     */
    private fun reportConflictingDeclarations(
            siblingsWithSameSignature: List<FunlNamedElement>,
            sink: FunlIssueSink): Boolean {

        if (siblingsWithSameSignature.size > 1) {
            val message = TEXT_ERROR_CONFLICTING_DECLARATIONS(siblingsWithSameSignature.map { getSignatureDescriptionPlain(it) })
            siblingsWithSameSignature.forEach { element ->
                sink.addIssue(FunlIssue(ERROR, element.getIdentifier()!!, message))
            }

            return true
        }

        return false
    }

    /**
     * Report declarations that shadow other (high-level) declarations (if there are any).
     *
     * Parameters:
     * - [elementSignature]: the signature of the given [elements]
     * - [elements]: the elements that shadow high-level elements (normally there should be just a single element in
     *   the list, multiple elements means duplicated declarations; this should be handled in
     *   [reportConflictingDeclarations] call).
     * - [higherLevelElements]: the [FunlNamedElement]s with the same signature localed at one or more levels higher
     * - [sink]: used to report issue
     *
     * Returns true if there are reported issues.
     */
    private fun reportShadowedDeclarations(
            elementSignature: ReferencedElementSignature,
            elements: List<FunlNamedElement>,
            higherLevelElements: Map<ReferencedElementSignature, List<FunlNamedElement>>,
            sink: FunlIssueSink): Boolean {

        val shadowedElements = higherLevelElements[elementSignature]
        if (!shadowedElements.isNullOrEmpty()) {
            val element = elements.first()
            sink.addIssue(FunlIssue(WARNING, element.getIdentifier()!!, TEXT_WARNING_NAME_SHADOWING(element.name.orEmpty())))

            return true
        }

        return false
    }

}
