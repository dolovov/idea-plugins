// This is a generated file. Not intended for manual editing.
package org.ddol.idea.plugin.funl.psi.impl;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.util.PsiTreeUtil;
import static org.ddol.idea.plugin.funl.psi.FunlTypes.*;
import com.intellij.extapi.psi.ASTWrapperPsiElement;
import org.ddol.idea.plugin.funl.psi.*;

public class FunlBlockBodyImpl extends ASTWrapperPsiElement implements FunlBlockBody {

  public FunlBlockBodyImpl(ASTNode node) {
    super(node);
  }

  public void accept(@NotNull FunlVisitor visitor) {
    visitor.visitBlockBody(this);
  }

  public void accept(@NotNull PsiElementVisitor visitor) {
    if (visitor instanceof FunlVisitor) accept((FunlVisitor)visitor);
    else super.accept(visitor);
  }

  @Override
  @NotNull
  public List<FunlIncomplete> getIncompleteList() {
    return PsiTreeUtil.getChildrenOfTypeAsList(this, FunlIncomplete.class);
  }

  @Override
  @NotNull
  public List<FunlOutput> getOutputList() {
    return PsiTreeUtil.getChildrenOfTypeAsList(this, FunlOutput.class);
  }

  @Override
  @NotNull
  public List<FunlReturn> getReturnList() {
    return PsiTreeUtil.getChildrenOfTypeAsList(this, FunlReturn.class);
  }

  @Override
  @NotNull
  public List<FunlValue> getValueList() {
    return PsiTreeUtil.getChildrenOfTypeAsList(this, FunlValue.class);
  }

}
