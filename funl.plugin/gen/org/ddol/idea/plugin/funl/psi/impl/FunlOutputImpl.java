// This is a generated file. Not intended for manual editing.
package org.ddol.idea.plugin.funl.psi.impl;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.util.PsiTreeUtil;
import static org.ddol.idea.plugin.funl.psi.FunlTypes.*;
import com.intellij.extapi.psi.ASTWrapperPsiElement;
import org.ddol.idea.plugin.funl.psi.*;
import org.ddol.idea.plugin.funl.tools.FunlItemPresentation;

public class FunlOutputImpl extends ASTWrapperPsiElement implements FunlOutput {

  public FunlOutputImpl(ASTNode node) {
    super(node);
  }

  public void accept(@NotNull FunlVisitor visitor) {
    visitor.visitOutput(this);
  }

  public void accept(@NotNull PsiElementVisitor visitor) {
    if (visitor instanceof FunlVisitor) accept((FunlVisitor)visitor);
    else super.accept(visitor);
  }

  @Override
  @Nullable
  public FunlExpression getExpression() {
    return findChildByClass(FunlExpression.class);
  }

  @NotNull
  public FunlItemPresentation getPresentation() {
    return InjectedMethods.getPresentation(this);
  }

}
