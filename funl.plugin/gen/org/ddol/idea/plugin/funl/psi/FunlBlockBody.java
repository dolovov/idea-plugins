// This is a generated file. Not intended for manual editing.
package org.ddol.idea.plugin.funl.psi;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.psi.PsiElement;

public interface FunlBlockBody extends PsiElement {

  @NotNull
  List<FunlIncomplete> getIncompleteList();

  @NotNull
  List<FunlOutput> getOutputList();

  @NotNull
  List<FunlReturn> getReturnList();

  @NotNull
  List<FunlValue> getValueList();

}
