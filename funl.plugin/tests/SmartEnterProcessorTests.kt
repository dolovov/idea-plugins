package org.ddol.idea.plugin.funl.tests

import com.intellij.openapi.actionSystem.IdeActions.ACTION_EDITOR_COMPLETE_STATEMENT
import com.intellij.testFramework.builders.EmptyModuleFixtureBuilder
import com.intellij.testFramework.fixtures.CodeInsightFixtureTestCase
import com.intellij.testFramework.fixtures.ModuleFixture
import org.ddol.idea.plugin.funl.FunlFileType
import org.ddol.idea.plugin.funl.tools.FunlSmartEnterProcessor

/**
 * Tests for [FunlSmartEnterProcessor].
 */
class SmartEnterProcessorTests : CodeInsightFixtureTestCase<EmptyModuleFixtureBuilder<ModuleFixture>>() {

    fun testParenthesesExpressionFixer1() = doTest(
            """
                val x = (<caret>
            """,
            """
                val x = ( <caret> )
            """
    )

    fun testParenthesesExpressionFixer2() = doTest(
            """
                val x = (                <caret>
            """,
            """
                val x = ( <caret> )
            """
    )

    fun testParenthesesExpressionFixer3() = doTest(
            """
                val x = (

                <caret>

            """,
            """
                val x = ( <caret> )



            """
    )

    fun testParenthesesExpressionFixer4() = doTest(
            """
                val test = 123
                val x = (test + 4<caret>
            """,
            """
                val test = 123
                val x = ( test + 4<caret> )
            """
    )

    fun testParenthesesExpressionFixer5() = doTest(
            """
                val test = 123
                val x = ( test + 4<caret>
            """,
            """
                val test = 123
                val x = ( test + 4<caret> )
            """
    )

    fun testParenthesesExpressionFixer6() = doTest(
            """
                val test = 123
                val x = (    test + 4<caret>
            """,
            """
                val test = 123
                val x = ( test + 4<caret> )
            """
    )

    fun testParenthesesExpressionFixer7() = doTest(
            """
                val test = 123
                val x = (  <caret>  test + 4
            """,
            """
                val test = 123
                val x = ( test + 4<caret> )
            """
    )

    fun testParenthesesExpressionFixer8() = doTest(
            """
                val test = 123
                val x = (
                <caret>
                test + 4

            """,
            """
                val test = 123
                val x = ( test + 4<caret> )

            """
    )

    fun testParenthesesExpressionFixer9() = doTest(
            """
                val test = 123
                val x = (

                test + 4
                <caret>

            """,
            """
                val test = 123
                val x = ( test + 4<caret> )


            """
    )

    fun testValueAssignmentFixer1() = doTest(
            """
                val x<caret>
            """,
            """
                val x = <caret>
            """
    )

    fun testValueAssignmentFixer2() = doTest(
            """
                val x         <caret>
            """,
            """
                val x = <caret>
            """
    )

    fun testValueAssignmentFixer3() = doTest(
            """
                val x

                <caret>

            """,
            """
                val x = <caret>



            """
    )

    fun testValueAssignmentFixer4() = doTest(
            """
                val test = 123
                val x<caret> test + 1
            """,
            """
                val test = 123
                val x = <caret>test + 1
            """
    )

    fun testValueAssignmentFixer5() = doTest(
            """
                val test = 123
                val x<caret>           test + 1
            """,
            """
                val test = 123
                val x = <caret>test + 1
            """
    )

    fun testValueAssignmentFixer6() = doTest(
            """
                val test = 123
                val x        <caret>  test + 1
            """,
            """
                val test = 123
                val x = <caret>test + 1
            """
    )

    fun testValueAssignmentFixer7() = doTest(
            """
                val test = 123
                val x
                <caret>
                test + 1

            """,
            """
                val test = 123
                val x = <caret>

                test + 1

            """
    )

    fun testFunctionBlockBodyWithoutReturnStatementFixer1() = doTest(
            """
               func foo() {<caret>}
            """,
            """
                func foo() {
                    return <caret>
                }
            """
    )

    fun testFunctionBlockBodyWithoutReturnStatementFixer2() = doTest(
            """
               func foo() {<caret>

               }
            """,
            """
                func foo() {
                    return <caret>
                }
            """
    )

    fun testFunctionBlockBodyWithoutReturnStatementFixer3() = doTest(
            """
               func foo() {
                   <caret>
               }
            """,
            """
                func foo() {
                    return <caret>
                }
            """
    )

    fun testFunctionBlockBodyWithoutReturnStatementFixer4() = doTest(
            """
               func foo() {

               <caret>}
            """,
            """
                func foo() {
                    return <caret>
                }
            """
    )

    fun testFunctionBlockBodyWithoutReturnStatementFixer5() = doTest(
            """
               func foo() {
                   val x = 5
                    <caret>
               }
            """,
            """
                func foo() {
                    val x = 5

                    return <caret>
                }
            """
    )

    fun testFunctionBlockBodyWithoutReturnStatementFixer6() = doTest(
            """
               func foo() {
                   val x = 5

                   <caret>
               }
            """,
            """
                func foo() {
                    val x = 5

                    return <caret>
                }
            """
    )

    fun testFunctionBlockBodyWithoutReturnStatementFixer7() = doTest(
            """
               func foo() {
                   val x = 5

               <caret>}
            """,
            """
                func foo() {
                    val x = 5

                    return <caret>
                }
            """
    )

    private fun doTest(before: String, after: String) {
        myFixture.configureByText(FunlFileType, before.trimIndent())
        myFixture.performEditorAction(ACTION_EDITOR_COMPLETE_STATEMENT)
        myFixture.checkResult(after.trimIndent())
    }
}
