package org.ddol.idea.plugin.funl

/**
 * Collection of all texts displayed to end user in Funl plugin.
 */
object Texts {

    const val TEXT_ERROR_TWO_STATEMENTS_SAME_LINE = "Individual statements should be separated by new line character"
    val TEXT_ERROR_UNRESOLVED_REFERENCE: s = { "Unresolved reference: '$it'" }
    val TEXT_ERROR_UNRESOLVED_REFERENCE_TEMPLATE = TEXT_ERROR_UNRESOLVED_REFERENCE("{0}")
    val TEXT_ERROR_CONFLICTING_DECLARATIONS: sl = { declarations ->
        StringBuilder("Conflicting declarations: ").appendJoined(declarations).toString()
    }

    val TEXT_ERROR_INCOMPLETE_STATEMENT: s_sl = { firstToken, expectedTokens ->
        StringBuilder().appendJoined(expectedTokens, " or ", SINGLE_QUOTE_WRAPPER)
                .append(" expected, got ")
                .appendWrapped(firstToken, SINGLE_QUOTE_WRAPPER)
                .toString()
    }

    const val TEXT_ERROR_ABSENT_RETURN_STATEMENT = "Expected return statement"
    const val TEXT_ERROR_RETURN_STATEMENT_INCORRECT_POSITION = "The return statement is expected at the end of function's block body"
    const val TEXT_ERROR_MULTIPLE_RETURN_STATEMENTS = "Multiple return statements are not allowed"

    val TEXT_ERROR_FUNCTION_NAME_CONFLICT: s_s = { newName, file -> "Function '$newName' is already declared in file '$file'" }
    val TEXT_ERROR_GLOBAL_VALUE_NAME_CONFLICT: s_s = { newName, file -> "Global value '$newName' is already declared in file '$file'" }
    val TEXT_ERROR_LOCAL_VALUE_NAME_CONFLICT: s_s = { newName, function -> "Value '$newName' is already declared in function '$function'" }
    val TEXT_ERROR_PARAMETER_NAME_CONFLICT: s_s = { newName, function -> "Function parameter '$newName' is already declared in function '$function'" }

    val TEXT_ERROR_REFERENCE_RESOLVING_CHANGES: s = { reference -> "'$reference' will resolve to another value or parameter after renaming" }

    val TEXT_WARNING_NAME_SHADOWING: s = { "Name shadowed: '$it'" }
    val TEXT_WARNING_FUNCTION_NEVER_USED: s = { "Function '$it' is never used" }
    val TEXT_WARNING_PARAMETER_NEVER_USED: s = { "Parameter '$it' is never used" }
    val TEXT_WARNING_VALUE_NEVER_USED: s = { "Value '$it' is never used" }

    const val TEXT_FIX_MOVE_STATEMENT_TO_NEXT_LINE = "Move statement to the next line"
    const val TEXT_FIX_INSERT_RETURN_STATEMENT = "Insert return statement"
    const val TEXT_FIX_DELETE_FUNCTION = "Delete function"
    const val TEXT_FIX_DELETE_VALUE = "Delete value"

    const val TEXT_STRUCTURE_VIEW_INCOMPLETE_FUNCTION = "<incomplete function>"
    const val TEXT_STRUCTURE_VIEW_INCOMPLETE_VALUE = "<incomplete value>"
    val TEXT_STRUCTURE_VIEW_OUTPUT: s = { "say: $it" }
    const val TEXT_STRUCTURE_VIEW_INCOMPLETE_STATEMENT = "<incomplete statement>"

    const val TEXT_PLUGIN_FILE_TYPE = "Funl File"
    const val TEXT_PLUGIN_FILE_DESCRIPTION = "Funny language file"

    val TEXT_SIGNATURE_VALUE_HTML: s = { "<b>val</b> $it" }
    val TEXT_SIGNATURE_VALUE_TEXT: s = { "val $it" }
    val TEXT_COMPACT_SIGNATURE_VALUE_TEXT: s = { it }
    val TEXT_SIGNATURE_PARAMETER_HTML: s = { "<b>function-parameter</b> $it" }
    val TEXT_SIGNATURE_PARAMETER_TEXT: s = { "function-parameter $it" }
    val TEXT_COMPACT_SIGNATURE_PARAMETER_TEXT: s = { it }
    val TEXT_SIGNATURE_FUNCTION_HTML: s_sl = { name, parameters ->
        StringBuilder("<b>func</b> ").append(name).appendParameters(parameters).toString()
    }
    val TEXT_SIGNATURE_FUNCTION_TEXT: s_sl = { name, parameters ->
        StringBuilder("func ").append(name).appendParameters(parameters).toString()
    }
    val TEXT_COMPACT_SIGNATURE_FUNCTION_TEXT: s_sl = { name, parameters ->
        StringBuilder(name).appendParameters(parameters).toString()
    }

    const val TEXT_ELEMENT_TYPE_VALUE = "value"
    const val TEXT_ELEMENT_TYPE_PARAMETER = "parameter"
    const val TEXT_ELEMENT_TYPE_FUNCTION = "function"

    const val TEXT_UNNAMED_ELEMENT = "<unnamed>"

    const val TEXT_RUN_CONFIGURATION_NAME = "Funl"
    const val TEXT_RUN_CONFIGURATION_DESCRIPTION = "Run Funl program"
    const val TEXT_RUN_CONFIGURATION_FILE_PATH = "&Path to Funl file:"
    const val TEXT_RUN_CONFIGURATION_ERROR_NO_FILE_SPECIFIED = "Funl file not found"
}

private fun StringBuilder.appendWrapped(
        value: String,
        wrapper: w = DEFAULT_WRAPPER): StringBuilder = wrapper.invoke(this, value)

private fun StringBuilder.appendJoined(
        items: List<String>,
        lastItemPrefix: String = ", ",
        wrapper: w = DEFAULT_WRAPPER): StringBuilder {

    appendWrapped(items.first(), wrapper)

    if (items.size > 1) {
        for (i in 1 until items.size - 1)
            append(", ").appendWrapped(items[i], wrapper)

        append(lastItemPrefix).appendWrapped(items.last(), wrapper)
    }

    return this
}

private fun StringBuilder.appendParameters(parameters: List<String>): StringBuilder {
    append('(')
    if (parameters.isNotEmpty())
        append(' ').appendJoined(parameters).append(' ')
    append(')')

    return this
}

private val DEFAULT_WRAPPER: w = { append(it) }
private val SINGLE_QUOTE_WRAPPER: w = { append('\'').append(it).append('\'') }

private typealias s = (String) -> String
private typealias s_s = (String, String) -> String
private typealias sl = (List<String>) -> String
private typealias s_sl = (String, List<String>) -> String

private typealias w = StringBuilder.(String) -> StringBuilder
