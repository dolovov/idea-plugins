package org.ddol.idea.plugin.funl.tools

import com.intellij.lang.BracePair
import com.intellij.lang.PairedBraceMatcher
import com.intellij.psi.PsiFile
import com.intellij.psi.tree.IElementType
import org.ddol.idea.plugin.funl.psi.FunlTypes.*

/**
 * Matching all supported flavors of paired braces in Funl.
 */
class FunlPairedBraceMatcher : PairedBraceMatcher {

    override fun getPairs() = BRACE_PAIRS

    override fun isPairedBracesAllowedBeforeType(unused1: IElementType, unused2: IElementType?) = true

    override fun getCodeConstructStart(unused: PsiFile?, openingBraceOffset: Int) = openingBraceOffset

    companion object {
        private val BRACE_PAIRS = arrayOf(
                BracePair(PAREN_OPEN, PAREN_CLOSE, true),
                BracePair(BRACE_OPEN, BRACE_CLOSE, true))
    }
}
