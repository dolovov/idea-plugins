package org.ddol.idea.plugin.funl.tools

import com.intellij.codeInsight.completion.*
import com.intellij.codeInsight.completion.CompletionInitializationContext.DUMMY_IDENTIFIER_TRIMMED
import com.intellij.codeInsight.completion.CompletionType.BASIC
import com.intellij.codeInsight.completion.util.ParenthesesInsertHandler
import com.intellij.codeInsight.lookup.LookupElement
import com.intellij.codeInsight.lookup.LookupElementBuilder
import com.intellij.lang.parser.GeneratedParserUtilBase.DUMMY_BLOCK
import com.intellij.patterns.PlatformPatterns.psiElement
import com.intellij.patterns.StandardPatterns.string
import com.intellij.psi.PsiElement
import com.intellij.psi.ReferenceRange.containsOffsetInElement
import com.intellij.psi.TokenType.ERROR_ELEMENT
import com.intellij.psi.TokenType.WHITE_SPACE
import com.intellij.psi.tree.TokenSet
import com.intellij.psi.tree.TokenSet.create
import com.intellij.psi.tree.TokenSet.orSet
import com.intellij.psi.util.PsiTreeUtil.findFirstParent
import com.intellij.psi.util.PsiTreeUtil.prevLeaf
import com.intellij.util.ProcessingContext
import org.ddol.idea.plugin.funl.FunlParserDefinition.Companion.ANY_BODY_ALL_STATEMENTS
import org.ddol.idea.plugin.funl.FunlParserDefinition.Companion.ANY_BODY_KEYWORD_TOKENS
import org.ddol.idea.plugin.funl.FunlParserDefinition.Companion.ANY_BODY_VALID_STATEMENTS
import org.ddol.idea.plugin.funl.FunlParserDefinition.Companion.BLOCK_BODY_KEYWORD_TOKENS
import org.ddol.idea.plugin.funl.FunlParserDefinition.Companion.FUNL_FILE
import org.ddol.idea.plugin.funl.FunlParserDefinition.Companion.PROGRAM_KEYWORD_TOKENS
import org.ddol.idea.plugin.funl.navigation.FunlDocumentationProvider.getCompactSignatureDescriptionPlain
import org.ddol.idea.plugin.funl.navigation.FunlDocumentationProvider.getIconForNamedElement
import org.ddol.idea.plugin.funl.psi.*
import org.ddol.idea.plugin.funl.psi.FunlTypes.*

/**
 * Code completion for Funl language.
 *
 * Two basic types of code completion supported:
 * - Suggesting a keyword (such as "func", "val", etc) that could be in the beginning of a new statement.
 * - Suggesting a reference to a function, parameter or value declared before the completion position.
 */
class FunlCompletionContributor : CompletionContributor() {

    init {
        extend(BASIC, PROGRAM_ELEMENT, FunlKeywordsCompletionProvider(PROGRAM_KEYWORD_TOKENS))
        extend(BASIC, BLOCK_BODY_ELEMENT, FunlKeywordsCompletionProvider(BLOCK_BODY_KEYWORD_TOKENS))
        extend(BASIC, REFERENCE_ELEMENT, FunlReferenceCompletionProvider)
    }

    override fun beforeCompletion(context: CompletionInitializationContext) {
        val file = context.file.takeIf { it.node.elementType == FUNL_FILE } ?: return
        val elementBeforeCursor = file.findElementAt(context.startOffset - 1) ?: return

        elementBeforeCursor.node.elementType.let {
            when {

                // whitespace
                it == WHITE_SPACE -> beforeCompletionForWhitespace(elementBeforeCursor, context)

                // keywords or identifiers
                ANY_BODY_KEYWORD_TOKENS.contains(it) || it == IDENTIFIER ->
                    beforeCompletionForIdentifierOrKeyword(elementBeforeCursor, context)

                // open brace
                it == BRACE_OPEN -> beforeCompletionForBraceOpen(elementBeforeCursor, context)
            }
        }
    }

    /**
     * Prepare for completion when element before cursor is whitespace.
     */
    private fun beforeCompletionForWhitespace(
            whitespaceBeforeCursor: PsiElement,
            context: CompletionInitializationContext) {

        // get the previous leaf element
        val previousVisibleLeaf = prevLeaf(whitespaceBeforeCursor)

        if (isLeafInRecognizedIncompleteComposite(previousVisibleLeaf)) {

            // incomplete previous element => prepare for completion of the incomplete element
            // i.e. no keyword completion
        } else if (isAtNewLineInWhitespace(whitespaceBeforeCursor, context)
                || isBlockBodyOpenBrace(previousVisibleLeaf)) {

            // the position where code completion was invoked is either at new line, or immediately after
            // open brace '{' character => prepare for completion of keyword-starting expression
            beforeCompletionKeywordStartingExpression(whitespaceBeforeCursor, context)
        }
    }

    /**
     * Prepare for completion when element before cursor is [IDENTIFIER] or some keyword.
     */
    private fun beforeCompletionForIdentifierOrKeyword(
            identifierBeforeCursor: PsiElement,
            context: CompletionInitializationContext) {

        var keywordCompletionPossible = isFirstLeafInKnownComposite(identifierBeforeCursor)

        if (!keywordCompletionPossible) {

            val previousLeaf = prevLeaf(identifierBeforeCursor)

            // no leaf before identifier => can complete with keyword
            keywordCompletionPossible = previousLeaf == null

            if (!keywordCompletionPossible && previousLeaf!!.node.elementType == WHITE_SPACE) {

                // leaf before identifier is whitespace with newline => can complete with keyword
                keywordCompletionPossible = previousLeaf.textContains('\n')

                if (!keywordCompletionPossible) {

                    // leaf before the whitespace is absent or is open brace '{' character => can complete with keyword
                    val gPreviousLeaf = prevLeaf(previousLeaf)
                    keywordCompletionPossible = gPreviousLeaf == null || isBlockBodyOpenBrace(gPreviousLeaf)
                }
            }

            if (!keywordCompletionPossible)
                // leaf is open brace '{' character => can complete with keyword
                keywordCompletionPossible = isBlockBodyOpenBrace(previousLeaf)
        }

        if (keywordCompletionPossible)
            // prepare for completion of keyword-starting expression (if applicable)
            beforeCompletionKeywordStartingExpression(identifierBeforeCursor, context)
    }

    /**
     * Prepare for completion when element before cursor is open brace character '{'.
     */
    private fun beforeCompletionForBraceOpen(
            braceOpenBeforeCursor: PsiElement,
            context: CompletionInitializationContext) {

        if (isBlockBodyOpenBrace(braceOpenBeforeCursor))
            beforeCompletionKeywordStartingExpression(braceOpenBeforeCursor, context)
    }

    /**
     * Prepare for completion of keyword-starting expression, such as [FunlValue], [FunlFunction], [FunlOutput] and
     * [FunlReturn].
     */
    private fun beforeCompletionKeywordStartingExpression(
            elementBeforeCursor: PsiElement,
            context: CompletionInitializationContext) {

        when (getContextParent(elementBeforeCursor)?.node?.elementType) {
            FUNL_FILE -> context.dummyIdentifier = DUMMY_IDENTIFIER_GLOBAL
            BLOCK_BODY -> context.dummyIdentifier = DUMMY_IDENTIFIER_LOCAL
        }
    }

    @Suppress("RedundantOverride")
    override fun fillCompletionVariants(parameters: CompletionParameters, result: CompletionResultSet) {
        super.fillCompletionVariants(parameters, result)
    }
}


private const val DUMMY_IDENTIFIER_GLOBAL = "FunlLangIdentifierGlObAlzzz"
private const val DUMMY_IDENTIFIER_LOCAL = "FunlLangIdentifierLoCaLzzz"

private val PROGRAM_ELEMENT = psiElement(IDENTIFIER).withText(string().contains(DUMMY_IDENTIFIER_GLOBAL))
private val BLOCK_BODY_ELEMENT = psiElement(IDENTIFIER).withText(string().contains(DUMMY_IDENTIFIER_LOCAL))
private val REFERENCE_ELEMENT = psiElement(IDENTIFIER).withText(string().contains(DUMMY_IDENTIFIER_TRIMMED))


private val CONTEXT_RESTRICTING_STATEMENTS = create(FUNL_FILE, BLOCK_BODY, INCOMPLETE)
private val CONTEXT_RESTRICTING_PLUS_ANY_BODY_VALID_STATEMENTS = orSet(CONTEXT_RESTRICTING_STATEMENTS, ANY_BODY_VALID_STATEMENTS)

/**
 * Check if the given [leaf] element belongs to some incomplete composite element that should be completed.
 */
private fun isLeafInRecognizedIncompleteComposite(leaf: PsiElement?): Boolean {

    var composite = leaf ?: return false

    while (true) {
        composite = composite.parent ?: return false

        if (composite.lastChild.node.elementType == ERROR_ELEMENT)
            return true

        when {
            CONTEXT_RESTRICTING_PLUS_ANY_BODY_VALID_STATEMENTS.contains(composite.node.elementType) -> return false
        }
    }
}

/**
 * If the given [leaf] element is the first child for some known composite element such as
 * [FunlValue], [FunlReturn], [FunlOutput] or [FunlFunction].
 */
private fun isFirstLeafInKnownComposite(leaf: PsiElement): Boolean {

    var composite = leaf
    var myLeaf = leaf

    while (true) {
        composite = composite.parent ?: return false

        if (myLeaf != composite.firstChild)
            return false
        else
            myLeaf = composite

        composite.node.elementType.let {
            when {
                ANY_BODY_VALID_STATEMENTS.contains(it) -> return true
                CONTEXT_RESTRICTING_STATEMENTS.contains(it) -> return false // stop here and return
                else -> {}
            }
        }
    }
}

/**
 * If [element] is an open brace '{' character inside of [BLOCK_BODY].
 */
private fun isBlockBodyOpenBrace(element: PsiElement?) =
        element?.node?.elementType == BRACE_OPEN && element?.parent?.node?.elementType == BLOCK_BODY

/**
 * Whether [CompletionInitializationContext.getStartOffset] points to the position in the given
 * [whitespace] element so that there is at least one new line character between the beginning of the whitespace
 * and the startOffset itself. Or the whitespace is the very first leaf in the file.
 */
private fun isAtNewLineInWhitespace(whitespace: PsiElement, context: CompletionInitializationContext): Boolean {

    assert(whitespace.node.elementType == WHITE_SPACE)

    return whitespace.prevSibling == null
            || whitespace.text.indexOf('\n') in 0..(context.startOffset - whitespace.node.startOffset - 1)
}

/**
 * Parent [PsiElement] of the expression being typed may not always be detected properly, because
 * the expression is not completed yet and parser is not able to recognize it properly and insert it into PSI tree.
 */
private fun getContextParent(identifier: PsiElement) = findFirstParent(identifier, true) {
    !ANY_BODY_ALL_STATEMENTS.contains(it.node.elementType) && it.node.elementType != DUMMY_BLOCK
}


/**
 * Completion of keywords.
 */
private class FunlKeywordsCompletionProvider(keywordTokens: TokenSet) : CompletionProvider<CompletionParameters>() {

    private val keywords = keywordTokens.types.map { buildLookupElement(it as FunlTokenType) }

    override fun addCompletions(parameters: CompletionParameters, unused: ProcessingContext?, result: CompletionResultSet) {
        if (parameters.completionType != BASIC)
            return

        result.addAllElements(keywords)
        result.stopHere() // do not continue completion
    }

    private fun buildLookupElement(tokenType: FunlTokenType): LookupElement {

        val lookupElement = LookupElementBuilder.create(tokenType.shortName)
                .bold()
                .withInsertHandler(AddSpaceInsertHandler(false))

        // order keywords according to predefined priority
        val priority = when (tokenType) {
            KEYWORD_FUNC -> 0
            KEYWORD_VAL -> -1
            KEYWORD_SAY -> -2
            KEYWORD_RETURN -> -3
            else -> -100
        }.toDouble()

        return PrioritizedLookupElement.withPriority(lookupElement, priority)
    }
}

/**
 * Completion of references for values, parameters and functions.
 */
private object FunlReferenceCompletionProvider : CompletionProvider<CompletionParameters>() {

    override fun addCompletions(parameters: CompletionParameters, unused: ProcessingContext?, result: CompletionResultSet) {
        if (parameters.completionType != BASIC)
            return

        doAddCompletions(parameters, result)
        result.stopHere() // do not continue completion
    }

    private fun doAddCompletions(parameters: CompletionParameters, result: CompletionResultSet) {

        val reference = parameters.position.containingFile.findReferenceAt(parameters.offset) as? FunlReferenceExpression ?: return
        val element = reference.element

        val offsetInElement = parameters.offset - element.textRange.startOffset
        if (!containsOffsetInElement(reference, offsetInElement))
            return

        result.withPrefixMatcher(element.text.substring(reference.rangeInElement.startOffset, offsetInElement))

        reference.variants.forEach { variant ->

            var lookupElement = LookupElementBuilder.create(variant.name!!)
                    .withIcon(getIconForNamedElement(variant))
                    .withPresentableText(getCompactSignatureDescriptionPlain(variant))

            if (variant is FunlFunction)
                lookupElement = lookupElement.withInsertHandler(
                        ParenthesesInsertHandler.getInstance(
                                variant.parameterList.isNotEmpty(),
                                false,
                                false,
                                true,
                                false))

            result.addElement(lookupElement)
        }
    }
}