package org.ddol.idea.plugin.funl.psi

import com.intellij.psi.PsiElementResolveResult
import org.ddol.idea.plugin.funl.navigation.*
import org.ddol.idea.plugin.funl.psi.FunlTypes.*
import org.ddol.idea.plugin.funl.tools.FunlItemPresentation.Companion.createPresentation

/**
 * Auxiliary methods injected into the source code of the generated Funl language elements.
 */
object InjectedMethods {

    /**
     * Helper methods for structured view:
     */
    @JvmStatic
    fun getPresentation(function: FunlFunction) = createPresentation(function)

    @JvmStatic
    fun getPresentation(value: FunlValue) = createPresentation(value)

    @JvmStatic
    fun getPresentation(output: FunlOutput) = createPresentation(output)

    @JvmStatic
    fun getPresentation(incomplete: FunlIncomplete) = createPresentation(incomplete)

    /**
     * Helper methods for implementers of [FunlElementWithIdentifier] and [FunlNamedElement]:
     */
    @JvmStatic
    fun getNameIdentifier(element: FunlElementWithIdentifier) = element.getIdentifier()

    @JvmStatic
    fun getName(element: FunlElementWithIdentifier): String? = getNameIdentifier(element)?.text

    @JvmStatic
    fun setName(element: FunlNamedElement, newName: String) = renameElement(element, newName)

    @JvmStatic
    fun getTextOffset(element: FunlNamedElement) = (getNameIdentifier(element) ?: element).textRange.startOffset

    /**
     * Helper methods for reference navigation support:
     */
    @JvmStatic
    fun getReference(expression: FunlReferenceExpression) = expression

    @JvmStatic
    fun multiResolve(expression: FunlReferenceExpression, @Suppress("UNUSED_PARAMETER") unused: Boolean) =
            resolveReferenceExpression(expression, FunlResolveModeExactMatch).map { PsiElementResolveResult(it) }.toTypedArray()

    @JvmStatic
    fun getVariants(expression: FunlReferenceExpression) =
            resolveReferenceExpression(expression, FunlResolveModeAllVariants).toTypedArray()

    @JvmStatic
    fun isFunctionCall(expression: FunlReferenceExpression) = expression.parent.node.elementType == FUNCTION_CALL_EXPRESSION

    /**
     * Helper methods for [FunlValue]:
     */
    @JvmStatic
    fun isGlobal(value: FunlValue) = value.parent is FunlFile
}
