package org.ddol.idea.plugin.funl.tests

import com.intellij.lang.annotation.Annotator
import com.intellij.testFramework.ExpectedHighlightingData
import com.intellij.testFramework.builders.EmptyModuleFixtureBuilder
import com.intellij.testFramework.fixtures.CodeInsightFixtureTestCase
import com.intellij.testFramework.fixtures.ModuleFixture
import org.ddol.idea.plugin.funl.annotators.*
import kotlin.reflect.KClass

/**
 * Tests for Funl [Annotator]s:
 * - [IncompleteStatementAnnotator]
 * - [TwoStatementsSameLineAnnotator]
 * - [ReturnStatementIssueAnnotator]
 * - [UnresolvedReferencesAnnotator]
 * - [UnusedElementAnnotator]
 * - [DuplicatedAndShadowedDeclarationsAnnotator]
 */
class AnnotatorTests : CodeInsightFixtureTestCase<EmptyModuleFixtureBuilder<ModuleFixture>>() {

    override fun setUp() {
        super.setUp()
        myFixture.configureByFile(testFilePath("AnnotatorTests.funl"))
    }

    fun testHighlighting() {
        myFixture.checkHighlighting(true, true, true, false)
    }

    fun testIntentionActions() {
        assertEquals(expectedIntentionActions(), foundIntentionActions())
    }

    private fun expectedIntentionActions(): List<Pair<KClass<out BasicElementFix>, String>> = listOf(
            DeleteElementFix::class to "func foo(a, b) = 1000 * a",
            DeleteElementFix::class to "val first = 1",
            MoveStatementToNextLineFix::class to "val second = 2",
            DeleteElementFix::class to "val second = 2",
            FunctionBlockBodyWithoutReturnStatementFix::class to "}"
    )

    private fun foundIntentionActions(): List<Pair<KClass<out BasicElementFix>, String>> {

        ExpectedHighlightingData(myFixture.editor.document, true, true, true, false, myFixture.file).init()

        return myFixture.doHighlighting()
                .filter { it.quickFixActionRanges != null }
                .flatMap { it.quickFixActionMarkers }
                .map { it.first.action }
                .filterIsInstance(BasicElementFix::class.java)
                .map { it::class to it.element.text }
    }
}
