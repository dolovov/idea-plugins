package org.ddol.idea.plugin.funl.annotators

import com.intellij.codeInsight.intention.IntentionAction
import com.intellij.psi.PsiElement
import org.ddol.idea.plugin.funl.validation.TwoStatementsSameLineValidator
import org.ddol.idea.plugin.funl.validation.TwoStatementsSameLineValidator.SUGGESTION_MOVE_ELEMENT_NEXT_LINE

/**
 * Annotator: Detects if there are two subsequent [PsiElement]s located at the same line of code that normally
 * should be located at different lines. Annotates the beginning of the second [PsiElement] with error
 * and suggests a quick fix to move the [PsiElement] to the next line.
 */
class TwoStatementsSameLineAnnotator : FunlValidatorAdapter<PsiElement>() {

    override val elementClass = PsiElement::class
    override val validator = TwoStatementsSameLineValidator

    override fun produceFixes(alias: String, element: PsiElement, consume: (IntentionAction) -> Unit) {
        if (alias == SUGGESTION_MOVE_ELEMENT_NEXT_LINE) consume(MoveStatementToNextLineFix(element))
    }
}
