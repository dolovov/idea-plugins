package org.ddol.idea.plugin.funl.navigation

import com.intellij.psi.PsiElement
import com.intellij.psi.PsiReference
import com.intellij.psi.search.LocalSearchScope
import com.intellij.psi.search.searches.ReferencesSearch
import com.intellij.refactoring.rename.RenamePsiElementProcessor
import com.intellij.util.containers.MultiMap
import org.ddol.idea.plugin.funl.Texts.TEXT_ERROR_FUNCTION_NAME_CONFLICT
import org.ddol.idea.plugin.funl.Texts.TEXT_ERROR_GLOBAL_VALUE_NAME_CONFLICT
import org.ddol.idea.plugin.funl.Texts.TEXT_ERROR_LOCAL_VALUE_NAME_CONFLICT
import org.ddol.idea.plugin.funl.Texts.TEXT_ERROR_PARAMETER_NAME_CONFLICT
import org.ddol.idea.plugin.funl.Texts.TEXT_ERROR_REFERENCE_RESOLVING_CHANGES
import org.ddol.idea.plugin.funl.psi.*
import org.ddol.idea.plugin.funl.tools.FunlNamesValidator
import org.ddol.idea.plugin.funl.unreachableCode

/**
 * To enable renaming refactoring for Funl.
 *
 * Renaming is allowed to the following elements:
 * - [FunlFunction]
 * - [FunlParameter]
 * - [FunlValue], both global (declared at the top level) and local (declared inside function)
 * - and references to these elements
 *
 * The following checks performed before renaming:
 * - All: New name validation (performed by [FunlNamesValidator])
 * - [FunlFunction]s: New signature conflicts with signatures of existing [FunlFunction]s
 * - [FunlParameter]s and [FunlValue]s:
 *   - Naming conflict with a sibling of the same kind (ex: two parameters have the same name within the same function)
 *   - Some [FunlReferenceExpression], that resolved to the renamed element previously, resolves to another element now
 *   - Some [FunlReferenceExpression], resolves to the renamed element now, but previously did it to another element
 */
class FunlRenamePsiElementProcessor : RenamePsiElementProcessor() {

    override fun canProcessElement(element: PsiElement) = element is FunlNamedElement

    override fun findReferences(element: PsiElement): MutableCollection<PsiReference> =
            ReferencesSearch.search(element, LocalSearchScope(element.containingFile)).findAll()

    override fun findExistingNameConflicts(
            element: PsiElement,
            newName: String,
            conflicts: MultiMap<PsiElement, String>) = findExistingNameConflicts(element, newName, conflicts, emptyMap())

    override fun findExistingNameConflicts(
            element: PsiElement,
            newName: String,
            conflicts: MultiMap<PsiElement, String>,
            unused: Map<PsiElement, String>) {

        when (element) {
            is FunlFunction -> findFunctionNameConflicts(element, newName, conflicts)
            is FunlValue -> findValueNameConflicts(element, newName, conflicts)
            is FunlParameter -> findParameterNameConflicts(element, newName, conflicts)
            else -> {}
        }
    }

    private fun findFunctionNameConflicts(
            renamedFunction: FunlFunction,
            newName: String,
            conflicts: MultiMap<PsiElement, String>) {

        val file = renamedFunction.containingFile as FunlFile

        // check whether there is another function with the same name and number of parameters
        getFunctions(file) { function ->
            function.name == newName && function.parameterList.size == renamedFunction.parameterList.size
        }.forEach { function ->
            conflicts.putValue(function, TEXT_ERROR_FUNCTION_NAME_CONFLICT(newName, file.name))
        }
    }

    private fun findValueNameConflicts(
            renamedValue: FunlValue,
            newName: String,
            conflicts: MultiMap<PsiElement, String>) {

        val parent = renamedValue.parent
        when (parent) {

            is FunlFile -> {

                // check whether there is a sibling global value with the same name
                getGlobalValues(parent) { it.name == newName }.forEach { globalValue ->
                    conflicts.putValue(globalValue, TEXT_ERROR_GLOBAL_VALUE_NAME_CONFLICT(newName, parent.name))
                }

                // check references resolving before and after renaming
                parent.acceptChildren(ReferencesResolvingVisitor(renamedValue.node.startOffset, renamedValue, newName, conflicts))
            }

            is FunlBlockBody -> {

                // check whether there is a sibling local value with the same name
                getLocalValues(parent) { it.name == newName }.forEach { localValue ->
                    conflicts.putValue(
                            localValue,
                            TEXT_ERROR_LOCAL_VALUE_NAME_CONFLICT(newName, (parent.parent as FunlFunction).name.orEmpty()))
                }

                // check references resolving before and after renaming
                parent.acceptChildren(ReferencesResolvingVisitor(renamedValue.node.startOffset, renamedValue, newName, conflicts))
            }

            else -> unreachableCode("Unexpected type of parent (${renamedValue.parent}) met for $renamedValue")
        }
    }

    private fun findParameterNameConflicts(
            renamedParameter: FunlParameter,
            newName: String,
            conflicts: MultiMap<PsiElement, String>) {

        val function = renamedParameter.parent as FunlFunction

        // check whether there is a sibling parameter with the same name
        getParameters(function) { it.name == newName }.forEach { parameter ->
            conflicts.putValue(parameter, TEXT_ERROR_PARAMETER_NAME_CONFLICT(newName, function.name.orEmpty()))
        }

        // check references resolving before and after renaming
        function.acceptChildren(ReferencesResolvingVisitor(0, renamedParameter, newName, conflicts))
    }
}

/**
 * Implementation of [FunlVisitor] for scanning Funl program and finding all [FunlReferenceExpression]s that are
 * resolved to old or new name of the [renamedElement]. For every such [FunlReferenceExpression] check: will
 * it start to resolve to a different [FunlNamedElement] after renaming. And if yes, yield a naming conflict.
 */
private class ReferencesResolvingVisitor(
        private val minStartOffset: Int,
        private val renamedElement: FunlNamedElement,
        private val newName: String,
        private val conflicts: MultiMap<PsiElement, String>) : FunlVisitor() {

    private val oldName = renamedElement.name!!

    override fun visitReferenceExpression(reference: FunlReferenceExpression) {
        super.visitReferenceExpression(reference)

        if (reference.isFunctionCall)
            return

        val referenceName = reference.getName() ?: return

        when (referenceName) {

            oldName -> {
                // the reference _may_ resolve to the renamed element
                // let's check this, and if yes - make sure it still will be resolved to the same element after renaming
                val resolved = resolve(reference)
                if (resolved == renamedElement) {

                    val resolvedRenamed = resolveRenamed(reference, true)
                    if (resolvedRenamed != renamedElement) {
                        // reference does not resolve to renamed element anymore
                        noMoreResolvesToRenamedElement(reference, resolvedRenamed)
                    }
                }
            }

            newName -> {
                // the reference _may_ resolve to some other element, but after renaming it may resolve to the renamed element
                // let's check that this won't happen
                val resolved = resolve(reference)
                if (resolved != null && resolved != renamedElement) {

                    val resolvedRenamed = resolveRenamed(reference, false)
                    if (resolvedRenamed != null && resolvedRenamed != resolved) {
                        // reference starts to resolve to renamed element
                        assert(resolvedRenamed == renamedElement) {
                            "Reference started to resolve to unexpected element during renaming:\nreference = $reference\nold = $resolved\nnew = $resolvedRenamed"
                        }
                        startsToResolveToRenamedElement(reference, resolved)
                    }
                }
            }

            else -> {}
        }
    }

    override fun visitElement(element: PsiElement?) {
        super.visitElement(element)

        if (element != null && !skippedElement(element))
            element.acceptChildren(this) // make it recursive
    }

    /**
     * Skip elements that ends before the [minStartOffset].
     */
    private fun skippedElement(element: PsiElement) = element.node.textRange.endOffset < minStartOffset

    @Suppress("UNUSED_PARAMETER")
    private fun noMoreResolvesToRenamedElement(reference: FunlReferenceExpression, newElement: FunlNamedElement?) =
            conflicts.putValue(reference, TEXT_ERROR_REFERENCE_RESOLVING_CHANGES(reference.getName()!!))

    @Suppress("UNUSED_PARAMETER")
    private fun startsToResolveToRenamedElement(reference: FunlReferenceExpression, oldElement: FunlNamedElement) =
            conflicts.putValue(reference, TEXT_ERROR_REFERENCE_RESOLVING_CHANGES(reference.getName()!!))

    private fun resolve(reference: FunlReferenceExpression) = reference.resolve() as FunlNamedElement?

    private fun resolveRenamed(
            reference: FunlReferenceExpression,
            referenceIsAlsoRenamed: Boolean): FunlNamedElement? {

        val resolved = resolveReferenceExpression(
                reference,
                FunlResolveModeExactMatchForRenaming(
                        renamedElement,
                        newName,
                        if (referenceIsAlsoRenamed) reference else null))

        assert(resolved.isNotEmpty()) { "Reference became completely unresolvable during renaming: $reference" }

        // if there are multiple resolved elements, then don't return any one of them
        // that's a situation when there is a naming conflict (and it should be reported separately to user)
        return resolved.takeIf { it.size == 1 }?.first()
    }
}
