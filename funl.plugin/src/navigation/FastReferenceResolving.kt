package org.ddol.idea.plugin.funl.navigation

import com.intellij.psi.PsiElement
import com.intellij.psi.tree.IElementType
import com.intellij.psi.util.PsiTreeUtil.findSiblingBackward
import com.intellij.psi.util.PsiTreeUtil.treeWalkUp
import org.ddol.idea.plugin.funl.FunlParserDefinition.Companion.FUNL_FILE
import org.ddol.idea.plugin.funl.navigation.ReferencedElementSignature.Companion.createSignature
import org.ddol.idea.plugin.funl.navigation.ReferencedElementSignature.Companion.createValidSignatureOrNull
import org.ddol.idea.plugin.funl.psi.*
import org.ddol.idea.plugin.funl.psi.FunlTypes.*

sealed class FunlResolveMode(val stopLookupOnFound: Boolean)

/**
 * Find the exact [FunlNamedElement] that a [FunlReferenceExpression] refers to.
 *
 * The resulting element may be one of the following:
 * 1) [FunlFunction]
 * 2) [FunlValue]
 * 3) [FunlParameter]
 * 3) none (not found, broken reference)
 *
 * If there are multiple target [FunlNamedElement]s that the reference could be resolved to (which is an error
 * in Funl program code and should be normally identified by the appropriate Funl inspection), then
 * [FunlNamedElement]s that the reference could be resolved to are taken.
 */
object FunlResolveModeExactMatch : FunlResolveMode(true)

/**
 * The same as [FunlResolveModeExactMatch] but does lookup in the assumption that [renamedElement] will be
 * renamed to [newName].
 */
class FunlResolveModeExactMatchForRenaming(
        val renamedElement: FunlNamedElement,
        val newName: String,
        val renamedReference: FunlReferenceExpression?) : FunlResolveMode(true) {

    val oldName = renamedElement.name!!

    init {
        assert(renamedReference == null || renamedReference.getName() == renamedElement.name)
    }
}

/**
 * Find all [FunlNamedElement]s that may be referenced by a [FunlReferenceExpression].
 *
 * Typically this is useful for code completion.
 */
object FunlResolveModeAllVariants : FunlResolveMode(false)

/**
 * Resolve the given [FunlReferenceExpression] in fast pace:
 * - do no scan the whole PSI tree
 * - do not collect any unrelated named elements
 * - stop when found, unless jumping to the next level of hierarchy is necessary
 */
fun resolveReferenceExpression(
        expression: FunlReferenceExpression,
        resolveMode: FunlResolveMode): List<FunlNamedElement> {

    if (expression.getName().isNullOrEmpty())
        return emptyList()

    return findReferencedElementsBeforeElement(
            expression,
            ReferencedElementSignatureMatcher(expression, resolveMode))
}

private fun findReferencedElementsBeforeElement(
        referenceElement: PsiElement,
        signatureMatcher: ReferencedElementSignatureMatcher): List<FunlNamedElement> {

    // the map to collect all matched elements
    val referencedElements = mutableListOf<FunlNamedElement>()

    // the lambda to process each individual element (and maybe add to to the collection of matched elements)
    // returns true if lookup should be stopped after the current row of siblings, otherwise - false
    val processElementLambda: (FunlNamedElement) -> Boolean = { element ->
        signatureMatcher.runIfMatches(element, createValidSignatureOrNull(element)) {
            // returns true if element was added to `referencedElements`, otherwise - false
            referencedElements.add(element)
        }
    }

    // walk through PSI tree to the top and lookup the necessary elements
    treeWalkUp(referenceElement, referenceElement.containingFile) { parent, child ->
        when (parent.node.elementType) {

            BLOCK_BODY ->
                if (signatureMatcher.lookupParametersAndValues)
                    return@treeWalkUp lookupElements(child, VALUE, processElementLambda)

            FUNCTION ->
                if (signatureMatcher.lookupParametersAndValues)
                    return@treeWalkUp lookupElements(child, PARAMETER, processElementLambda)

            FUNL_FILE -> {
                if (signatureMatcher.lookupParametersAndValues)
                    if (!lookupElements(child, VALUE, processElementLambda))
                        return@treeWalkUp false

                if (signatureMatcher.lookupFunctions)
                    return@treeWalkUp lookupElements(child, FUNCTION, processElementLambda)
            }

            else -> {
            }
        }

        return@treeWalkUp true
    }

    return referencedElements.reversed()
}

/**
 * Returns:
 * - true if lookup should be continued further
 * - false if lookup should be stopped after the current row of siblings
 */
private fun lookupElements(
        startElement: PsiElement,
        elementType: IElementType,
        block: (FunlNamedElement) -> Boolean): Boolean {

    var continueLookup = true

    var element: PsiElement? = startElement
    do {
        element = findSiblingBackward(element!!, elementType, null)?.also { e ->
            continueLookup = !block(e as FunlNamedElement) && continueLookup
        }
    } while (element != null)

    return continueLookup
}

private class ReferencedElementSignatureMatcher(
        reference: FunlReferenceExpression,
        private val resolveMode: FunlResolveMode) {

    private val searchKey = when (resolveMode) {

        is FunlResolveModeExactMatch -> {
            // use signature of the reference expression as the search key
            createSignature(reference)
        }

        is FunlResolveModeExactMatchForRenaming -> {
            // use signature of the reference expression as the search key
            val signature = createSignature(reference)
            if (resolveMode.renamedReference == reference) signature.renameTo(resolveMode.newName) else signature
        }

        is FunlResolveModeAllVariants -> {
            // null means that every valid named element up to the top of the file will match
            null
        }
    }

    val lookupParametersAndValues = searchKey == null || !searchKey.isFunction
    val lookupFunctions = searchKey == null || searchKey.isFunction

    /**
     * Returns:
     * - true if lookup should be stopped after the current row of siblings
     * - false if lookup should be continued further
     */
    fun runIfMatches(
            element: FunlNamedElement,
            signature: ReferencedElementSignature?,
            block: () -> Boolean): Boolean {

        return if (signature != null && (searchKey == null || searchKey == renameSignature(element, signature)))
            block() && resolveMode.stopLookupOnFound
        else
            false // continue lookup
    }

    private fun renameSignature(
            element: FunlNamedElement,
            signature: ReferencedElementSignature): ReferencedElementSignature {

        if (resolveMode is FunlResolveModeExactMatchForRenaming
                && resolveMode.oldName == signature.name
                && resolveMode.renamedElement == element) {

            return signature.renameTo(resolveMode.newName)
        }

        return signature
    }
}
