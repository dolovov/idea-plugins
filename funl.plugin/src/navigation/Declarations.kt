package org.ddol.idea.plugin.funl.navigation

import org.ddol.idea.plugin.funl.navigation.ReferencedElementSignature.Companion.createValidSignatureOrNull
import org.ddol.idea.plugin.funl.psi.*

/**
 * Collects declarations for the given [FunlFile] for further analysis. The resulting [Declarations] object
 * includes all [FunlFunction]s, [FunlParameter]s and [FunlValue]s found in Funl file.
 */
fun collectDeclarations(file: FunlFile): Declarations {

    val visitor = DeclarationsCollectingVisitor()
    file.acceptChildren(visitor)

    return visitor.getDeclarations()
}

@Suppress("unused")
/**
 * Holder for [FunlNamedElement]s found in program. Used for further analysis (ex: inspections).
 *
 * Note: This class is not thread-safe.
 */
class Declarations {

    private val functions = newMultiMap<ReferencedElementSignature, FunlFunction>()
    private val functionParameters = newMultiMap2<FunlFunction, ReferencedElementSignature, FunlParameter>()
    private val functionLocalValues = newMultiMap2<FunlFunction, ReferencedElementSignature, FunlValue>()

    private val globalValues = newMultiMap<ReferencedElementSignature, FunlValue>()

    /**
     * The counter for global elements (such as [FunlFunction] and global [FunlValue]).
     */
    private var globalElementsCounter = 0

    /**
     * Keep a serial order ([Int]) for every global element (to answer the question `which was first and which second`)
     */
    private val globalElementsOrder = newMap<FunlNamedElement, Pair<ReferencedElementSignature, Int>>()


    internal fun addFunction(function: FunlFunction, signature: ReferencedElementSignature) {
        functions.putValue(signature, function)
        trackGlobalElement(signature, function)
    }

    internal fun addGlobalValue(value: FunlValue, signature: ReferencedElementSignature) {
        assert(value.isGlobal)
        globalValues.putValue(signature, value)
        trackGlobalElement(signature, value)
    }

    internal fun addParameter(function: FunlFunction, parameter: FunlParameter, signature: ReferencedElementSignature) {
        functionParameters.putValue(function, signature, parameter)
    }

    internal fun addLocalValue(function: FunlFunction, value: FunlValue, signature: ReferencedElementSignature) {
        assert(!value.isGlobal)
        functionLocalValues.putValue(function, signature, value)
    }

    private fun trackGlobalElement(signature: ReferencedElementSignature, element: FunlNamedElement) {
        globalElementsOrder[element] = Pair(signature, globalElementsCounter++)
    }

    /**
     * Get all [FunlFunction]s.
     */
    fun functions(): Map<ReferencedElementSignature, List<FunlFunction>> = functions

    /**
     * Get all global [FunlValue]s.
     */
    fun globalValues(): Map<ReferencedElementSignature, List<FunlValue>> = globalValues

    /**
     * Get all global [FunlValue]s declared before the given [FunlFunction].
     */
    fun globalValuesBeforeFunction(function: FunlFunction): Map<ReferencedElementSignature, List<FunlValue>> {
        val functionOrder = getOrder(function)
        return findGlobalElementsByOrder { it < functionOrder }
    }

    /**
     * Get all [FunlFunction]s declared after the given global [FunlValue].
     */
    fun functionsAfterGlobalValue(value: FunlValue): Map<ReferencedElementSignature, List<FunlFunction>> {
        assert(value.isGlobal)

        val valueOrder = getOrder(value)
        return findGlobalElementsByOrder { it > valueOrder }
    }

    /**
     * Get all [FunlParameter]s declared inside of [FunlFunction].
     */
    fun parameters(function: FunlFunction): Map<ReferencedElementSignature, List<FunlParameter>> =
            functionParameters[function].orEmpty()

    /**
     * Get all local [FunlValue]s declared inside of [FunlFunction].
     */
    fun localValues(function: FunlFunction): Map<ReferencedElementSignature, List<FunlValue>> =
            functionLocalValues[function].orEmpty()


    private fun getOrder(element: FunlNamedElement) = globalElementsOrder[element]!!.second

    private inline fun <reified T> findGlobalElementsByOrder(predicate: (Int) -> Boolean): MutableMultiMap<ReferencedElementSignature, T> {
        val result = newMultiMap<ReferencedElementSignature, T>()

        globalElementsOrder.forEach {
            if (it.key is T && predicate(it.value.second))
                result.putValue(it.value.first, it.key as T)
        }

        return result
    }

    private fun <K, V> newMap(): MutableMap<K, V> = LinkedHashMap()
    private fun <K, V> newMultiMap(): MutableMultiMap<K, V> = LinkedHashMap()
    private fun <K1, K2, V> newMultiMap2(): MutableMultiMap2<K1, K2, V> = LinkedHashMap()

    private fun <K, V> MutableMultiMap<K, V>.putValue(key: K, value: V) =
            this.computeIfAbsent(key) { mutableListOf() }.also { this[key]!!.add(value) }

    private fun <K1, K2, V> MutableMultiMap2<K1, K2, V>.putValue(key1: K1, key2: K2, value: V) =
            this.computeIfAbsent(key1) { newMultiMap() }.also { this[key1]!!.putValue(key2, value) }
}

/**
 * Implementation of [FunlVisitor] for scanning [FunlFile] and finding all
 * [FunlNamedElement]s such as [FunlValue], [FunlFunction] and [FunlParameter].
 */
private class DeclarationsCollectingVisitor : FunlVisitor() {

    private val declarations = Declarations()

    private var contextFunction: FunlFunction? = null

    override fun visitFunction(function: FunlFunction) {
        assertNoContextFunction()
        super.visitFunction(function)
        createValidSignatureOrNull(function) { declarations.addFunction(function, it) }
        withinContext(function) { function.acceptChildren(this) }
    }

    override fun visitBlockBody(blockBody: FunlBlockBody) {
        assertContextFunction()
        super.visitBlockBody(blockBody)
        blockBody.acceptChildren(this)
    }

    override fun visitValue(value: FunlValue) {
        super.visitValue(value)
        createValidSignatureOrNull(value) {
            when (value.isGlobal) {
                true -> {
                    assertNoContextFunction()
                    declarations.addGlobalValue(value, it)
                }
                false -> {
                    assertContextFunction()
                    declarations.addLocalValue(contextFunction!!, value, it)
                }
            }
        }
    }

    override fun visitParameter(parameter: FunlParameter) {
        assertContextFunction()
        super.visitParameter(parameter)
        createValidSignatureOrNull(parameter) { declarations.addParameter(contextFunction!!, parameter, it) }
    }

    fun getDeclarations() = declarations

    private fun withinContext(function: FunlFunction, block: () -> Unit) {
        assertNoContextFunction()

        contextFunction = function
        try {
            block()
        } finally {
            contextFunction = null
        }
    }

    private fun assertContextFunction() = assert(contextFunction != null)
    private fun assertNoContextFunction() = assert(contextFunction == null)
}

private typealias MutableMultiMap<K, V> = MutableMap<K, MutableList<V>>
private typealias MutableMultiMap2<K1, K2, V> = MutableMap<K1, MutableMap<K2, MutableList<V>>>
