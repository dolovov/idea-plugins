package org.ddol.idea.plugin.funl.tests

import com.intellij.lang.LanguageBraceMatching
import com.intellij.testFramework.ParsingTestCase
import org.ddol.idea.plugin.funl.FunlFileType
import org.ddol.idea.plugin.funl.FunlLanguage
import org.ddol.idea.plugin.funl.FunlParserDefinition
import org.ddol.idea.plugin.funl.tools.FunlPairedBraceMatcher

/**
 * Test parsing of Funl program with different variations of correct and wrong statements.
 * Make sure that the produced PSI tree corresponds to the expected.
 */
class ParsingTest : ParsingTestCase("", FunlFileType.defaultExtension, FunlParserDefinition()) {

    fun testParsingTest() = doTest(true)

    override fun skipSpaces() = false

    override fun includeRanges() = true

    override fun getTestDataPath() = TEST_DATA_PATH

    override fun loadFile(name: String) = doLoadFile(name)

    override fun setUp() {
        // set up all the necessary mocks
        super.setUp()

        // explicitly add paired brace matcher (needed for correct parsing)
        addExplicitExtension(LanguageBraceMatching.INSTANCE, FunlLanguage, FunlPairedBraceMatcher())
    }
}
