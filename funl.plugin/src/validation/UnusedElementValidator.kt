package org.ddol.idea.plugin.funl.validation

import com.intellij.psi.search.LocalSearchScope
import com.intellij.psi.search.searches.ReferencesSearch
import org.ddol.idea.plugin.funl.FunlParserDefinition.Companion.NAMED_ELEMENT_TOKENS
import org.ddol.idea.plugin.funl.Texts.TEXT_WARNING_FUNCTION_NEVER_USED
import org.ddol.idea.plugin.funl.Texts.TEXT_WARNING_PARAMETER_NEVER_USED
import org.ddol.idea.plugin.funl.Texts.TEXT_WARNING_VALUE_NEVER_USED
import org.ddol.idea.plugin.funl.navigation.FunlNamedElement
import org.ddol.idea.plugin.funl.psi.FunlTypes.*
import org.ddol.idea.plugin.funl.unreachableCode
import org.ddol.idea.plugin.funl.validation.FunlIssueLevel.WEAK_WARNING

/**
 * Validator: reports as warning [FunlNamedElement]s that are in fact not used by Funl program.
 */
object UnusedElementValidator : FunlValidator<FunlNamedElement> {

    const val SUGGESTION_DELETE_ELEMENT = "deleteElement"

    override val matchingElementTypes = NAMED_ELEMENT_TOKENS

    override fun validate(element: FunlNamedElement, sink: FunlIssueSink) {

        val identifier = element.getIdentifier() ?: return

        if (ReferencesSearch.search(element, LocalSearchScope(element.containingFile)).findFirst() == null) {

            val issue = FunlIssue(WEAK_WARNING, identifier, message(element))

            when (element.node.elementType) {
                // do not suggest fix for parameter (maybe implement it later?)
                FUNCTION, VALUE -> issue.withFixSuggestion(SUGGESTION_DELETE_ELEMENT, element)
            }

            sink.addIssue(issue)
        }
    }

    private fun message(element: FunlNamedElement) = when (element.node.elementType) {
        FUNCTION -> TEXT_WARNING_FUNCTION_NEVER_USED(element.name.orEmpty())
        PARAMETER -> TEXT_WARNING_PARAMETER_NEVER_USED(element.name.orEmpty())
        VALUE -> TEXT_WARNING_VALUE_NEVER_USED(element.name.orEmpty())
        else -> unreachableCode()
    }
}
