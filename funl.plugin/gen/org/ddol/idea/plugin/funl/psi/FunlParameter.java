// This is a generated file. Not intended for manual editing.
package org.ddol.idea.plugin.funl.psi;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.psi.PsiElement;
import org.ddol.idea.plugin.funl.navigation.FunlNamedElement;
import org.ddol.idea.plugin.funl.navigation.FunlElementWithIdentifier;

public interface FunlParameter extends FunlNamedElement {

  @NotNull
  PsiElement getIdentifier();

  @Nullable
  String getName();

  @Nullable
  FunlElementWithIdentifier setName(String newName);

  @Nullable
  PsiElement getNameIdentifier();

  int getTextOffset();

}
